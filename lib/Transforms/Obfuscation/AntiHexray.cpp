#include "llvm/Pass.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/InstrTypes.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Type.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/IR/GlobalValue.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/CodeGen/ISDOpcodes.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Transforms/IPO.h"
#include <list>

#include "llvm/Transforms/Obfuscation/eversafe_helper.hpp"
#include "llvm/Transforms/Obfuscation/AntiHexray.h"
#include "llvm/IRReader/IRReader.h"
#include "llvm/Linker/Linker.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/IR/InlineAsm.h"
#include "llvm/IR/CallingConv.h"
#include "llvm/ADT/Triple.h"







using namespace llvm;
using namespace std;
namespace {
	struct AntiHexray : public FunctionPass {
		static char ID; // Pass identification
		bool flag;
		AntiHexray() : FunctionPass(ID) {}
    AntiHexray(bool flag) : FunctionPass(ID) {
			this->flag = flag;
			 AntiHexray();
		 }
		virtual const char *getAntiHexrayAsm(Module *M)
		{
			string Target = M->getTargetTriple();
			Triple T = Triple(Target);
			const char *AntiHex;
			switch(T.getArch())
			{
				case Triple::thumb :
				case Triple::thumbeb :
				case Triple::arm :
				case Triple::armeb :
					AntiHex="push {r1,r2}\n" //임시 레지스터 백업
						"mov r1,pc\n" //현재 pc(program counter) 레지스터
						"mov r2,pc\n" //현재 pc(program counter) 레지스터
						"sub r2,r1\n" //연속된 2개의 op코드 주소의 차를 구한다 (thumb = 2, arm = 4)
						"add pc,r2\n" //현재 pc 에서 위의 값을 더하면 2개의 op코드를 뛰어넘고 다음 op코드가 실행된다.
						"mov r0,r0\n" //dummy
						"mov r0,r0\n" //dummy
						"pop {r1,r2}"; //임시 레지스터 복구
					break;

				case Triple::x86 :
					AntiHex="pushf\n" //플래그 레지스터 백업
						"call . + 5\n" //다음 op코드를 CALL
						"addl $$5, (%esp)\n" //복귀주소에 5를 더함
						"ret\n" //복귀주소로 점프
						"popf"; //플래그 레지스터 복구
					break;

				case Triple::x86_64 :
					AntiHex="pushf\n" //플래그 레지스터 백업
						"call . + 5\n" //다음 op코드를 CALL
						"addq $$6, (%rsp)\n" //복귀주소에 6을 더함
						"ret\n" //복귀주소로 점프
						"popf"; //플래그 레지스터 복구
					break;
				case Triple::aarch64 :
				case Triple::aarch64_be :
					AntiHex="stp x29,x30,[sp,#-16]!\n" //프레임 포인터 레지스터 및 복귀주소 레지스터 백업
						"bl . + 4\n" //다음 op코드를 CALL
						"add x30,x30,#8\n" //복귀주소에 8을 더함
						"ret\n" //복귀주소로 점프
						"ldp x29,x30,[sp],#16\n";
            #if 0
					AntiHex="stp x29, x30,[sp,#-32]!\n" //프레임 포인터 레지스터 및 복귀주소 레지스터 백업
						"bl . + 4\n" //다음 op코드를 CALL
						"add x30,x30,#8\n" //복귀주소에 8을 더함
						"ret\n" //복귀주소로 점프
						"mov x0, x0\n"
						"ldp x29, x30,[sp],#32\n";
            #endif
						// "ldp x29,[sp],#16\n";
					break;
				default :
					AntiHex = NULL;

			}
			return AntiHex;
		}
		virtual bool runOnFunction(Function &F){
      if(isContainAsmCallInst(F) == true) return false;
      Function *tmp = &F;
      if(!toObfuscate(flag, tmp, ANTIHEXFLAG)){
        return false;
      }
      // if(!EversafeHelper::instance().isApply(F.getName().str(), ANTIHEXFLAG)){
			// 	return false;
			// }
			// else{
			// 	llvm::errs() << "=================================================================================" <<"\n";
			// 	// ModuleName Parsing
			// 	string ModuleName = F.getParent()->getName().str();
			// 	ModuleName = ModuleName.substr(ModuleName.rfind("/"));
			// 	ModuleName = ModuleName.erase(0, 1);
			// 	llvm::errs() << "[ModuleName : " << ModuleName << "]\n";
			// 	// InsertLogData
			// 	string getName = F.getName().str();
			// 	ModuleName = F.getParent()->getName().str();
			// 	LogMgr::getLogMgr()->Insert(getName, ModuleName);
			// 	llvm::errs() << "Obfuscation Type : AntiDecompile Obfuscation\n";
			// 	llvm::errs() << "Complete\n";
			// 	llvm::errs() << "=================================================================================" <<"\n";
			// }

			Module *mod = F.getParent();
			const char * AntiHex = getAntiHexrayAsm(mod);
			if(AntiHex == NULL){
				errs() << "This Architecture is not Support Hexray\n";
				return false;
			}
      errs() << "Running AntiDecompile On " << F.getName() << "\n";





			//for(Function::iterator i = F.begin(), e = F.end(); i != e; i++)
			{
                Function::iterator i = F.begin(); //FIXME
				std::vector<Type*>FuncTy_0_args;
				FunctionType* FuncTy_0 = FunctionType::get(
						Type::getVoidTy(mod->getContext()),
						FuncTy_0_args,
						false);

				//InlineAsm* ptr_6 = InlineAsm::get(FuncTy_0, AntiHex, "~{dirflag},~{fpsr},~{flags}", true);
				InlineAsm* ptr_6 = InlineAsm::get(FuncTy_0, AntiHex, "", true);
				//InlineAsm* ptr_6 = InlineAsm::get(FuncTy_0, "nop", "",true);
				CallInst* void_5 = CallInst::Create(ptr_6, "");
				void_5->setCallingConv(CallingConv::C);
				void_5->setTailCall(false);
				AttributeList void_5_PAL;
				{
          // SmallVector<AttributeSet, 4> Attrs;
					AttributeList PAS;
					{
						AttrBuilder B;
						B.addAttribute(Attribute::NoUnwind);
						PAS = AttributeList::get(mod->getContext(), ~0U, B);
					}
          // Attrs.push_back(PAS);
				void_5_PAL = AttributeList::get(mod->getContext(), PAS);

				}
				void_5->setAttributes(void_5_PAL);
				i->getInstList().push_front(void_5);
			}
			return true;
		}
	};
}

char AntiHexray::ID = 0;
static RegisterPass<AntiHexray> X("AntiHexray", "inserting AntiHexray code");


FunctionPass *llvm::createAntiHexray() {
	return new AntiHexray();
}

FunctionPass *llvm::createAntiHexray(bool flag) {
	return new AntiHexray(flag);
}
