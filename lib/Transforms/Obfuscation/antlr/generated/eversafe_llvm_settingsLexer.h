
// Generated from eversafe_llvm_settings.g4 by ANTLR 4.7.1

#pragma once


#include "antlr4-runtime.h"


namespace antlrcpptest {


class  eversafe_llvm_settingsLexer : public antlr4::Lexer {
public:
  enum {
    CPP = 1, OBJC = 2, CC = 3, CLASS = 4, NAMESPACE = 5, METHOD = 6, ADD = 7, 
    SET = 8, REMOVE = 9, FUNCTION = 10, BOGUS = 11, FLATTEN = 12, WRAPPER = 13, 
    BRANCH = 14, SPILT = 15, STRING = 16, DUMMY = 17, ANTIDECOMPILE = 18, 
    SUBSTITUDE = 19, ANTICLASSDUMP = 20, ALL = 21, NON = 22, DWILDCARD = 23, 
    WILDCARD = 24, LBRACK = 25, RBRACK = 26, LBRACE = 27, RBRACE = 28, LPAREN = 29, 
    RPAREN = 30, COMMA = 31, DOT = 32, DCOLON = 33, INNER = 34, SEMICOLON = 35, 
    NAME = 36, CppGlobalMethodName = 37, ObjcMethodName = 38, ObjcGlobalMethodName = 39, 
    CppMethodName = 40, LineComment = 41, WS = 42, ANY = 43
  };

  eversafe_llvm_settingsLexer(antlr4::CharStream *input);
  ~eversafe_llvm_settingsLexer();

  virtual std::string getGrammarFileName() const override;
  virtual const std::vector<std::string>& getRuleNames() const override;

  virtual const std::vector<std::string>& getChannelNames() const override;
  virtual const std::vector<std::string>& getModeNames() const override;
  virtual const std::vector<std::string>& getTokenNames() const override; // deprecated, use vocabulary instead
  virtual antlr4::dfa::Vocabulary& getVocabulary() const override;

  virtual const std::vector<uint16_t> getSerializedATN() const override;
  virtual const antlr4::atn::ATN& getATN() const override;

private:
  static std::vector<antlr4::dfa::DFA> _decisionToDFA;
  static antlr4::atn::PredictionContextCache _sharedContextCache;
  static std::vector<std::string> _ruleNames;
  static std::vector<std::string> _tokenNames;
  static std::vector<std::string> _channelNames;
  static std::vector<std::string> _modeNames;

  static std::vector<std::string> _literalNames;
  static std::vector<std::string> _symbolicNames;
  static antlr4::dfa::Vocabulary _vocabulary;
  static antlr4::atn::ATN _atn;
  static std::vector<uint16_t> _serializedATN;


  // Individual action functions triggered by action() above.

  // Individual semantic predicate functions triggered by sempred() above.

  struct Initializer {
    Initializer();
  };
  static Initializer _init;
};

}  // namespace antlrcpptest
