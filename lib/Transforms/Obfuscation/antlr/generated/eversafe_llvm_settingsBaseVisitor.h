
// Generated from eversafe_llvm_settings.g4 by ANTLR 4.7.1

#pragma once


#include "antlr4-runtime.h"
#include "eversafe_llvm_settingsVisitor.h"


namespace antlrcpptest {

/**
 * This class provides an empty implementation of eversafe_llvm_settingsVisitor, which can be
 * extended to create a visitor which only needs to handle a subset of the available methods.
 */
class  eversafe_llvm_settingsBaseVisitor : public eversafe_llvm_settingsVisitor {
public:

  virtual antlrcpp::Any visitSettings(eversafe_llvm_settingsParser::SettingsContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitObjc(eversafe_llvm_settingsParser::ObjcContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitObjcInternalData(eversafe_llvm_settingsParser::ObjcInternalDataContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitObjcClassFormat(eversafe_llvm_settingsParser::ObjcClassFormatContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitObjcClassIntetnalData(eversafe_llvm_settingsParser::ObjcClassIntetnalDataContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitObjcMethodFormat(eversafe_llvm_settingsParser::ObjcMethodFormatContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitObjcGlobalMethodFormat(eversafe_llvm_settingsParser::ObjcGlobalMethodFormatContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCpp(eversafe_llvm_settingsParser::CppContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCppInternalData(eversafe_llvm_settingsParser::CppInternalDataContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCppClassInternalData(eversafe_llvm_settingsParser::CppClassInternalDataContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCppSingleAsteriskClassInternalData(eversafe_llvm_settingsParser::CppSingleAsteriskClassInternalDataContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCppClassFormat(eversafe_llvm_settingsParser::CppClassFormatContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitSingleCppClassFormat(eversafe_llvm_settingsParser::SingleCppClassFormatContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitDoulbeCppClassFormat(eversafe_llvm_settingsParser::DoulbeCppClassFormatContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCppMethodFormat(eversafe_llvm_settingsParser::CppMethodFormatContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCppName(eversafe_llvm_settingsParser::CppNameContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitSingleAsteriskName(eversafe_llvm_settingsParser::SingleAsteriskNameContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitDoubleAsteriskName(eversafe_llvm_settingsParser::DoubleAsteriskNameContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCppMethodName(eversafe_llvm_settingsParser::CppMethodNameContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitC(eversafe_llvm_settingsParser::CContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCInternalData(eversafe_llvm_settingsParser::CInternalDataContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFunctionFormat(eversafe_llvm_settingsParser::FunctionFormatContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitBlock(eversafe_llvm_settingsParser::BlockContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitSetApplyFormat(eversafe_llvm_settingsParser::SetApplyFormatContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitSetObfusKeys(eversafe_llvm_settingsParser::SetObfusKeysContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitAddRemoveApplyForamt(eversafe_llvm_settingsParser::AddRemoveApplyForamtContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitAddRemoveObfusKeys(eversafe_llvm_settingsParser::AddRemoveObfusKeysContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitAddRemoveObfusKey(eversafe_llvm_settingsParser::AddRemoveObfusKeyContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitAllNoneObfusKey(eversafe_llvm_settingsParser::AllNoneObfusKeyContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitSepar(eversafe_llvm_settingsParser::SeparContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitDuplicateKeyword(eversafe_llvm_settingsParser::DuplicateKeywordContext *ctx) override {
    return visitChildren(ctx);
  }


};

}  // namespace antlrcpptest
