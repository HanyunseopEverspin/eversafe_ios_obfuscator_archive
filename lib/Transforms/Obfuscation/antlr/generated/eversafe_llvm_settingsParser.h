
// Generated from eversafe_llvm_settings.g4 by ANTLR 4.7.1

#pragma once


#include "antlr4-runtime.h"


namespace antlrcpptest {


class  eversafe_llvm_settingsParser : public antlr4::Parser {
public:
  enum {
    CPP = 1, OBJC = 2, CC = 3, CLASS = 4, NAMESPACE = 5, METHOD = 6, ADD = 7, 
    SET = 8, REMOVE = 9, FUNCTION = 10, BOGUS = 11, FLATTEN = 12, WRAPPER = 13, 
    BRANCH = 14, SPILT = 15, STRING = 16, DUMMY = 17, ANTIDECOMPILE = 18, 
    SUBSTITUDE = 19, ANTICLASSDUMP = 20, ALL = 21, NON = 22, DWILDCARD = 23, 
    WILDCARD = 24, LBRACK = 25, RBRACK = 26, LBRACE = 27, RBRACE = 28, LPAREN = 29, 
    RPAREN = 30, COMMA = 31, DOT = 32, DCOLON = 33, INNER = 34, SEMICOLON = 35, 
    NAME = 36, CppGlobalMethodName = 37, ObjcMethodName = 38, ObjcGlobalMethodName = 39, 
    CppMethodName = 40, LineComment = 41, WS = 42, ANY = 43
  };

  enum {
    RuleSettings = 0, RuleObjc = 1, RuleObjcInternalData = 2, RuleObjcClassFormat = 3, 
    RuleObjcClassIntetnalData = 4, RuleObjcMethodFormat = 5, RuleObjcGlobalMethodFormat = 6, 
    RuleCpp = 7, RuleCppInternalData = 8, RuleCppClassInternalData = 9, 
    RuleCppSingleAsteriskClassInternalData = 10, RuleCppClassFormat = 11, 
    RuleSingleCppClassFormat = 12, RuleDoulbeCppClassFormat = 13, RuleCppMethodFormat = 14, 
    RuleCppName = 15, RuleSingleAsteriskName = 16, RuleDoubleAsteriskName = 17, 
    RuleCppMethodName = 18, RuleC = 19, RuleCInternalData = 20, RuleFunctionFormat = 21, 
    RuleBlock = 22, RuleSetApplyFormat = 23, RuleSetObfusKeys = 24, RuleAddRemoveApplyForamt = 25, 
    RuleAddRemoveObfusKeys = 26, RuleAddRemoveObfusKey = 27, RuleAllNoneObfusKey = 28, 
    RuleSepar = 29, RuleDuplicateKeyword = 30
  };

  eversafe_llvm_settingsParser(antlr4::TokenStream *input);
  ~eversafe_llvm_settingsParser();

  virtual std::string getGrammarFileName() const override;
  virtual const antlr4::atn::ATN& getATN() const override { return _atn; };
  virtual const std::vector<std::string>& getTokenNames() const override { return _tokenNames; }; // deprecated: use vocabulary instead.
  virtual const std::vector<std::string>& getRuleNames() const override;
  virtual antlr4::dfa::Vocabulary& getVocabulary() const override;


  class SettingsContext;
  class ObjcContext;
  class ObjcInternalDataContext;
  class ObjcClassFormatContext;
  class ObjcClassIntetnalDataContext;
  class ObjcMethodFormatContext;
  class ObjcGlobalMethodFormatContext;
  class CppContext;
  class CppInternalDataContext;
  class CppClassInternalDataContext;
  class CppSingleAsteriskClassInternalDataContext;
  class CppClassFormatContext;
  class SingleCppClassFormatContext;
  class DoulbeCppClassFormatContext;
  class CppMethodFormatContext;
  class CppNameContext;
  class SingleAsteriskNameContext;
  class DoubleAsteriskNameContext;
  class CppMethodNameContext;
  class CContext;
  class CInternalDataContext;
  class FunctionFormatContext;
  class BlockContext;
  class SetApplyFormatContext;
  class SetObfusKeysContext;
  class AddRemoveApplyForamtContext;
  class AddRemoveObfusKeysContext;
  class AddRemoveObfusKeyContext;
  class AllNoneObfusKeyContext;
  class SeparContext;
  class DuplicateKeywordContext; 

  class  SettingsContext : public antlr4::ParserRuleContext {
  public:
    SettingsContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    ObjcContext *objc();
    CppContext *cpp();
    CContext *c();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  SettingsContext* settings();

  class  ObjcContext : public antlr4::ParserRuleContext {
  public:
    ObjcContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *OBJC();
    antlr4::tree::TerminalNode *LBRACE();
    antlr4::tree::TerminalNode *RBRACE();
    SetApplyFormatContext *setApplyFormat();
    ObjcInternalDataContext *objcInternalData();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ObjcContext* objc();

  class  ObjcInternalDataContext : public antlr4::ParserRuleContext {
  public:
    ObjcInternalDataContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<ObjcGlobalMethodFormatContext *> objcGlobalMethodFormat();
    ObjcGlobalMethodFormatContext* objcGlobalMethodFormat(size_t i);
    std::vector<ObjcClassFormatContext *> objcClassFormat();
    ObjcClassFormatContext* objcClassFormat(size_t i);

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ObjcInternalDataContext* objcInternalData();

  class  ObjcClassFormatContext : public antlr4::ParserRuleContext {
  public:
    ObjcClassFormatContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *CLASS();
    antlr4::tree::TerminalNode *LBRACE();
    ObjcClassIntetnalDataContext *objcClassIntetnalData();
    antlr4::tree::TerminalNode *RBRACE();
    antlr4::tree::TerminalNode *NAME();
    DuplicateKeywordContext *duplicateKeyword();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ObjcClassFormatContext* objcClassFormat();

  class  ObjcClassIntetnalDataContext : public antlr4::ParserRuleContext {
  public:
    ObjcClassIntetnalDataContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<ObjcMethodFormatContext *> objcMethodFormat();
    ObjcMethodFormatContext* objcMethodFormat(size_t i);
    std::vector<SetApplyFormatContext *> setApplyFormat();
    SetApplyFormatContext* setApplyFormat(size_t i);
    std::vector<AddRemoveApplyForamtContext *> addRemoveApplyForamt();
    AddRemoveApplyForamtContext* addRemoveApplyForamt(size_t i);

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ObjcClassIntetnalDataContext* objcClassIntetnalData();

  class  ObjcMethodFormatContext : public antlr4::ParserRuleContext {
  public:
    ObjcMethodFormatContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *METHOD();
    BlockContext *block();
    antlr4::tree::TerminalNode *ObjcGlobalMethodName();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ObjcMethodFormatContext* objcMethodFormat();

  class  ObjcGlobalMethodFormatContext : public antlr4::ParserRuleContext {
  public:
    ObjcGlobalMethodFormatContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *METHOD();
    antlr4::tree::TerminalNode *ObjcGlobalMethodName();
    BlockContext *block();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ObjcGlobalMethodFormatContext* objcGlobalMethodFormat();

  class  CppContext : public antlr4::ParserRuleContext {
  public:
    CppContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *CPP();
    antlr4::tree::TerminalNode *LBRACE();
    antlr4::tree::TerminalNode *RBRACE();
    SetApplyFormatContext *setApplyFormat();
    std::vector<CppInternalDataContext *> cppInternalData();
    CppInternalDataContext* cppInternalData(size_t i);

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  CppContext* cpp();

  class  CppInternalDataContext : public antlr4::ParserRuleContext {
  public:
    CppInternalDataContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    CppMethodFormatContext *cppMethodFormat();
    CppClassFormatContext *cppClassFormat();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  CppInternalDataContext* cppInternalData();

  class  CppClassInternalDataContext : public antlr4::ParserRuleContext {
  public:
    CppClassInternalDataContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<CppClassFormatContext *> cppClassFormat();
    CppClassFormatContext* cppClassFormat(size_t i);
    std::vector<CppMethodFormatContext *> cppMethodFormat();
    CppMethodFormatContext* cppMethodFormat(size_t i);
    std::vector<SetApplyFormatContext *> setApplyFormat();
    SetApplyFormatContext* setApplyFormat(size_t i);
    std::vector<AddRemoveApplyForamtContext *> addRemoveApplyForamt();
    AddRemoveApplyForamtContext* addRemoveApplyForamt(size_t i);

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  CppClassInternalDataContext* cppClassInternalData();

  class  CppSingleAsteriskClassInternalDataContext : public antlr4::ParserRuleContext {
  public:
    CppSingleAsteriskClassInternalDataContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<CppMethodFormatContext *> cppMethodFormat();
    CppMethodFormatContext* cppMethodFormat(size_t i);
    std::vector<SetApplyFormatContext *> setApplyFormat();
    SetApplyFormatContext* setApplyFormat(size_t i);
    std::vector<AddRemoveApplyForamtContext *> addRemoveApplyForamt();
    AddRemoveApplyForamtContext* addRemoveApplyForamt(size_t i);

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  CppSingleAsteriskClassInternalDataContext* cppSingleAsteriskClassInternalData();

  class  CppClassFormatContext : public antlr4::ParserRuleContext {
  public:
    CppClassFormatContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    SingleCppClassFormatContext *singleCppClassFormat();
    DoulbeCppClassFormatContext *doulbeCppClassFormat();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  CppClassFormatContext* cppClassFormat();

  class  SingleCppClassFormatContext : public antlr4::ParserRuleContext {
  public:
    SingleCppClassFormatContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *NAMESPACE();
    SingleAsteriskNameContext *singleAsteriskName();
    antlr4::tree::TerminalNode *LBRACE();
    CppSingleAsteriskClassInternalDataContext *cppSingleAsteriskClassInternalData();
    antlr4::tree::TerminalNode *RBRACE();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  SingleCppClassFormatContext* singleCppClassFormat();

  class  DoulbeCppClassFormatContext : public antlr4::ParserRuleContext {
  public:
    DoulbeCppClassFormatContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *NAMESPACE();
    DoubleAsteriskNameContext *doubleAsteriskName();
    antlr4::tree::TerminalNode *LBRACE();
    CppClassInternalDataContext *cppClassInternalData();
    antlr4::tree::TerminalNode *RBRACE();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  DoulbeCppClassFormatContext* doulbeCppClassFormat();

  class  CppMethodFormatContext : public antlr4::ParserRuleContext {
  public:
    CppMethodFormatContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *METHOD();
    BlockContext *block();
    CppMethodNameContext *cppMethodName();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  CppMethodFormatContext* cppMethodFormat();

  class  CppNameContext : public antlr4::ParserRuleContext {
  public:
    CppNameContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    SingleAsteriskNameContext *singleAsteriskName();
    DoubleAsteriskNameContext *doubleAsteriskName();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  CppNameContext* cppName();

  class  SingleAsteriskNameContext : public antlr4::ParserRuleContext {
  public:
    SingleAsteriskNameContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<antlr4::tree::TerminalNode *> NAME();
    antlr4::tree::TerminalNode* NAME(size_t i);
    std::vector<DuplicateKeywordContext *> duplicateKeyword();
    DuplicateKeywordContext* duplicateKeyword(size_t i);
    std::vector<antlr4::tree::TerminalNode *> DCOLON();
    antlr4::tree::TerminalNode* DCOLON(size_t i);
    antlr4::tree::TerminalNode *WILDCARD();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  SingleAsteriskNameContext* singleAsteriskName();

  class  DoubleAsteriskNameContext : public antlr4::ParserRuleContext {
  public:
    DoubleAsteriskNameContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<antlr4::tree::TerminalNode *> NAME();
    antlr4::tree::TerminalNode* NAME(size_t i);
    std::vector<DuplicateKeywordContext *> duplicateKeyword();
    DuplicateKeywordContext* duplicateKeyword(size_t i);
    std::vector<antlr4::tree::TerminalNode *> DCOLON();
    antlr4::tree::TerminalNode* DCOLON(size_t i);
    antlr4::tree::TerminalNode *DWILDCARD();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  DoubleAsteriskNameContext* doubleAsteriskName();

  class  CppMethodNameContext : public antlr4::ParserRuleContext {
  public:
    CppMethodNameContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<antlr4::tree::TerminalNode *> NAME();
    antlr4::tree::TerminalNode* NAME(size_t i);
    std::vector<DuplicateKeywordContext *> duplicateKeyword();
    DuplicateKeywordContext* duplicateKeyword(size_t i);
    std::vector<antlr4::tree::TerminalNode *> DCOLON();
    antlr4::tree::TerminalNode* DCOLON(size_t i);

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  CppMethodNameContext* cppMethodName();

  class  CContext : public antlr4::ParserRuleContext {
  public:
    CContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *CC();
    antlr4::tree::TerminalNode *LBRACE();
    antlr4::tree::TerminalNode *RBRACE();
    SetApplyFormatContext *setApplyFormat();
    CInternalDataContext *cInternalData();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  CContext* c();

  class  CInternalDataContext : public antlr4::ParserRuleContext {
  public:
    CInternalDataContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<FunctionFormatContext *> functionFormat();
    FunctionFormatContext* functionFormat(size_t i);
    std::vector<SetApplyFormatContext *> setApplyFormat();
    SetApplyFormatContext* setApplyFormat(size_t i);
    std::vector<AddRemoveApplyForamtContext *> addRemoveApplyForamt();
    AddRemoveApplyForamtContext* addRemoveApplyForamt(size_t i);

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  CInternalDataContext* cInternalData();

  class  FunctionFormatContext : public antlr4::ParserRuleContext {
  public:
    FunctionFormatContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *FUNCTION();
    BlockContext *block();
    antlr4::tree::TerminalNode *NAME();
    DuplicateKeywordContext *duplicateKeyword();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  FunctionFormatContext* functionFormat();

  class  BlockContext : public antlr4::ParserRuleContext {
  public:
    BlockContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *LBRACE();
    antlr4::tree::TerminalNode *RBRACE();
    std::vector<SetApplyFormatContext *> setApplyFormat();
    SetApplyFormatContext* setApplyFormat(size_t i);
    std::vector<AddRemoveApplyForamtContext *> addRemoveApplyForamt();
    AddRemoveApplyForamtContext* addRemoveApplyForamt(size_t i);

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  BlockContext* block();

  class  SetApplyFormatContext : public antlr4::ParserRuleContext {
  public:
    SetApplyFormatContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    SetObfusKeysContext *setObfusKeys();
    antlr4::tree::TerminalNode *SEMICOLON();
    antlr4::tree::TerminalNode *SET();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  SetApplyFormatContext* setApplyFormat();

  class  SetObfusKeysContext : public antlr4::ParserRuleContext {
  public:
    SetObfusKeysContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    AllNoneObfusKeyContext *allNoneObfusKey();
    std::vector<AddRemoveObfusKeyContext *> addRemoveObfusKey();
    AddRemoveObfusKeyContext* addRemoveObfusKey(size_t i);
    std::vector<antlr4::tree::TerminalNode *> COMMA();
    antlr4::tree::TerminalNode* COMMA(size_t i);

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  SetObfusKeysContext* setObfusKeys();

  class  AddRemoveApplyForamtContext : public antlr4::ParserRuleContext {
  public:
    AddRemoveApplyForamtContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    AddRemoveObfusKeysContext *addRemoveObfusKeys();
    antlr4::tree::TerminalNode *SEMICOLON();
    antlr4::tree::TerminalNode *REMOVE();
    antlr4::tree::TerminalNode *ADD();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  AddRemoveApplyForamtContext* addRemoveApplyForamt();

  class  AddRemoveObfusKeysContext : public antlr4::ParserRuleContext {
  public:
    AddRemoveObfusKeysContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<AddRemoveObfusKeyContext *> addRemoveObfusKey();
    AddRemoveObfusKeyContext* addRemoveObfusKey(size_t i);
    std::vector<antlr4::tree::TerminalNode *> COMMA();
    antlr4::tree::TerminalNode* COMMA(size_t i);

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  AddRemoveObfusKeysContext* addRemoveObfusKeys();

  class  AddRemoveObfusKeyContext : public antlr4::ParserRuleContext {
  public:
    AddRemoveObfusKeyContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *ANTICLASSDUMP();
    antlr4::tree::TerminalNode *ANTIDECOMPILE();
    antlr4::tree::TerminalNode *DUMMY();
    antlr4::tree::TerminalNode *BOGUS();
    antlr4::tree::TerminalNode *FLATTEN();
    antlr4::tree::TerminalNode *WRAPPER();
    antlr4::tree::TerminalNode *BRANCH();
    antlr4::tree::TerminalNode *SPILT();
    antlr4::tree::TerminalNode *STRING();
    antlr4::tree::TerminalNode *SUBSTITUDE();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  AddRemoveObfusKeyContext* addRemoveObfusKey();

  class  AllNoneObfusKeyContext : public antlr4::ParserRuleContext {
  public:
    AllNoneObfusKeyContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *ALL();
    antlr4::tree::TerminalNode *NON();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  AllNoneObfusKeyContext* allNoneObfusKey();

  class  SeparContext : public antlr4::ParserRuleContext {
  public:
    SeparContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *DCOLON();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  SeparContext* separ();

  class  DuplicateKeywordContext : public antlr4::ParserRuleContext {
  public:
    DuplicateKeywordContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *NON();
    antlr4::tree::TerminalNode *ALL();
    antlr4::tree::TerminalNode *ANTICLASSDUMP();
    antlr4::tree::TerminalNode *ANTIDECOMPILE();
    antlr4::tree::TerminalNode *DUMMY();
    antlr4::tree::TerminalNode *FUNCTION();
    antlr4::tree::TerminalNode *CPP();
    antlr4::tree::TerminalNode *OBJC();
    antlr4::tree::TerminalNode *CC();
    antlr4::tree::TerminalNode *CLASS();
    antlr4::tree::TerminalNode *ADD();
    antlr4::tree::TerminalNode *SET();
    antlr4::tree::TerminalNode *REMOVE();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  DuplicateKeywordContext* duplicateKeyword();


private:
  static std::vector<antlr4::dfa::DFA> _decisionToDFA;
  static antlr4::atn::PredictionContextCache _sharedContextCache;
  static std::vector<std::string> _ruleNames;
  static std::vector<std::string> _tokenNames;

  static std::vector<std::string> _literalNames;
  static std::vector<std::string> _symbolicNames;
  static antlr4::dfa::Vocabulary _vocabulary;
  static antlr4::atn::ATN _atn;
  static std::vector<uint16_t> _serializedATN;


  struct Initializer {
    Initializer();
  };
  static Initializer _init;
};

}  // namespace antlrcpptest
