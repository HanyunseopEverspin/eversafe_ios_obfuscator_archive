
// Generated from eversafe_llvm_settings.g4 by ANTLR 4.7.1

#pragma once


#include "antlr4-runtime.h"
#include "eversafe_llvm_settingsParser.h"


namespace antlrcpptest {

/**
 * This interface defines an abstract listener for a parse tree produced by eversafe_llvm_settingsParser.
 */
class  eversafe_llvm_settingsListener : public antlr4::tree::ParseTreeListener {
public:

  virtual void enterSettings(eversafe_llvm_settingsParser::SettingsContext *ctx) = 0;
  virtual void exitSettings(eversafe_llvm_settingsParser::SettingsContext *ctx) = 0;

  virtual void enterObjc(eversafe_llvm_settingsParser::ObjcContext *ctx) = 0;
  virtual void exitObjc(eversafe_llvm_settingsParser::ObjcContext *ctx) = 0;

  virtual void enterObjcInternalData(eversafe_llvm_settingsParser::ObjcInternalDataContext *ctx) = 0;
  virtual void exitObjcInternalData(eversafe_llvm_settingsParser::ObjcInternalDataContext *ctx) = 0;

  virtual void enterObjcClassFormat(eversafe_llvm_settingsParser::ObjcClassFormatContext *ctx) = 0;
  virtual void exitObjcClassFormat(eversafe_llvm_settingsParser::ObjcClassFormatContext *ctx) = 0;

  virtual void enterObjcClassIntetnalData(eversafe_llvm_settingsParser::ObjcClassIntetnalDataContext *ctx) = 0;
  virtual void exitObjcClassIntetnalData(eversafe_llvm_settingsParser::ObjcClassIntetnalDataContext *ctx) = 0;

  virtual void enterObjcMethodFormat(eversafe_llvm_settingsParser::ObjcMethodFormatContext *ctx) = 0;
  virtual void exitObjcMethodFormat(eversafe_llvm_settingsParser::ObjcMethodFormatContext *ctx) = 0;

  virtual void enterObjcGlobalMethodFormat(eversafe_llvm_settingsParser::ObjcGlobalMethodFormatContext *ctx) = 0;
  virtual void exitObjcGlobalMethodFormat(eversafe_llvm_settingsParser::ObjcGlobalMethodFormatContext *ctx) = 0;

  virtual void enterCpp(eversafe_llvm_settingsParser::CppContext *ctx) = 0;
  virtual void exitCpp(eversafe_llvm_settingsParser::CppContext *ctx) = 0;

  virtual void enterCppInternalData(eversafe_llvm_settingsParser::CppInternalDataContext *ctx) = 0;
  virtual void exitCppInternalData(eversafe_llvm_settingsParser::CppInternalDataContext *ctx) = 0;

  virtual void enterCppClassInternalData(eversafe_llvm_settingsParser::CppClassInternalDataContext *ctx) = 0;
  virtual void exitCppClassInternalData(eversafe_llvm_settingsParser::CppClassInternalDataContext *ctx) = 0;

  virtual void enterCppSingleAsteriskClassInternalData(eversafe_llvm_settingsParser::CppSingleAsteriskClassInternalDataContext *ctx) = 0;
  virtual void exitCppSingleAsteriskClassInternalData(eversafe_llvm_settingsParser::CppSingleAsteriskClassInternalDataContext *ctx) = 0;

  virtual void enterCppClassFormat(eversafe_llvm_settingsParser::CppClassFormatContext *ctx) = 0;
  virtual void exitCppClassFormat(eversafe_llvm_settingsParser::CppClassFormatContext *ctx) = 0;

  virtual void enterSingleCppClassFormat(eversafe_llvm_settingsParser::SingleCppClassFormatContext *ctx) = 0;
  virtual void exitSingleCppClassFormat(eversafe_llvm_settingsParser::SingleCppClassFormatContext *ctx) = 0;

  virtual void enterDoulbeCppClassFormat(eversafe_llvm_settingsParser::DoulbeCppClassFormatContext *ctx) = 0;
  virtual void exitDoulbeCppClassFormat(eversafe_llvm_settingsParser::DoulbeCppClassFormatContext *ctx) = 0;

  virtual void enterCppMethodFormat(eversafe_llvm_settingsParser::CppMethodFormatContext *ctx) = 0;
  virtual void exitCppMethodFormat(eversafe_llvm_settingsParser::CppMethodFormatContext *ctx) = 0;

  virtual void enterCppName(eversafe_llvm_settingsParser::CppNameContext *ctx) = 0;
  virtual void exitCppName(eversafe_llvm_settingsParser::CppNameContext *ctx) = 0;

  virtual void enterSingleAsteriskName(eversafe_llvm_settingsParser::SingleAsteriskNameContext *ctx) = 0;
  virtual void exitSingleAsteriskName(eversafe_llvm_settingsParser::SingleAsteriskNameContext *ctx) = 0;

  virtual void enterDoubleAsteriskName(eversafe_llvm_settingsParser::DoubleAsteriskNameContext *ctx) = 0;
  virtual void exitDoubleAsteriskName(eversafe_llvm_settingsParser::DoubleAsteriskNameContext *ctx) = 0;

  virtual void enterCppMethodName(eversafe_llvm_settingsParser::CppMethodNameContext *ctx) = 0;
  virtual void exitCppMethodName(eversafe_llvm_settingsParser::CppMethodNameContext *ctx) = 0;

  virtual void enterC(eversafe_llvm_settingsParser::CContext *ctx) = 0;
  virtual void exitC(eversafe_llvm_settingsParser::CContext *ctx) = 0;

  virtual void enterCInternalData(eversafe_llvm_settingsParser::CInternalDataContext *ctx) = 0;
  virtual void exitCInternalData(eversafe_llvm_settingsParser::CInternalDataContext *ctx) = 0;

  virtual void enterFunctionFormat(eversafe_llvm_settingsParser::FunctionFormatContext *ctx) = 0;
  virtual void exitFunctionFormat(eversafe_llvm_settingsParser::FunctionFormatContext *ctx) = 0;

  virtual void enterBlock(eversafe_llvm_settingsParser::BlockContext *ctx) = 0;
  virtual void exitBlock(eversafe_llvm_settingsParser::BlockContext *ctx) = 0;

  virtual void enterSetApplyFormat(eversafe_llvm_settingsParser::SetApplyFormatContext *ctx) = 0;
  virtual void exitSetApplyFormat(eversafe_llvm_settingsParser::SetApplyFormatContext *ctx) = 0;

  virtual void enterSetObfusKeys(eversafe_llvm_settingsParser::SetObfusKeysContext *ctx) = 0;
  virtual void exitSetObfusKeys(eversafe_llvm_settingsParser::SetObfusKeysContext *ctx) = 0;

  virtual void enterAddRemoveApplyForamt(eversafe_llvm_settingsParser::AddRemoveApplyForamtContext *ctx) = 0;
  virtual void exitAddRemoveApplyForamt(eversafe_llvm_settingsParser::AddRemoveApplyForamtContext *ctx) = 0;

  virtual void enterAddRemoveObfusKeys(eversafe_llvm_settingsParser::AddRemoveObfusKeysContext *ctx) = 0;
  virtual void exitAddRemoveObfusKeys(eversafe_llvm_settingsParser::AddRemoveObfusKeysContext *ctx) = 0;

  virtual void enterAddRemoveObfusKey(eversafe_llvm_settingsParser::AddRemoveObfusKeyContext *ctx) = 0;
  virtual void exitAddRemoveObfusKey(eversafe_llvm_settingsParser::AddRemoveObfusKeyContext *ctx) = 0;

  virtual void enterAllNoneObfusKey(eversafe_llvm_settingsParser::AllNoneObfusKeyContext *ctx) = 0;
  virtual void exitAllNoneObfusKey(eversafe_llvm_settingsParser::AllNoneObfusKeyContext *ctx) = 0;

  virtual void enterSepar(eversafe_llvm_settingsParser::SeparContext *ctx) = 0;
  virtual void exitSepar(eversafe_llvm_settingsParser::SeparContext *ctx) = 0;

  virtual void enterDuplicateKeyword(eversafe_llvm_settingsParser::DuplicateKeywordContext *ctx) = 0;
  virtual void exitDuplicateKeyword(eversafe_llvm_settingsParser::DuplicateKeywordContext *ctx) = 0;


};

}  // namespace antlrcpptest
