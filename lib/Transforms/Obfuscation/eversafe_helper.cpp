//
//  eversafe_helper.cpp
//  antlrcpp-demo
//
//  Created by yunseop han on 07/11/2017.
//  Copyright © 2017 Dan McLaughlin. All rights reserved.
//

#include "llvm/Transforms/Obfuscation/eversafe_helper.hpp"




EversafeHelper::EversafeHelper(){

}

bool EversafeHelper::isApply(std::string moduleName, ObfuscationFlag oF){
    if(settings->filePath == "") return false;
    return settings->isApply(moduleName,oF);

}
