//
//  eversafe_helper.hpp
//  antlrcpp-demo
//
//  Created by yunseop han on 07/11/2017.
//  Copyright © 2017 Dan McLaughlin. All rights reserved.
//

#ifndef eversafe_helper_hpp
#define eversafe_helper_hpp

#include "llvm/Transforms/Obfuscation/eversafe_settings.hpp"


class EversafeHelper{
private:
    SettingsMain* settings = nullptr;
    EversafeHelper();
public:
    bool isApply(std::string moduleName, ObfuscationFlag oF);

    static EversafeHelper& instance(){
        static EversafeHelper* instance = new EversafeHelper();
        return *instance;
    }

    void settingInit(std::string filePath){
      if(settings == nullptr)
         settings = new SettingsMain(filePath);
    }

};

#endif /* eversafe_helper_hpp */
