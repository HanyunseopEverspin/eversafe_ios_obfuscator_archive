//
//  eversafe_settings.cpp
//  antlrcpp-demo
//
//  Copyright © 2017 Dan McLaughlin. All rights reserved.
//

#include "llvm/Transforms/Obfuscation/eversafe_settings.hpp"

#include "antlr4-runtime.h"
#include "generated/eversafe_llvm_settingsBaseListener.h"
#include "generated/eversafe_llvm_settingsLexer.h"
#include "generated/eversafe_llvm_settingsParser.h"
#include "llvm/Support/raw_ostream.h"
#include <cxxabi.h>
#include <string.h>

// TODO :FIXME namespace 이름 변경..
using namespace antlrcpptest;
using namespace antlr4;

SettingsTrie *objcRootNode;
SettingsTrie *cppRootNode;
SettingsTrie *cRootNode;
void Tokenize(const std::string &str, std::vector<std::string> &tokens,
              const std::string &delimiters = " ");
class EversafeBaseListener : public eversafe_llvm_settingsBaseListener {

private:
  int applyType;
  SettingsTrie *currentNode;
  std::string nameStack;
  std::string cppNameStack;

  ~EversafeBaseListener() override {}
  void enterObjc(eversafe_llvm_settingsParser::ObjcContext * /*ctx*/) override {
    currentNode = objcRootNode;
  }
  void enterObjcClassFormat(
      eversafe_llvm_settingsParser::ObjcClassFormatContext *ctx) override {

    nameStack = ctx->NAME()->getText();
    nameStack = ctx->NAME() != nullptr ? ctx->NAME()->getText() : ctx->duplicateKeyword()->getText();
    currentNode =
        new SettingsTrie(nameStack, eversafe_llvm_settingsParser::CLASS,
                         eversafe_llvm_settingsParser::OBJC);
    if (currentNode->fullNameSearchNode(
            objcRootNode, currentNode->originalFullName) != nullptr)
      currentNode = currentNode->fullNameSearchNode(
          objcRootNode, currentNode->originalFullName);

    objcRootNode->settingInsert(objcRootNode, currentNode);
  }

  void enterObjcMethodFormat(
      eversafe_llvm_settingsParser::ObjcMethodFormatContext *ctx) override {

    std::string globalMethodName = ctx->ObjcGlobalMethodName()->getText();
    std::string methodNewName = "";
    methodNewName.append(globalMethodName.substr(0, 2));
    methodNewName.append(nameStack);
    methodNewName.append(" ");
    methodNewName.append(globalMethodName.substr(2, globalMethodName.size()));

    currentNode =
        new SettingsTrie(methodNewName, eversafe_llvm_settingsParser::METHOD,
                         eversafe_llvm_settingsParser::OBJC);
    if (currentNode->fullNameSearchNode(
            objcRootNode, currentNode->originalFullName) != nullptr)
      currentNode = currentNode->fullNameSearchNode(
          objcRootNode, currentNode->originalFullName);

    objcRootNode->settingInsert(objcRootNode, currentNode);
  }
  void exitObjcClassFormat(
      eversafe_llvm_settingsParser::ObjcClassFormatContext * /*ctx*/) override {
    nameStack = "";
  }
  void enterObjcGlobalMethodFormat(
      eversafe_llvm_settingsParser::ObjcGlobalMethodFormatContext *ctx)
      override {
    std::string globalMethodName = ctx->ObjcGlobalMethodName()->getText();
    currentNode =
        new SettingsTrie(globalMethodName, eversafe_llvm_settingsParser::METHOD,
                         eversafe_llvm_settingsParser::OBJC);
    if (currentNode->fullNameSearchNode(
            objcRootNode, currentNode->originalFullName) != nullptr)
      currentNode = currentNode->fullNameSearchNode(
          objcRootNode, currentNode->originalFullName);

    objcRootNode->settingInsert(objcRootNode, currentNode);
  }
  void exitObjcGlobalMethodFormat(
      eversafe_llvm_settingsParser::ObjcGlobalMethodFormatContext * /*ctx*/)
      override {
    nameStack = "";
  }
  void enterSetApplyFormat(
      eversafe_llvm_settingsParser::SetApplyFormatContext *ctx) override {

    std::vector<std::string> applyFlagVector;

    if (ctx->setObfusKeys()->allNoneObfusKey() != nullptr)
      applyFlagVector.push_back(
          ctx->setObfusKeys()->allNoneObfusKey()->getText());
    for (size_t i = 0; i < ctx->setObfusKeys()->addRemoveObfusKey().size();
         i++) {
      applyFlagVector.push_back(
          ctx->setObfusKeys()->addRemoveObfusKey(i)->getText());
    }
    currentNode->setApply(applyFlagVector);
  }

  virtual void
  enterCpp(eversafe_llvm_settingsParser::CppContext * /*ctx*/) override {
    currentNode = cppRootNode;
  }

  virtual void enterSingleCppClassFormat(
      eversafe_llvm_settingsParser::SingleCppClassFormatContext *ctx) override {
    cppNameStack = nameStack;
    if (cppNameStack != "") {
      cppNameStack += "::";
    }

    std::string tempName = ctx->singleAsteriskName()->getText();
    std::vector<std::string> v;
    Tokenize(tempName, v, "::");

    for (size_t i = 1; i < v.size() - 1; i++) {
      cppNameStack += "::";
      cppNameStack += v.at(i);
    }
    if (ctx->singleAsteriskName()->WILDCARD()) {
      if (cppNameStack.size() < 5 ||
          cppNameStack.substr(cppNameStack.length() - 5, 3) !=
              "::*") { // ClassName::*::* 방지( 마지막에 ::*로 끝나는지 확인)
        if (ctx->singleAsteriskName()->NAME(0) != nullptr)
          cppNameStack += "::";
        cppNameStack += ctx->singleAsteriskName()->WILDCARD()->getText();
      } else {
        exit(-1);
      }
    }
    currentNode =
        new SettingsTrie(cppNameStack, eversafe_llvm_settingsParser::CLASS,
                         eversafe_llvm_settingsParser::CPP);
    if (currentNode->fullNameSearchNode(
            cppRootNode, currentNode->originalFullName) != nullptr)
      currentNode = currentNode->fullNameSearchNode(
          cppRootNode, currentNode->originalFullName);
    cppRootNode->settingInsert(cppRootNode, currentNode);
  }
  virtual void exitSingleCppClassFormat(
      eversafe_llvm_settingsParser::SingleCppClassFormatContext * /*ctx*/)
      override {
    cppNameStack = "";
  }

  virtual void enterDoulbeCppClassFormat(
      eversafe_llvm_settingsParser::DoulbeCppClassFormatContext *ctx) override {

    if (nameStack != "") {
      nameStack += "::";
    }
    std::string tempName = ctx->doubleAsteriskName()->getText();
    std::vector<std::string> v;
    Tokenize(tempName, v, "::");
    nameStack += v.at(0);
    for (size_t i = 1; i < v.size() - 1; i++) {
      nameStack += "::";
      nameStack += v.at(i);
    }
    currentNode =
        new SettingsTrie(nameStack, eversafe_llvm_settingsParser::CLASS,
                         eversafe_llvm_settingsParser::CPP);
    if (currentNode->fullNameSearchNode(
            cppRootNode, currentNode->originalFullName) != nullptr)
      currentNode = currentNode->fullNameSearchNode(
          cppRootNode, currentNode->originalFullName);
    cppRootNode->settingInsert(cppRootNode, currentNode);
  }
  virtual void exitDoulbeCppClassFormat(
      eversafe_llvm_settingsParser::DoulbeCppClassFormatContext * /*ctx*/)
      override {
    nameStack = "";
  }

  virtual void enterCppMethodFormat(
      eversafe_llvm_settingsParser::CppMethodFormatContext *ctx) override {
    std::string tempName = cppNameStack != "" ? cppNameStack : nameStack;
    if (tempName != "")
      tempName += "::";
    if (ctx->cppMethodName() != nullptr) {
      tempName += ctx->cppMethodName()->getText();
    }

    currentNode =
        new SettingsTrie(tempName, eversafe_llvm_settingsParser::METHOD,
                         eversafe_llvm_settingsParser::CPP);
    if (currentNode->fullNameSearchNode(
            cppRootNode, currentNode->originalFullName) != nullptr)
      currentNode = currentNode->fullNameSearchNode(
          cppRootNode, currentNode->originalFullName);

    cppRootNode->settingInsert(cppRootNode, currentNode);
  }

  void enterAddRemoveApplyForamt(
      eversafe_llvm_settingsParser::AddRemoveApplyForamtContext *ctx) override {
    if (ctx->ADD() != nullptr) {
      applyType = eversafe_llvm_settingsParser::ADD;
    }
    if (ctx->REMOVE() != nullptr) {
      applyType = eversafe_llvm_settingsParser::REMOVE;
    }
  }

  void enterAddRemoveObfusKeys(
      eversafe_llvm_settingsParser::AddRemoveObfusKeysContext *ctx) override {
    std::vector<std::string> applyFlagVector;
    for (size_t i = 0; i < ctx->addRemoveObfusKey().size(); i++) {
      applyFlagVector.push_back(ctx->addRemoveObfusKey(i)->getText());
    }
    currentNode->addRemoveApply(applyType, applyFlagVector);
  }

  void enterC(eversafe_llvm_settingsParser::CContext * /*ctx*/) override {
    currentNode = cRootNode;
  }
  void enterFunctionFormat(
      eversafe_llvm_settingsParser::FunctionFormatContext *ctx) override {
    std::string tempName = ctx->NAME() != nullptr
                               ? ctx->NAME()->getText()
                               : ctx->duplicateKeyword()->getText();
    currentNode =
        new SettingsTrie(tempName, eversafe_llvm_settingsParser::FUNCTION,
                         eversafe_llvm_settingsParser::CC);
    cRootNode->settingInsert(cRootNode, currentNode);
  }

  virtual void enterSettings(
      eversafe_llvm_settingsParser::SettingsContext * /*ctx*/) override {}
  virtual void exitSettings(
      eversafe_llvm_settingsParser::SettingsContext * /*ctx*/) override {}
  virtual void
  exitObjc(eversafe_llvm_settingsParser::ObjcContext * /*ctx*/) override {}
  virtual void enterObjcInternalData(
      eversafe_llvm_settingsParser::ObjcInternalDataContext * /*ctx*/)
      override {}
  virtual void exitObjcInternalData(
      eversafe_llvm_settingsParser::ObjcInternalDataContext * /*ctx*/)
      override {}
  virtual void enterObjcClassIntetnalData(
      eversafe_llvm_settingsParser::ObjcClassIntetnalDataContext * /*ctx*/)
      override {}
  virtual void exitObjcClassIntetnalData(
      eversafe_llvm_settingsParser::ObjcClassIntetnalDataContext * /*ctx*/)
      override {}
  virtual void exitObjcMethodFormat(
      eversafe_llvm_settingsParser::ObjcMethodFormatContext * /*ctx*/)
      override {}
  virtual void
  exitC(eversafe_llvm_settingsParser::CContext * /*ctx*/) override {}
  virtual void enterCInternalData(
      eversafe_llvm_settingsParser::CInternalDataContext * /*ctx*/) override {}
  virtual void exitCInternalData(
      eversafe_llvm_settingsParser::CInternalDataContext * /*ctx*/) override {}
  virtual void exitFunctionFormat(
      eversafe_llvm_settingsParser::FunctionFormatContext * /*ctx*/) override {}
  virtual void
  enterBlock(eversafe_llvm_settingsParser::BlockContext * /*ctx*/) override {}
  virtual void
  exitBlock(eversafe_llvm_settingsParser::BlockContext * /*ctx*/) override {}
  virtual void exitSetApplyFormat(
      eversafe_llvm_settingsParser::SetApplyFormatContext * /*ctx*/) override {}
  virtual void enterSetObfusKeys(
      eversafe_llvm_settingsParser::SetObfusKeysContext * /*ctx*/) override {}
  virtual void exitSetObfusKeys(
      eversafe_llvm_settingsParser::SetObfusKeysContext * /*ctx*/) override {}
  virtual void exitAddRemoveApplyForamt(
      eversafe_llvm_settingsParser::AddRemoveApplyForamtContext * /*ctx*/)
      override {}
  virtual void exitAddRemoveObfusKeys(
      eversafe_llvm_settingsParser::AddRemoveObfusKeysContext * /*ctx*/)
      override {}
  virtual void enterAddRemoveObfusKey(
      eversafe_llvm_settingsParser::AddRemoveObfusKeyContext * /*ctx*/)
      override {}
  virtual void exitAddRemoveObfusKey(
      eversafe_llvm_settingsParser::AddRemoveObfusKeyContext * /*ctx*/)
      override {}
  virtual void enterAllNoneObfusKey(
      eversafe_llvm_settingsParser::AllNoneObfusKeyContext * /*ctx*/) override {
  }
  virtual void exitAllNoneObfusKey(
      eversafe_llvm_settingsParser::AllNoneObfusKeyContext * /*ctx*/) override {
  }
  virtual void
  enterSepar(eversafe_llvm_settingsParser::SeparContext * /*ctx*/) override {}
  virtual void
  exitSepar(eversafe_llvm_settingsParser::SeparContext * /*ctx*/) override {}
  virtual void enterDuplicateKeyword(
      eversafe_llvm_settingsParser::DuplicateKeywordContext * /*ctx*/)
      override {}
  virtual void exitDuplicateKeyword(
      eversafe_llvm_settingsParser::DuplicateKeywordContext * /*ctx*/)
      override {}
  virtual void enterEveryRule(antlr4::ParserRuleContext * /*ctx*/) override {}
  virtual void exitEveryRule(antlr4::ParserRuleContext * /*ctx*/) override {}
  virtual void visitTerminal(antlr4::tree::TerminalNode * /*node*/) override {}
  virtual void visitErrorNode(antlr4::tree::ErrorNode * /*node*/) override {}
  virtual void enterCppSingleAsteriskClassInternalData(
      eversafe_llvm_settingsParser::CppSingleAsteriskClassInternalDataContext
          * /*ctx*/) override {}
  virtual void exitCppSingleAsteriskClassInternalData(
      eversafe_llvm_settingsParser::CppSingleAsteriskClassInternalDataContext
          * /*ctx*/) override {}
  virtual void exitCppMethodFormat(
      eversafe_llvm_settingsParser::CppMethodFormatContext * /*ctx*/) override {
  }
  virtual void enterCppName(
      eversafe_llvm_settingsParser::CppNameContext * /*ctx*/) override {}
  virtual void
  exitCppName(eversafe_llvm_settingsParser::CppNameContext * /*ctx*/) override {
  }
  virtual void enterSingleAsteriskName(
      eversafe_llvm_settingsParser::SingleAsteriskNameContext * /*ctx*/)
      override {}
  virtual void exitSingleAsteriskName(
      eversafe_llvm_settingsParser::SingleAsteriskNameContext * /*ctx*/)
      override {}
  virtual void enterDoubleAsteriskName(
      eversafe_llvm_settingsParser::DoubleAsteriskNameContext * /*ctx*/)
      override {}
  virtual void exitDoubleAsteriskName(
      eversafe_llvm_settingsParser::DoubleAsteriskNameContext * /*ctx*/)
      override {}
  virtual void enterCppMethodName(
      eversafe_llvm_settingsParser::CppMethodNameContext * /*ctx*/) override {}
  virtual void exitCppMethodName(
      eversafe_llvm_settingsParser::CppMethodNameContext * /*ctx*/) override {}
  virtual void
  exitCpp(eversafe_llvm_settingsParser::CppContext * /*ctx*/) override {}
  virtual void enterCppInternalData(
      eversafe_llvm_settingsParser::CppInternalDataContext * /*ctx*/) override {
  }
  virtual void exitCppInternalData(
      eversafe_llvm_settingsParser::CppInternalDataContext * /*ctx*/) override {
  }
  virtual void enterCppClassInternalData(
      eversafe_llvm_settingsParser::CppClassInternalDataContext * /*ctx*/)
      override {}
  virtual void exitCppClassInternalData(
      eversafe_llvm_settingsParser::CppClassInternalDataContext * /*ctx*/)
      override {}
  virtual void enterCppClassFormat(
      eversafe_llvm_settingsParser::CppClassFormatContext * /*ctx*/) override {}
  virtual void exitCppClassFormat(
      eversafe_llvm_settingsParser::CppClassFormatContext * /*ctx*/) override {}
};

void Tokenize(const std::string &str, std::vector<std::string> &tokens,
              const std::string &delimiters) {
  // 맨 첫 글자가 구분자인 경우 무시
  std::string::size_type lastPos = str.find_first_not_of(delimiters, 0);
  // 구분자가 아닌 첫 글자를 찾는다
  std::string::size_type pos = str.find_first_of(delimiters, lastPos);

  while (std::string::npos != pos || std::string::npos != lastPos) {
    // token을 찾았으니 vector에 추가한다
    tokens.push_back(str.substr(lastPos, pos - lastPos));
    // 구분자를 뛰어넘는다.  "not_of"에 주의하라
    lastPos = str.find_first_not_of(delimiters, pos);
    // 다음 구분자가 아닌 글자를 찾는다
    pos = str.find_first_of(delimiters, lastPos);
  }
}

SettingsTrie::SettingsTrie(int settingsType) { // create Root Node
  this->originalFullName = "";
  this->tokenName = "";
  this->parentNode = nullptr;
  this->settingsType = 0;
  this->tokenType = 0;
  this->applyFlag = 0;
  this->settingsType = settingsType;
}

SettingsTrie::SettingsTrie(std::string fullName, int tokenType,
                           int settingsType) {
  this->originalFullName = "";
  this->tokenName = "";
  this->parentNode = nullptr;
  this->settingsType = 0;
  this->tokenType = 0;
  this->applyFlag = 0;
  std::vector<std::string> nameTokens;
  if (settingsType == eversafe_llvm_settingsParser::OBJC &&
      tokenType == eversafe_llvm_settingsParser::METHOD) {
    Tokenize(fullName, nameTokens, "-+[] "); // tokens[0] -> class,
                                             // thokens[1] -> method
                                             // this->originalFullName =
                                             // originalFullName;
  } else if (settingsType == eversafe_llvm_settingsParser::OBJC &&
             tokenType == eversafe_llvm_settingsParser::CLASS) {
    Tokenize(fullName, nameTokens, ": "); // tokens[0] -> class,
                                          // thokens[1] -> method
                                          // this->originalFullName =
                                          // originalFullName;
  } else if (settingsType == eversafe_llvm_settingsParser::CPP) {
    Tokenize(fullName, nameTokens, ":"); // tokens[0] -> class,
                                         // thokens[1] -> method
                                         // this->originalFullName =
                                         // originalFullName;
  } else if (settingsType == eversafe_llvm_settingsParser::CC) {
    Tokenize(fullName, nameTokens); // tokens[0] -> class, thokens[1] ->
                                    // method  this->originalFullName =
                                    // originalFullName;
  }

  this->tokenName = nameTokens[nameTokens.size() - 1];
  this->originalFullName = fullName;
  this->tokenType = tokenType;
  this->settingsType = settingsType;

  if (this->tokenName == "**") {
    this->tokenName = nameTokens[nameTokens.size() - 2];
    this->originalFullName =
        fullName.substr(0, fullName.size() - fullName.find("::**"));
  }
}

void SettingsTrie::addRemoveApply(size_t addRemoveFlag,
                                  std::vector<std::string> applyFlagVector) {
  this->applyFlag = this->parentNode->applyFlag;
  if (addRemoveFlag == eversafe_llvm_settingsParser::REMOVE) {
    for (auto pos = applyFlagVector.begin(); pos < applyFlagVector.end();
         pos++) {
      if (!strcasecmp(pos->c_str(), "ANTIDECOMPILE")) {
        this->applyFlag &= ~ANTIHEXFLAG;
      } else if (!strcasecmp(pos->c_str(), "DUMMY")) {
        this->applyFlag &= ~DUMMYFLAG;
      }else if (!strcasecmp(pos->c_str(), "BOGUS")) {
        this->applyFlag &= ~BOGUSFLAG;
      }else if (!strcasecmp(pos->c_str(), "FLATTEN")) {
        this->applyFlag &= ~FLATTENFLAG;
      }else if (!strcasecmp(pos->c_str(), "WRAPPER")) {
        this->applyFlag &= ~WRAPPERFLAG;
      }else if (!strcasecmp(pos->c_str(), "BRANCH")) {
        this->applyFlag &= ~BRANCHFLAG;
      }else if (!strcasecmp(pos->c_str(), "SPILT")) {
        this->applyFlag &= ~SPILTFLAG;
      }else if (!strcasecmp(pos->c_str(), "STRING")) {
        this->applyFlag &= ~STRINGFLAG;
      }else if (!strcasecmp(pos->c_str(), "SUBSTITUDE")) {
        this->applyFlag &= ~SUBSTITUDEFLAG;
      }else if (!strcasecmp(pos->c_str(), "SUBSTITUDE")){
        this->applyFlag &= ~ANTICLASSFLAG;
      }
    }
  } else if (addRemoveFlag == eversafe_llvm_settingsParser::ADD) {
    for (auto pos = applyFlagVector.begin(); pos < applyFlagVector.end();
         pos++) {
      if (!strcasecmp(pos->c_str(), "ANTIDECOMPILE")) {
        this->applyFlag |= ANTIHEXFLAG;
      } else if (!strcasecmp(pos->c_str(), "DUMMY")) {
        this->applyFlag |= DUMMYFLAG;
      }else if (!strcasecmp(pos->c_str(), "BOGUS")) {
        this->applyFlag |= BOGUSFLAG;
      }else if (!strcasecmp(pos->c_str(), "FLATTEN")) {
        this->applyFlag |= FLATTENFLAG;
      }else if (!strcasecmp(pos->c_str(), "WRAPPER")) {
        this->applyFlag |= WRAPPERFLAG;
      }else if (!strcasecmp(pos->c_str(), "BRANCH")) {
        this->applyFlag |= BRANCHFLAG;
      }else if (!strcasecmp(pos->c_str(), "SPILT")) {
        this->applyFlag |= SPILTFLAG;
      }else if (!strcasecmp(pos->c_str(), "STRING")) {
        this->applyFlag |= STRINGFLAG;
      }else if (!strcasecmp(pos->c_str(), "SUBSTITUDE")) {
        this->applyFlag |= SUBSTITUDEFLAG;
      }else if(!strcasecmp(pos->c_str(), "SUBSTITUDE")) {
        this->applyFlag |= ANTICLASSFLAG;
      }
    }
  }
}

void SettingsTrie::setApply(std::vector<std::string> applyFlagVector) {
  int tempApplyFlag = 0;
  for (auto pos = applyFlagVector.begin(); pos < applyFlagVector.end(); pos++) {
    if (!strcasecmp(pos->c_str(), "ANTIDECOMPILE")) {
      tempApplyFlag |= ANTIHEXFLAG;
    } else if (!strcasecmp(pos->c_str(), "DUMMY")) {
      tempApplyFlag |= DUMMYFLAG;
    }else if (!strcasecmp(pos->c_str(), "BOGUS")) {
      tempApplyFlag |= BOGUSFLAG;
    }else if (!strcasecmp(pos->c_str(), "FLATTEN")) {
      tempApplyFlag |= FLATTENFLAG;
    }else if (!strcasecmp(pos->c_str(), "WRAPPER")) {
      tempApplyFlag |= WRAPPERFLAG;
    }else if (!strcasecmp(pos->c_str(), "BRANCH")) {
      tempApplyFlag |= BRANCHFLAG;
    }else if (!strcasecmp(pos->c_str(), "SPILT")) {
      tempApplyFlag |= SPILTFLAG;
    }else if (!strcasecmp(pos->c_str(), "STRING")) {
      tempApplyFlag |= STRINGFLAG;
    }else if (!strcasecmp(pos->c_str(), "SUBSTITUDE")) {
      tempApplyFlag |= SUBSTITUDEFLAG;
    }else if (!strcasecmp(pos->c_str(), "ANTICLASSDUMP")) {
      tempApplyFlag |= ANTICLASSFLAG;
    }else if (!strcasecmp(pos->c_str(), "ALL")) {
      tempApplyFlag = DUMMYFLAG  | ANTIHEXFLAG | BOGUSFLAG | FLATTENFLAG | WRAPPERFLAG | BRANCHFLAG | SPILTFLAG | STRINGFLAG | SUBSTITUDEFLAG | ANTICLASSFLAG;
    }else if (!strcasecmp(pos->c_str(), "NONE")) {
      tempApplyFlag = 0;
    }
  }
  this->applyFlag = tempApplyFlag;
}

void SettingsTrie::printNode(SettingsTrie *rootNode) {
  llvm::errs() << "========================================"
               << rootNode->originalFullName
               << "========================================"
               << "\n";
  llvm::errs() << "settingsType : " << rootNode->settingsType << "\n";
  llvm::errs() << "tokenName : " << rootNode->tokenName << "\n";
  llvm::errs() << "tokenType : " << rootNode->tokenType << "\n";
  llvm::errs() << "flag : " << (int)rootNode->applyFlag << "\n";
  if (rootNode->settingsType != eversafe_llvm_settingsParser::CC) {
    llvm::errs() << "thisNode : " << rootNode << "\n";
    llvm::errs() << "parentNode : " << rootNode->parentNode << "\n";
  }
  llvm::errs()
      << "==============================================================="
         "================="
      << "\n";
  for (auto pos = rootNode->child.begin(); pos < rootNode->child.end(); pos++) {
    printNode(*pos);
  }
}

void SettingsTrie::settingInsert(SettingsTrie *rootNode,
                                 SettingsTrie *targetNode) {
  SettingsTrie *cRoot = rootNode;

  switch (this->settingsType) {
  case eversafe_llvm_settingsParser::OBJC: {
    switch (targetNode->tokenType) {
    case eversafe_llvm_settingsParser::CLASS:
    case eversafe_llvm_settingsParser::METHOD: {

      std::vector<std::string> nameTokens;
      Tokenize(targetNode->originalFullName, nameTokens, "-+[] ");

      if (nameTokens.size() != 1 &&
          targetNode->tokenType == eversafe_llvm_settingsParser::METHOD) {
        std::string tempName = targetNode->originalFullName.substr(0, 2);
        tempName.append(nameTokens[1]);
        tempName.append("]");
        targetNode->tokenName = tempName;
      }
      targetNode->parentNode = cRoot;

      for (auto pos = nameTokens.begin(); pos < nameTokens.end(); pos++) {

        if (searchNode(cRoot, *pos) == nullptr) {
          cRoot->child.push_back(targetNode);
        } else {
          cRoot = searchNode(cRoot, *pos);
        }
        targetNode->parentNode = cRoot;
        targetNode->applyFlag = targetNode->parentNode->applyFlag;
      }
    } break;
    default:
      break;
    }
  } break;
  case eversafe_llvm_settingsParser::CPP: {
    std::vector<std::string> nameTokens;
    Tokenize(targetNode->originalFullName, nameTokens, ":");
    std::string str;
    for (auto pos = nameTokens.begin(); pos < nameTokens.end(); pos++) {
      str += *pos;
      if (searchNode(cRoot, *pos) == nullptr) {
        SettingsTrie *insertNode = nullptr;

        switch (targetNode->tokenType) {
        case eversafe_llvm_settingsParser::METHOD: {
          if (searchNode(cRoot, "*") != nullptr &&
              targetNode->originalFullName.find("*") != std::string::npos)
            cRoot = searchNode(cRoot, "*");
          break;
        }
        default: { break; }
        }
        if (pos == nameTokens.end() - 1) {
          insertNode = targetNode;
        } else {
          switch (targetNode->tokenType) {
          case eversafe_llvm_settingsParser::CLASS: {
            insertNode =
                new SettingsTrie(str, eversafe_llvm_settingsParser::CLASS,
                                 eversafe_llvm_settingsParser::CPP);
            break;
          }
          case eversafe_llvm_settingsParser::METHOD: {
            insertNode =
                new SettingsTrie(str, eversafe_llvm_settingsParser::METHOD,
                                 eversafe_llvm_settingsParser::CPP);
            break;
          }
          default: { break; }
          }
        }
        insertNode->parentNode = cRoot;
        insertNode->applyFlag = insertNode->parentNode->applyFlag;
        cRoot->child.push_back(insertNode);
        cRoot = searchNode(cRoot, *pos);
      } else {
        cRoot = searchNode(cRoot, *pos);
      }

      str += "::";
    }
  } break;
  case eversafe_llvm_settingsParser::CC: {
    if (targetNode->tokenType == eversafe_llvm_settingsParser::FUNCTION) {
      cRoot->child.push_back(targetNode);
    }
    break;
  }
  default: { break; }
  }
  return;
}
SettingsTrie *SettingsTrie::searchNode(SettingsTrie *rootNode,
                                       std::string tokenName) {
  for (std::vector<SettingsTrie *>::iterator pos = rootNode->child.begin();
       pos < rootNode->child.end(); pos++) {
    if (pos[0]->isCompare(tokenName)) {
      return pos[0];
    }
  }
  return nullptr;
}

SettingsTrie *SettingsTrie::fullNameSearchNode(SettingsTrie *rootNode,
                                               std::string fullName) {
  SettingsTrie *cRoot = rootNode;
  switch (rootNode->settingsType) {
  case eversafe_llvm_settingsParser::OBJC: {
    std::vector<std::string> tokenVector;
    Tokenize(fullName, tokenVector, "-+[] ");
    for (auto pos = tokenVector.begin(); pos < tokenVector.end(); pos++) {
      if (searchNode(cRoot, *pos) != nullptr) {
        cRoot = searchNode(cRoot, *pos);
      } else {
        return nullptr;
      }
    }
    return cRoot;
    break;
  }
  case eversafe_llvm_settingsParser::CPP: {
    std::vector<std::string> tokenNames;
    Tokenize(fullName, tokenNames, ":");
    for (auto pos = tokenNames.begin(); pos < tokenNames.end(); pos++) {
      if (searchNode(cRoot, *pos) != nullptr) {
        cRoot = searchNode(cRoot, *pos);
      } else {
        return nullptr;
      }
    }
    return cRoot;
    break;
  }

  case eversafe_llvm_settingsParser::CC:
    break;
  }
  return nullptr;
}

bool SettingsTrie::isCompare(std::string name) {
  if (this->originalFullName == name)
    return true;
  if (this->tokenName == name)
    return true;
  return false;
}

class AntlrErrorListener : public ANTLRErrorListener {
  void syntaxError(Recognizer * /*recognizer*/, Token * /*offendingSymbol*/,
                   size_t line, size_t charPositionInLine,
                   const std::string &msg, std::exception_ptr /*e*/) override {
    llvm::errs() << "Eversafe : [" << line << ":" << charPositionInLine << "]"
                 << msg << "\n";
    exit(-1);
  }
  void reportAmbiguity(Parser * /*recognizer*/, const dfa::DFA & /*dfa*/,
                       size_t /*startIndex*/, size_t /*stopIndex*/,
                       bool /*exact*/, const antlrcpp::BitSet & /*ambigAlts*/,
                       atn::ATNConfigSet * /*configs*/) override {}
  void reportAttemptingFullContext(Parser * /*recognizer*/,
                                   const dfa::DFA & /*dfa*/,
                                   size_t /*startIndex*/, size_t /*stopIndex*/,
                                   const antlrcpp::BitSet & /*conflictingAlts*/,
                                   atn::ATNConfigSet * /*configs*/) override {}
  void reportContextSensitivity(Parser * /*recognizer*/,
                                const dfa::DFA & /*dfa*/, size_t /*startIndex*/,
                                size_t /*stopIndex*/, size_t /*prediction*/,
                                atn::ATNConfigSet * /*configs*/) override {}
};

SettingsMain::SettingsMain(std::string filePath) {
  this->filePath = filePath;
  std::ifstream is(filePath);

  if (!is.good()) { // file check
    if (filePath != "Eversafe_Obfuscation") {
      llvm::errs() << "Eversafe : \"" << filePath << "\" file not found"
                   << "\n";
      exit(-1);
    }
  }

  objcRootNode = new SettingsTrie(eversafe_llvm_settingsParser::OBJC);
  cppRootNode = new SettingsTrie(eversafe_llvm_settingsParser::CPP);
  cRootNode = new SettingsTrie(eversafe_llvm_settingsParser::CC);

  AntlrErrorListener *antlrErrorListener = new AntlrErrorListener();
  EversafeBaseListener *eversafeBaseListene = new EversafeBaseListener();

  ANTLRInputStream input(is);
  eversafe_llvm_settingsLexer lexer(&input);
  lexer.removeErrorListeners();
  lexer.addErrorListener(antlrErrorListener);
  CommonTokenStream tokensStream(&lexer);
  tokensStream.fill();
  eversafe_llvm_settingsParser Parser(&tokensStream);
  Parser.removeErrorListeners();
  Parser.addErrorListener(antlrErrorListener);
  tree::ParseTree *tree = Parser.settings();

  tree::ParseTreeWalker::DEFAULT.walk(eversafeBaseListene, tree);

  // if(objcRoot != nullptr)
  //     objcRoot->printNode(objcRoot);
  // if(cppRoot != nullptr)
  //     cppRoot->printNode(cppRoot);
  // if(ccRoot != nullptr)
  //     ccRoot->printNode(ccRoot);
}

SettingsTrie *SettingsTrie::isApply(std::string moduleName, int settingsType) {
  SettingsTrie *cRoot = this;
  std::vector<std::string> nameTokens;

  switch (settingsType) {
  case eversafe_llvm_settingsParser::OBJC: {

    /**
     우선순위 :
     1. 직접지정된 method
     2. root에서 모든 함수로  지정된 method
     3. 최상위 node
     */
    //            llvm::errs() << "111111" <<"\n";
    Tokenize(moduleName, nameTokens, "-+[] ");
    //            llvm::errs() << "22222" <<"\n";

    for (auto pos = nameTokens.begin(); pos < nameTokens.end(); pos++) {
      if (searchNode(cRoot, *pos) != nullptr) {
        cRoot = searchNode(cRoot, *pos);
      } else if (pos == nameTokens.end() - 1) {
        std::string tempName = moduleName.substr(1, 2);
        tempName.append(nameTokens[2]);
        tempName.append("]");
        if (searchNode(cRoot, tempName) != nullptr) {
          cRoot = searchNode(cRoot, tempName);
        }
      }
    }

    if (cRoot->tokenType != eversafe_llvm_settingsParser::METHOD) {

      std::string tempName = moduleName.substr(1, 2);

      tempName.append(nameTokens[2]);

      tempName.append("]");

      if (searchNode(this, tempName) != nullptr) {

        cRoot = searchNode(this, tempName);
      } else {
      }
    }
    break;
  }
  case eversafe_llvm_settingsParser::CPP: {
    /**
     우선순위 :
     1. 직접지정된 method
     2. whild card
     3. root에서 모든 함수로 지정된 method
     4. 최상위 node에서 method
     5. 최상위 namespace 블럭
     */

    Tokenize(moduleName, nameTokens, ":");
    for (auto pos = nameTokens.begin(); pos < nameTokens.end(); pos++) {
      std::vector<std::string> tempToken;
      Tokenize(*pos, tempToken, "("); // override 미지원으로 인한 변수 삭제
      if (pos->find("(") == std::string::npos) { // method가 아닌경우

        if (searchNode(cRoot, tempToken[0]) != nullptr) {
          cRoot = searchNode(cRoot, tempToken[0]);

        } else {
          break;
        }
      } else {

        if (searchNode(cRoot, "*") != nullptr) {
          cRoot = searchNode(cRoot, "*");
          if (searchNode(cRoot, tempToken[0]) != nullptr) {
            cRoot = searchNode(cRoot, tempToken[0]);
          }
        }
      }
    }
    std::vector<std::string> tempToken;
    std::string temp = *(nameTokens.end() - 1);
    Tokenize(temp, tempToken, "(");
    if (searchNode(cRoot, tempToken[0]) != nullptr) {
      cRoot = searchNode(cRoot, tempToken[0]);
    }
    break;
  }
  case eversafe_llvm_settingsParser::CC: {
    Tokenize(moduleName, nameTokens, "(,) ");
    if (searchNode(cRoot, nameTokens[0]) != nullptr)
      cRoot = searchNode(cRoot, nameTokens[0]);

    break;
  }
  default:
    break;
  }

  return cRoot;
}

bool SettingsMain::isApply(std::string moduleName, ObfuscationFlag oF) {
  SettingsTrie *cRoot = nullptr;
  if (moduleName.substr(1, 2) == "-[" || moduleName.substr(1, 2) == "+[") {
    cRoot =
        objcRootNode->isApply(moduleName, eversafe_llvm_settingsParser::OBJC);
  } else if (moduleName.substr(0, 2) == "_Z") {
    int status = 0;
    char *DemangledName =
        abi::__cxa_demangle(moduleName.c_str(), nullptr, nullptr, &status);
    if (DemangledName == nullptr)
      return false;

    std::string result = DemangledName;
    free(DemangledName);
    if (status != 0)
      return false;

    cRoot = cppRootNode->isApply(result, eversafe_llvm_settingsParser::CPP);

  } else if (moduleName.substr(0, 2) != "__") {
    cRoot = cRootNode->isApply(moduleName, eversafe_llvm_settingsParser::CC);
  } else {
    return false;
  }
  return (cRoot->applyFlag & oF);
}
