#ifndef __UTILS_OBF__
#define __UTILS_OBF__

#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Transforms/Utils/Local.h" // For DemoteRegToStack and DemotePHIToStack
#include "llvm/Transforms/Obfuscation/eversafe_helper.hpp"
#include <stdio.h>

using namespace llvm;

bool isContainAsmCallInst(Function &F);
void fixStack(Function *f);
std::string readAnnotate(Function *f);
bool toObfuscate(bool flag, Function *f, ObfuscationFlag attribute);

#endif
