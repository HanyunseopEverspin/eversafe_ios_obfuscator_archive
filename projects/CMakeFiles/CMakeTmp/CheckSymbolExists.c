/* */
#include <sys/resource.h>

int main(int argc, char** argv)
{
  (void)argv;
#ifndef getrusage
  return ((int*)(&getrusage))[argc];
#else
  (void)argc;
  return 0;
#endif
}

