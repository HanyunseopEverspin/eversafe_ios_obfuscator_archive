add_subdirectory(antlr)
set(ANLTR_LIB_OUTPUT_DIR "${CMAKE_HOME_DIRECTORY}/lib/Transforms/Obfuscation/antlr/dist") # put generated libraries here.
include_directories(
  antlr/src
  antlr
)

add_llvm_library(LLVMObfuscation
  DummyCode.cpp
  AntiHexray.cpp
  FunctionCallObfuscate.cpp
  CryptoUtils.cpp
  BogusControlFlow.cpp
  Substitution.cpp
  Flattening.cpp
  Utils.cpp
  SplitBasicBlocks.cpp
  AntiClassDump.cpp
  StringEncryption.cpp
  IndirectBranch.cpp
  FunctionWrapper.cpp
  Obfuscation.cpp
  eversafe_helper.cpp
  eversafe_settings.cpp
  DEPENDS
  intrinsics_gen
  antlr4_static
  LINK_LIBS
  ${ANLTR_LIB_OUTPUT_DIR}/libantlr4-runtime.a
  )
