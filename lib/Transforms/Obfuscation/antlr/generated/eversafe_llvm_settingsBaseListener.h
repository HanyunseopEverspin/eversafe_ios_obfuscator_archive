
// Generated from eversafe_llvm_settings.g4 by ANTLR 4.7.1

#pragma once


#include "antlr4-runtime.h"
#include "eversafe_llvm_settingsListener.h"


namespace antlrcpptest {

/**
 * This class provides an empty implementation of eversafe_llvm_settingsListener,
 * which can be extended to create a listener which only needs to handle a subset
 * of the available methods.
 */
class  eversafe_llvm_settingsBaseListener : public eversafe_llvm_settingsListener {
public:

  virtual void enterSettings(eversafe_llvm_settingsParser::SettingsContext * /*ctx*/) override { }
  virtual void exitSettings(eversafe_llvm_settingsParser::SettingsContext * /*ctx*/) override { }

  virtual void enterObjc(eversafe_llvm_settingsParser::ObjcContext * /*ctx*/) override { }
  virtual void exitObjc(eversafe_llvm_settingsParser::ObjcContext * /*ctx*/) override { }

  virtual void enterObjcInternalData(eversafe_llvm_settingsParser::ObjcInternalDataContext * /*ctx*/) override { }
  virtual void exitObjcInternalData(eversafe_llvm_settingsParser::ObjcInternalDataContext * /*ctx*/) override { }

  virtual void enterObjcClassFormat(eversafe_llvm_settingsParser::ObjcClassFormatContext * /*ctx*/) override { }
  virtual void exitObjcClassFormat(eversafe_llvm_settingsParser::ObjcClassFormatContext * /*ctx*/) override { }

  virtual void enterObjcClassIntetnalData(eversafe_llvm_settingsParser::ObjcClassIntetnalDataContext * /*ctx*/) override { }
  virtual void exitObjcClassIntetnalData(eversafe_llvm_settingsParser::ObjcClassIntetnalDataContext * /*ctx*/) override { }

  virtual void enterObjcMethodFormat(eversafe_llvm_settingsParser::ObjcMethodFormatContext * /*ctx*/) override { }
  virtual void exitObjcMethodFormat(eversafe_llvm_settingsParser::ObjcMethodFormatContext * /*ctx*/) override { }

  virtual void enterObjcGlobalMethodFormat(eversafe_llvm_settingsParser::ObjcGlobalMethodFormatContext * /*ctx*/) override { }
  virtual void exitObjcGlobalMethodFormat(eversafe_llvm_settingsParser::ObjcGlobalMethodFormatContext * /*ctx*/) override { }

  virtual void enterCpp(eversafe_llvm_settingsParser::CppContext * /*ctx*/) override { }
  virtual void exitCpp(eversafe_llvm_settingsParser::CppContext * /*ctx*/) override { }

  virtual void enterCppInternalData(eversafe_llvm_settingsParser::CppInternalDataContext * /*ctx*/) override { }
  virtual void exitCppInternalData(eversafe_llvm_settingsParser::CppInternalDataContext * /*ctx*/) override { }

  virtual void enterCppClassInternalData(eversafe_llvm_settingsParser::CppClassInternalDataContext * /*ctx*/) override { }
  virtual void exitCppClassInternalData(eversafe_llvm_settingsParser::CppClassInternalDataContext * /*ctx*/) override { }

  virtual void enterCppSingleAsteriskClassInternalData(eversafe_llvm_settingsParser::CppSingleAsteriskClassInternalDataContext * /*ctx*/) override { }
  virtual void exitCppSingleAsteriskClassInternalData(eversafe_llvm_settingsParser::CppSingleAsteriskClassInternalDataContext * /*ctx*/) override { }

  virtual void enterCppClassFormat(eversafe_llvm_settingsParser::CppClassFormatContext * /*ctx*/) override { }
  virtual void exitCppClassFormat(eversafe_llvm_settingsParser::CppClassFormatContext * /*ctx*/) override { }

  virtual void enterSingleCppClassFormat(eversafe_llvm_settingsParser::SingleCppClassFormatContext * /*ctx*/) override { }
  virtual void exitSingleCppClassFormat(eversafe_llvm_settingsParser::SingleCppClassFormatContext * /*ctx*/) override { }

  virtual void enterDoulbeCppClassFormat(eversafe_llvm_settingsParser::DoulbeCppClassFormatContext * /*ctx*/) override { }
  virtual void exitDoulbeCppClassFormat(eversafe_llvm_settingsParser::DoulbeCppClassFormatContext * /*ctx*/) override { }

  virtual void enterCppMethodFormat(eversafe_llvm_settingsParser::CppMethodFormatContext * /*ctx*/) override { }
  virtual void exitCppMethodFormat(eversafe_llvm_settingsParser::CppMethodFormatContext * /*ctx*/) override { }

  virtual void enterCppName(eversafe_llvm_settingsParser::CppNameContext * /*ctx*/) override { }
  virtual void exitCppName(eversafe_llvm_settingsParser::CppNameContext * /*ctx*/) override { }

  virtual void enterSingleAsteriskName(eversafe_llvm_settingsParser::SingleAsteriskNameContext * /*ctx*/) override { }
  virtual void exitSingleAsteriskName(eversafe_llvm_settingsParser::SingleAsteriskNameContext * /*ctx*/) override { }

  virtual void enterDoubleAsteriskName(eversafe_llvm_settingsParser::DoubleAsteriskNameContext * /*ctx*/) override { }
  virtual void exitDoubleAsteriskName(eversafe_llvm_settingsParser::DoubleAsteriskNameContext * /*ctx*/) override { }

  virtual void enterCppMethodName(eversafe_llvm_settingsParser::CppMethodNameContext * /*ctx*/) override { }
  virtual void exitCppMethodName(eversafe_llvm_settingsParser::CppMethodNameContext * /*ctx*/) override { }

  virtual void enterC(eversafe_llvm_settingsParser::CContext * /*ctx*/) override { }
  virtual void exitC(eversafe_llvm_settingsParser::CContext * /*ctx*/) override { }

  virtual void enterCInternalData(eversafe_llvm_settingsParser::CInternalDataContext * /*ctx*/) override { }
  virtual void exitCInternalData(eversafe_llvm_settingsParser::CInternalDataContext * /*ctx*/) override { }

  virtual void enterFunctionFormat(eversafe_llvm_settingsParser::FunctionFormatContext * /*ctx*/) override { }
  virtual void exitFunctionFormat(eversafe_llvm_settingsParser::FunctionFormatContext * /*ctx*/) override { }

  virtual void enterBlock(eversafe_llvm_settingsParser::BlockContext * /*ctx*/) override { }
  virtual void exitBlock(eversafe_llvm_settingsParser::BlockContext * /*ctx*/) override { }

  virtual void enterSetApplyFormat(eversafe_llvm_settingsParser::SetApplyFormatContext * /*ctx*/) override { }
  virtual void exitSetApplyFormat(eversafe_llvm_settingsParser::SetApplyFormatContext * /*ctx*/) override { }

  virtual void enterSetObfusKeys(eversafe_llvm_settingsParser::SetObfusKeysContext * /*ctx*/) override { }
  virtual void exitSetObfusKeys(eversafe_llvm_settingsParser::SetObfusKeysContext * /*ctx*/) override { }

  virtual void enterAddRemoveApplyForamt(eversafe_llvm_settingsParser::AddRemoveApplyForamtContext * /*ctx*/) override { }
  virtual void exitAddRemoveApplyForamt(eversafe_llvm_settingsParser::AddRemoveApplyForamtContext * /*ctx*/) override { }

  virtual void enterAddRemoveObfusKeys(eversafe_llvm_settingsParser::AddRemoveObfusKeysContext * /*ctx*/) override { }
  virtual void exitAddRemoveObfusKeys(eversafe_llvm_settingsParser::AddRemoveObfusKeysContext * /*ctx*/) override { }

  virtual void enterAddRemoveObfusKey(eversafe_llvm_settingsParser::AddRemoveObfusKeyContext * /*ctx*/) override { }
  virtual void exitAddRemoveObfusKey(eversafe_llvm_settingsParser::AddRemoveObfusKeyContext * /*ctx*/) override { }

  virtual void enterAllNoneObfusKey(eversafe_llvm_settingsParser::AllNoneObfusKeyContext * /*ctx*/) override { }
  virtual void exitAllNoneObfusKey(eversafe_llvm_settingsParser::AllNoneObfusKeyContext * /*ctx*/) override { }

  virtual void enterSepar(eversafe_llvm_settingsParser::SeparContext * /*ctx*/) override { }
  virtual void exitSepar(eversafe_llvm_settingsParser::SeparContext * /*ctx*/) override { }

  virtual void enterDuplicateKeyword(eversafe_llvm_settingsParser::DuplicateKeywordContext * /*ctx*/) override { }
  virtual void exitDuplicateKeyword(eversafe_llvm_settingsParser::DuplicateKeywordContext * /*ctx*/) override { }


  virtual void enterEveryRule(antlr4::ParserRuleContext * /*ctx*/) override { }
  virtual void exitEveryRule(antlr4::ParserRuleContext * /*ctx*/) override { }
  virtual void visitTerminal(antlr4::tree::TerminalNode * /*node*/) override { }
  virtual void visitErrorNode(antlr4::tree::ErrorNode * /*node*/) override { }

};

}  // namespace antlrcpptest
