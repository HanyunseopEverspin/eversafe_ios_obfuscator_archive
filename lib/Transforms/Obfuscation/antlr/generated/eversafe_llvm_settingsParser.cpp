
// Generated from eversafe_llvm_settings.g4 by ANTLR 4.7.1


#include "eversafe_llvm_settingsListener.h"
#include "eversafe_llvm_settingsVisitor.h"

#include "eversafe_llvm_settingsParser.h"


using namespace antlrcpp;
using namespace antlrcpptest;
using namespace antlr4;

eversafe_llvm_settingsParser::eversafe_llvm_settingsParser(TokenStream *input) : Parser(input) {
  _interpreter = new atn::ParserATNSimulator(this, _atn, _decisionToDFA, _sharedContextCache);
}

eversafe_llvm_settingsParser::~eversafe_llvm_settingsParser() {
  delete _interpreter;
}

std::string eversafe_llvm_settingsParser::getGrammarFileName() const {
  return "eversafe_llvm_settings.g4";
}

const std::vector<std::string>& eversafe_llvm_settingsParser::getRuleNames() const {
  return _ruleNames;
}

dfa::Vocabulary& eversafe_llvm_settingsParser::getVocabulary() const {
  return _vocabulary;
}


//----------------- SettingsContext ------------------------------------------------------------------

eversafe_llvm_settingsParser::SettingsContext::SettingsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

eversafe_llvm_settingsParser::ObjcContext* eversafe_llvm_settingsParser::SettingsContext::objc() {
  return getRuleContext<eversafe_llvm_settingsParser::ObjcContext>(0);
}

eversafe_llvm_settingsParser::CppContext* eversafe_llvm_settingsParser::SettingsContext::cpp() {
  return getRuleContext<eversafe_llvm_settingsParser::CppContext>(0);
}

eversafe_llvm_settingsParser::CContext* eversafe_llvm_settingsParser::SettingsContext::c() {
  return getRuleContext<eversafe_llvm_settingsParser::CContext>(0);
}


size_t eversafe_llvm_settingsParser::SettingsContext::getRuleIndex() const {
  return eversafe_llvm_settingsParser::RuleSettings;
}

void eversafe_llvm_settingsParser::SettingsContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->enterSettings(this);
}

void eversafe_llvm_settingsParser::SettingsContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->exitSettings(this);
}


antlrcpp::Any eversafe_llvm_settingsParser::SettingsContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = (eversafe_llvm_settingsVisitor *)(visitor))
    return parserVisitor->visitSettings(this);
  else
    return visitor->visitChildren(this);
}

eversafe_llvm_settingsParser::SettingsContext* eversafe_llvm_settingsParser::settings() {
  SettingsContext *_localctx = _tracker.createInstance<SettingsContext>(_ctx, getState());
  enterRule(_localctx, 0, eversafe_llvm_settingsParser::RuleSettings);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(63);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == eversafe_llvm_settingsParser::OBJC) {
      setState(62);
      objc();
    }
    setState(66);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == eversafe_llvm_settingsParser::CPP) {
      setState(65);
      cpp();
    }
    setState(69);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == eversafe_llvm_settingsParser::CC) {
      setState(68);
      c();
    }

  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ObjcContext ------------------------------------------------------------------

eversafe_llvm_settingsParser::ObjcContext::ObjcContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* eversafe_llvm_settingsParser::ObjcContext::OBJC() {
  return getToken(eversafe_llvm_settingsParser::OBJC, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::ObjcContext::LBRACE() {
  return getToken(eversafe_llvm_settingsParser::LBRACE, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::ObjcContext::RBRACE() {
  return getToken(eversafe_llvm_settingsParser::RBRACE, 0);
}

eversafe_llvm_settingsParser::SetApplyFormatContext* eversafe_llvm_settingsParser::ObjcContext::setApplyFormat() {
  return getRuleContext<eversafe_llvm_settingsParser::SetApplyFormatContext>(0);
}

eversafe_llvm_settingsParser::ObjcInternalDataContext* eversafe_llvm_settingsParser::ObjcContext::objcInternalData() {
  return getRuleContext<eversafe_llvm_settingsParser::ObjcInternalDataContext>(0);
}


size_t eversafe_llvm_settingsParser::ObjcContext::getRuleIndex() const {
  return eversafe_llvm_settingsParser::RuleObjc;
}

void eversafe_llvm_settingsParser::ObjcContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->enterObjc(this);
}

void eversafe_llvm_settingsParser::ObjcContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->exitObjc(this);
}


antlrcpp::Any eversafe_llvm_settingsParser::ObjcContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = (eversafe_llvm_settingsVisitor *)(visitor))
    return parserVisitor->visitObjc(this);
  else
    return visitor->visitChildren(this);
}

eversafe_llvm_settingsParser::ObjcContext* eversafe_llvm_settingsParser::objc() {
  ObjcContext *_localctx = _tracker.createInstance<ObjcContext>(_ctx, getState());
  enterRule(_localctx, 2, eversafe_llvm_settingsParser::RuleObjc);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(71);
    match(eversafe_llvm_settingsParser::OBJC);
    setState(72);
    match(eversafe_llvm_settingsParser::LBRACE);
    setState(74);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == eversafe_llvm_settingsParser::SET) {
      setState(73);
      setApplyFormat();
    }
    setState(77);
    _errHandler->sync(this);

    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 4, _ctx)) {
    case 1: {
      setState(76);
      objcInternalData();
      break;
    }

    }
    setState(79);
    match(eversafe_llvm_settingsParser::RBRACE);

  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ObjcInternalDataContext ------------------------------------------------------------------

eversafe_llvm_settingsParser::ObjcInternalDataContext::ObjcInternalDataContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<eversafe_llvm_settingsParser::ObjcGlobalMethodFormatContext *> eversafe_llvm_settingsParser::ObjcInternalDataContext::objcGlobalMethodFormat() {
  return getRuleContexts<eversafe_llvm_settingsParser::ObjcGlobalMethodFormatContext>();
}

eversafe_llvm_settingsParser::ObjcGlobalMethodFormatContext* eversafe_llvm_settingsParser::ObjcInternalDataContext::objcGlobalMethodFormat(size_t i) {
  return getRuleContext<eversafe_llvm_settingsParser::ObjcGlobalMethodFormatContext>(i);
}

std::vector<eversafe_llvm_settingsParser::ObjcClassFormatContext *> eversafe_llvm_settingsParser::ObjcInternalDataContext::objcClassFormat() {
  return getRuleContexts<eversafe_llvm_settingsParser::ObjcClassFormatContext>();
}

eversafe_llvm_settingsParser::ObjcClassFormatContext* eversafe_llvm_settingsParser::ObjcInternalDataContext::objcClassFormat(size_t i) {
  return getRuleContext<eversafe_llvm_settingsParser::ObjcClassFormatContext>(i);
}


size_t eversafe_llvm_settingsParser::ObjcInternalDataContext::getRuleIndex() const {
  return eversafe_llvm_settingsParser::RuleObjcInternalData;
}

void eversafe_llvm_settingsParser::ObjcInternalDataContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->enterObjcInternalData(this);
}

void eversafe_llvm_settingsParser::ObjcInternalDataContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->exitObjcInternalData(this);
}


antlrcpp::Any eversafe_llvm_settingsParser::ObjcInternalDataContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = (eversafe_llvm_settingsVisitor *)(visitor))
    return parserVisitor->visitObjcInternalData(this);
  else
    return visitor->visitChildren(this);
}

eversafe_llvm_settingsParser::ObjcInternalDataContext* eversafe_llvm_settingsParser::objcInternalData() {
  ObjcInternalDataContext *_localctx = _tracker.createInstance<ObjcInternalDataContext>(_ctx, getState());
  enterRule(_localctx, 4, eversafe_llvm_settingsParser::RuleObjcInternalData);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(85);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 6, _ctx);
    while (alt != 1 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1 + 1) {
        setState(83);
        _errHandler->sync(this);
        switch (_input->LA(1)) {
          case eversafe_llvm_settingsParser::METHOD: {
            setState(81);
            objcGlobalMethodFormat();
            break;
          }

          case eversafe_llvm_settingsParser::CLASS: {
            setState(82);
            objcClassFormat();
            break;
          }

        default:
          throw NoViableAltException(this);
        }
      }
      setState(87);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 6, _ctx);
    }

  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ObjcClassFormatContext ------------------------------------------------------------------

eversafe_llvm_settingsParser::ObjcClassFormatContext::ObjcClassFormatContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* eversafe_llvm_settingsParser::ObjcClassFormatContext::CLASS() {
  return getToken(eversafe_llvm_settingsParser::CLASS, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::ObjcClassFormatContext::LBRACE() {
  return getToken(eversafe_llvm_settingsParser::LBRACE, 0);
}

eversafe_llvm_settingsParser::ObjcClassIntetnalDataContext* eversafe_llvm_settingsParser::ObjcClassFormatContext::objcClassIntetnalData() {
  return getRuleContext<eversafe_llvm_settingsParser::ObjcClassIntetnalDataContext>(0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::ObjcClassFormatContext::RBRACE() {
  return getToken(eversafe_llvm_settingsParser::RBRACE, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::ObjcClassFormatContext::NAME() {
  return getToken(eversafe_llvm_settingsParser::NAME, 0);
}

eversafe_llvm_settingsParser::DuplicateKeywordContext* eversafe_llvm_settingsParser::ObjcClassFormatContext::duplicateKeyword() {
  return getRuleContext<eversafe_llvm_settingsParser::DuplicateKeywordContext>(0);
}


size_t eversafe_llvm_settingsParser::ObjcClassFormatContext::getRuleIndex() const {
  return eversafe_llvm_settingsParser::RuleObjcClassFormat;
}

void eversafe_llvm_settingsParser::ObjcClassFormatContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->enterObjcClassFormat(this);
}

void eversafe_llvm_settingsParser::ObjcClassFormatContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->exitObjcClassFormat(this);
}


antlrcpp::Any eversafe_llvm_settingsParser::ObjcClassFormatContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = (eversafe_llvm_settingsVisitor *)(visitor))
    return parserVisitor->visitObjcClassFormat(this);
  else
    return visitor->visitChildren(this);
}

eversafe_llvm_settingsParser::ObjcClassFormatContext* eversafe_llvm_settingsParser::objcClassFormat() {
  ObjcClassFormatContext *_localctx = _tracker.createInstance<ObjcClassFormatContext>(_ctx, getState());
  enterRule(_localctx, 6, eversafe_llvm_settingsParser::RuleObjcClassFormat);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(88);
    match(eversafe_llvm_settingsParser::CLASS);
    setState(91);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case eversafe_llvm_settingsParser::NAME: {
        setState(89);
        match(eversafe_llvm_settingsParser::NAME);
        break;
      }

      case eversafe_llvm_settingsParser::CPP:
      case eversafe_llvm_settingsParser::OBJC:
      case eversafe_llvm_settingsParser::CC:
      case eversafe_llvm_settingsParser::CLASS:
      case eversafe_llvm_settingsParser::ADD:
      case eversafe_llvm_settingsParser::SET:
      case eversafe_llvm_settingsParser::REMOVE:
      case eversafe_llvm_settingsParser::FUNCTION:
      case eversafe_llvm_settingsParser::DUMMY:
      case eversafe_llvm_settingsParser::ANTIDECOMPILE:
      case eversafe_llvm_settingsParser::ANTICLASSDUMP:
      case eversafe_llvm_settingsParser::ALL:
      case eversafe_llvm_settingsParser::NON: {
        setState(90);
        duplicateKeyword();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
    setState(93);
    match(eversafe_llvm_settingsParser::LBRACE);
    setState(94);
    objcClassIntetnalData();
    setState(95);
    match(eversafe_llvm_settingsParser::RBRACE);

  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ObjcClassIntetnalDataContext ------------------------------------------------------------------

eversafe_llvm_settingsParser::ObjcClassIntetnalDataContext::ObjcClassIntetnalDataContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<eversafe_llvm_settingsParser::ObjcMethodFormatContext *> eversafe_llvm_settingsParser::ObjcClassIntetnalDataContext::objcMethodFormat() {
  return getRuleContexts<eversafe_llvm_settingsParser::ObjcMethodFormatContext>();
}

eversafe_llvm_settingsParser::ObjcMethodFormatContext* eversafe_llvm_settingsParser::ObjcClassIntetnalDataContext::objcMethodFormat(size_t i) {
  return getRuleContext<eversafe_llvm_settingsParser::ObjcMethodFormatContext>(i);
}

std::vector<eversafe_llvm_settingsParser::SetApplyFormatContext *> eversafe_llvm_settingsParser::ObjcClassIntetnalDataContext::setApplyFormat() {
  return getRuleContexts<eversafe_llvm_settingsParser::SetApplyFormatContext>();
}

eversafe_llvm_settingsParser::SetApplyFormatContext* eversafe_llvm_settingsParser::ObjcClassIntetnalDataContext::setApplyFormat(size_t i) {
  return getRuleContext<eversafe_llvm_settingsParser::SetApplyFormatContext>(i);
}

std::vector<eversafe_llvm_settingsParser::AddRemoveApplyForamtContext *> eversafe_llvm_settingsParser::ObjcClassIntetnalDataContext::addRemoveApplyForamt() {
  return getRuleContexts<eversafe_llvm_settingsParser::AddRemoveApplyForamtContext>();
}

eversafe_llvm_settingsParser::AddRemoveApplyForamtContext* eversafe_llvm_settingsParser::ObjcClassIntetnalDataContext::addRemoveApplyForamt(size_t i) {
  return getRuleContext<eversafe_llvm_settingsParser::AddRemoveApplyForamtContext>(i);
}


size_t eversafe_llvm_settingsParser::ObjcClassIntetnalDataContext::getRuleIndex() const {
  return eversafe_llvm_settingsParser::RuleObjcClassIntetnalData;
}

void eversafe_llvm_settingsParser::ObjcClassIntetnalDataContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->enterObjcClassIntetnalData(this);
}

void eversafe_llvm_settingsParser::ObjcClassIntetnalDataContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->exitObjcClassIntetnalData(this);
}


antlrcpp::Any eversafe_llvm_settingsParser::ObjcClassIntetnalDataContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = (eversafe_llvm_settingsVisitor *)(visitor))
    return parserVisitor->visitObjcClassIntetnalData(this);
  else
    return visitor->visitChildren(this);
}

eversafe_llvm_settingsParser::ObjcClassIntetnalDataContext* eversafe_llvm_settingsParser::objcClassIntetnalData() {
  ObjcClassIntetnalDataContext *_localctx = _tracker.createInstance<ObjcClassIntetnalDataContext>(_ctx, getState());
  enterRule(_localctx, 8, eversafe_llvm_settingsParser::RuleObjcClassIntetnalData);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(104);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << eversafe_llvm_settingsParser::METHOD)
      | (1ULL << eversafe_llvm_settingsParser::ADD)
      | (1ULL << eversafe_llvm_settingsParser::SET)
      | (1ULL << eversafe_llvm_settingsParser::REMOVE))) != 0)) {
      setState(102);
      _errHandler->sync(this);
      switch (_input->LA(1)) {
        case eversafe_llvm_settingsParser::METHOD: {
          setState(97);
          objcMethodFormat();
          break;
        }

        case eversafe_llvm_settingsParser::ADD:
        case eversafe_llvm_settingsParser::SET:
        case eversafe_llvm_settingsParser::REMOVE: {
          setState(100);
          _errHandler->sync(this);
          switch (_input->LA(1)) {
            case eversafe_llvm_settingsParser::SET: {
              setState(98);
              setApplyFormat();
              break;
            }

            case eversafe_llvm_settingsParser::ADD:
            case eversafe_llvm_settingsParser::REMOVE: {
              setState(99);
              addRemoveApplyForamt();
              break;
            }

          default:
            throw NoViableAltException(this);
          }
          break;
        }

      default:
        throw NoViableAltException(this);
      }
      setState(106);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }

  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ObjcMethodFormatContext ------------------------------------------------------------------

eversafe_llvm_settingsParser::ObjcMethodFormatContext::ObjcMethodFormatContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* eversafe_llvm_settingsParser::ObjcMethodFormatContext::METHOD() {
  return getToken(eversafe_llvm_settingsParser::METHOD, 0);
}

eversafe_llvm_settingsParser::BlockContext* eversafe_llvm_settingsParser::ObjcMethodFormatContext::block() {
  return getRuleContext<eversafe_llvm_settingsParser::BlockContext>(0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::ObjcMethodFormatContext::ObjcGlobalMethodName() {
  return getToken(eversafe_llvm_settingsParser::ObjcGlobalMethodName, 0);
}


size_t eversafe_llvm_settingsParser::ObjcMethodFormatContext::getRuleIndex() const {
  return eversafe_llvm_settingsParser::RuleObjcMethodFormat;
}

void eversafe_llvm_settingsParser::ObjcMethodFormatContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->enterObjcMethodFormat(this);
}

void eversafe_llvm_settingsParser::ObjcMethodFormatContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->exitObjcMethodFormat(this);
}


antlrcpp::Any eversafe_llvm_settingsParser::ObjcMethodFormatContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = (eversafe_llvm_settingsVisitor *)(visitor))
    return parserVisitor->visitObjcMethodFormat(this);
  else
    return visitor->visitChildren(this);
}

eversafe_llvm_settingsParser::ObjcMethodFormatContext* eversafe_llvm_settingsParser::objcMethodFormat() {
  ObjcMethodFormatContext *_localctx = _tracker.createInstance<ObjcMethodFormatContext>(_ctx, getState());
  enterRule(_localctx, 10, eversafe_llvm_settingsParser::RuleObjcMethodFormat);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(107);
    match(eversafe_llvm_settingsParser::METHOD);

    setState(108);
    match(eversafe_llvm_settingsParser::ObjcGlobalMethodName);
    setState(109);
    block();

  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ObjcGlobalMethodFormatContext ------------------------------------------------------------------

eversafe_llvm_settingsParser::ObjcGlobalMethodFormatContext::ObjcGlobalMethodFormatContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* eversafe_llvm_settingsParser::ObjcGlobalMethodFormatContext::METHOD() {
  return getToken(eversafe_llvm_settingsParser::METHOD, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::ObjcGlobalMethodFormatContext::ObjcGlobalMethodName() {
  return getToken(eversafe_llvm_settingsParser::ObjcGlobalMethodName, 0);
}

eversafe_llvm_settingsParser::BlockContext* eversafe_llvm_settingsParser::ObjcGlobalMethodFormatContext::block() {
  return getRuleContext<eversafe_llvm_settingsParser::BlockContext>(0);
}


size_t eversafe_llvm_settingsParser::ObjcGlobalMethodFormatContext::getRuleIndex() const {
  return eversafe_llvm_settingsParser::RuleObjcGlobalMethodFormat;
}

void eversafe_llvm_settingsParser::ObjcGlobalMethodFormatContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->enterObjcGlobalMethodFormat(this);
}

void eversafe_llvm_settingsParser::ObjcGlobalMethodFormatContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->exitObjcGlobalMethodFormat(this);
}


antlrcpp::Any eversafe_llvm_settingsParser::ObjcGlobalMethodFormatContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = (eversafe_llvm_settingsVisitor *)(visitor))
    return parserVisitor->visitObjcGlobalMethodFormat(this);
  else
    return visitor->visitChildren(this);
}

eversafe_llvm_settingsParser::ObjcGlobalMethodFormatContext* eversafe_llvm_settingsParser::objcGlobalMethodFormat() {
  ObjcGlobalMethodFormatContext *_localctx = _tracker.createInstance<ObjcGlobalMethodFormatContext>(_ctx, getState());
  enterRule(_localctx, 12, eversafe_llvm_settingsParser::RuleObjcGlobalMethodFormat);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(111);
    match(eversafe_llvm_settingsParser::METHOD);
    setState(112);
    match(eversafe_llvm_settingsParser::ObjcGlobalMethodName);
    setState(113);
    block();

  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CppContext ------------------------------------------------------------------

eversafe_llvm_settingsParser::CppContext::CppContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* eversafe_llvm_settingsParser::CppContext::CPP() {
  return getToken(eversafe_llvm_settingsParser::CPP, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::CppContext::LBRACE() {
  return getToken(eversafe_llvm_settingsParser::LBRACE, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::CppContext::RBRACE() {
  return getToken(eversafe_llvm_settingsParser::RBRACE, 0);
}

eversafe_llvm_settingsParser::SetApplyFormatContext* eversafe_llvm_settingsParser::CppContext::setApplyFormat() {
  return getRuleContext<eversafe_llvm_settingsParser::SetApplyFormatContext>(0);
}

std::vector<eversafe_llvm_settingsParser::CppInternalDataContext *> eversafe_llvm_settingsParser::CppContext::cppInternalData() {
  return getRuleContexts<eversafe_llvm_settingsParser::CppInternalDataContext>();
}

eversafe_llvm_settingsParser::CppInternalDataContext* eversafe_llvm_settingsParser::CppContext::cppInternalData(size_t i) {
  return getRuleContext<eversafe_llvm_settingsParser::CppInternalDataContext>(i);
}


size_t eversafe_llvm_settingsParser::CppContext::getRuleIndex() const {
  return eversafe_llvm_settingsParser::RuleCpp;
}

void eversafe_llvm_settingsParser::CppContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->enterCpp(this);
}

void eversafe_llvm_settingsParser::CppContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->exitCpp(this);
}


antlrcpp::Any eversafe_llvm_settingsParser::CppContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = (eversafe_llvm_settingsVisitor *)(visitor))
    return parserVisitor->visitCpp(this);
  else
    return visitor->visitChildren(this);
}

eversafe_llvm_settingsParser::CppContext* eversafe_llvm_settingsParser::cpp() {
  CppContext *_localctx = _tracker.createInstance<CppContext>(_ctx, getState());
  enterRule(_localctx, 14, eversafe_llvm_settingsParser::RuleCpp);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(115);
    match(eversafe_llvm_settingsParser::CPP);
    setState(116);
    match(eversafe_llvm_settingsParser::LBRACE);
    setState(118);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == eversafe_llvm_settingsParser::SET) {
      setState(117);
      setApplyFormat();
    }
    setState(123);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == eversafe_llvm_settingsParser::NAMESPACE

    || _la == eversafe_llvm_settingsParser::METHOD) {
      setState(120);
      cppInternalData();
      setState(125);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(126);
    match(eversafe_llvm_settingsParser::RBRACE);

  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CppInternalDataContext ------------------------------------------------------------------

eversafe_llvm_settingsParser::CppInternalDataContext::CppInternalDataContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

eversafe_llvm_settingsParser::CppMethodFormatContext* eversafe_llvm_settingsParser::CppInternalDataContext::cppMethodFormat() {
  return getRuleContext<eversafe_llvm_settingsParser::CppMethodFormatContext>(0);
}

eversafe_llvm_settingsParser::CppClassFormatContext* eversafe_llvm_settingsParser::CppInternalDataContext::cppClassFormat() {
  return getRuleContext<eversafe_llvm_settingsParser::CppClassFormatContext>(0);
}


size_t eversafe_llvm_settingsParser::CppInternalDataContext::getRuleIndex() const {
  return eversafe_llvm_settingsParser::RuleCppInternalData;
}

void eversafe_llvm_settingsParser::CppInternalDataContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->enterCppInternalData(this);
}

void eversafe_llvm_settingsParser::CppInternalDataContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->exitCppInternalData(this);
}


antlrcpp::Any eversafe_llvm_settingsParser::CppInternalDataContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = (eversafe_llvm_settingsVisitor *)(visitor))
    return parserVisitor->visitCppInternalData(this);
  else
    return visitor->visitChildren(this);
}

eversafe_llvm_settingsParser::CppInternalDataContext* eversafe_llvm_settingsParser::cppInternalData() {
  CppInternalDataContext *_localctx = _tracker.createInstance<CppInternalDataContext>(_ctx, getState());
  enterRule(_localctx, 16, eversafe_llvm_settingsParser::RuleCppInternalData);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(130);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case eversafe_llvm_settingsParser::METHOD: {
        setState(128);
        cppMethodFormat();
        break;
      }

      case eversafe_llvm_settingsParser::NAMESPACE: {
        setState(129);
        cppClassFormat();
        break;
      }

    default:
      throw NoViableAltException(this);
    }

  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CppClassInternalDataContext ------------------------------------------------------------------

eversafe_llvm_settingsParser::CppClassInternalDataContext::CppClassInternalDataContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<eversafe_llvm_settingsParser::CppClassFormatContext *> eversafe_llvm_settingsParser::CppClassInternalDataContext::cppClassFormat() {
  return getRuleContexts<eversafe_llvm_settingsParser::CppClassFormatContext>();
}

eversafe_llvm_settingsParser::CppClassFormatContext* eversafe_llvm_settingsParser::CppClassInternalDataContext::cppClassFormat(size_t i) {
  return getRuleContext<eversafe_llvm_settingsParser::CppClassFormatContext>(i);
}

std::vector<eversafe_llvm_settingsParser::CppMethodFormatContext *> eversafe_llvm_settingsParser::CppClassInternalDataContext::cppMethodFormat() {
  return getRuleContexts<eversafe_llvm_settingsParser::CppMethodFormatContext>();
}

eversafe_llvm_settingsParser::CppMethodFormatContext* eversafe_llvm_settingsParser::CppClassInternalDataContext::cppMethodFormat(size_t i) {
  return getRuleContext<eversafe_llvm_settingsParser::CppMethodFormatContext>(i);
}

std::vector<eversafe_llvm_settingsParser::SetApplyFormatContext *> eversafe_llvm_settingsParser::CppClassInternalDataContext::setApplyFormat() {
  return getRuleContexts<eversafe_llvm_settingsParser::SetApplyFormatContext>();
}

eversafe_llvm_settingsParser::SetApplyFormatContext* eversafe_llvm_settingsParser::CppClassInternalDataContext::setApplyFormat(size_t i) {
  return getRuleContext<eversafe_llvm_settingsParser::SetApplyFormatContext>(i);
}

std::vector<eversafe_llvm_settingsParser::AddRemoveApplyForamtContext *> eversafe_llvm_settingsParser::CppClassInternalDataContext::addRemoveApplyForamt() {
  return getRuleContexts<eversafe_llvm_settingsParser::AddRemoveApplyForamtContext>();
}

eversafe_llvm_settingsParser::AddRemoveApplyForamtContext* eversafe_llvm_settingsParser::CppClassInternalDataContext::addRemoveApplyForamt(size_t i) {
  return getRuleContext<eversafe_llvm_settingsParser::AddRemoveApplyForamtContext>(i);
}


size_t eversafe_llvm_settingsParser::CppClassInternalDataContext::getRuleIndex() const {
  return eversafe_llvm_settingsParser::RuleCppClassInternalData;
}

void eversafe_llvm_settingsParser::CppClassInternalDataContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->enterCppClassInternalData(this);
}

void eversafe_llvm_settingsParser::CppClassInternalDataContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->exitCppClassInternalData(this);
}


antlrcpp::Any eversafe_llvm_settingsParser::CppClassInternalDataContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = (eversafe_llvm_settingsVisitor *)(visitor))
    return parserVisitor->visitCppClassInternalData(this);
  else
    return visitor->visitChildren(this);
}

eversafe_llvm_settingsParser::CppClassInternalDataContext* eversafe_llvm_settingsParser::cppClassInternalData() {
  CppClassInternalDataContext *_localctx = _tracker.createInstance<CppClassInternalDataContext>(_ctx, getState());
  enterRule(_localctx, 18, eversafe_llvm_settingsParser::RuleCppClassInternalData);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(140);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << eversafe_llvm_settingsParser::NAMESPACE)
      | (1ULL << eversafe_llvm_settingsParser::METHOD)
      | (1ULL << eversafe_llvm_settingsParser::ADD)
      | (1ULL << eversafe_llvm_settingsParser::SET)
      | (1ULL << eversafe_llvm_settingsParser::REMOVE))) != 0)) {
      setState(138);
      _errHandler->sync(this);
      switch (_input->LA(1)) {
        case eversafe_llvm_settingsParser::NAMESPACE: {
          setState(132);
          cppClassFormat();
          break;
        }

        case eversafe_llvm_settingsParser::METHOD: {
          setState(133);
          cppMethodFormat();
          break;
        }

        case eversafe_llvm_settingsParser::ADD:
        case eversafe_llvm_settingsParser::SET:
        case eversafe_llvm_settingsParser::REMOVE: {
          setState(136);
          _errHandler->sync(this);
          switch (_input->LA(1)) {
            case eversafe_llvm_settingsParser::SET: {
              setState(134);
              setApplyFormat();
              break;
            }

            case eversafe_llvm_settingsParser::ADD:
            case eversafe_llvm_settingsParser::REMOVE: {
              setState(135);
              addRemoveApplyForamt();
              break;
            }

          default:
            throw NoViableAltException(this);
          }
          break;
        }

      default:
        throw NoViableAltException(this);
      }
      setState(142);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }

  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CppSingleAsteriskClassInternalDataContext ------------------------------------------------------------------

eversafe_llvm_settingsParser::CppSingleAsteriskClassInternalDataContext::CppSingleAsteriskClassInternalDataContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<eversafe_llvm_settingsParser::CppMethodFormatContext *> eversafe_llvm_settingsParser::CppSingleAsteriskClassInternalDataContext::cppMethodFormat() {
  return getRuleContexts<eversafe_llvm_settingsParser::CppMethodFormatContext>();
}

eversafe_llvm_settingsParser::CppMethodFormatContext* eversafe_llvm_settingsParser::CppSingleAsteriskClassInternalDataContext::cppMethodFormat(size_t i) {
  return getRuleContext<eversafe_llvm_settingsParser::CppMethodFormatContext>(i);
}

std::vector<eversafe_llvm_settingsParser::SetApplyFormatContext *> eversafe_llvm_settingsParser::CppSingleAsteriskClassInternalDataContext::setApplyFormat() {
  return getRuleContexts<eversafe_llvm_settingsParser::SetApplyFormatContext>();
}

eversafe_llvm_settingsParser::SetApplyFormatContext* eversafe_llvm_settingsParser::CppSingleAsteriskClassInternalDataContext::setApplyFormat(size_t i) {
  return getRuleContext<eversafe_llvm_settingsParser::SetApplyFormatContext>(i);
}

std::vector<eversafe_llvm_settingsParser::AddRemoveApplyForamtContext *> eversafe_llvm_settingsParser::CppSingleAsteriskClassInternalDataContext::addRemoveApplyForamt() {
  return getRuleContexts<eversafe_llvm_settingsParser::AddRemoveApplyForamtContext>();
}

eversafe_llvm_settingsParser::AddRemoveApplyForamtContext* eversafe_llvm_settingsParser::CppSingleAsteriskClassInternalDataContext::addRemoveApplyForamt(size_t i) {
  return getRuleContext<eversafe_llvm_settingsParser::AddRemoveApplyForamtContext>(i);
}


size_t eversafe_llvm_settingsParser::CppSingleAsteriskClassInternalDataContext::getRuleIndex() const {
  return eversafe_llvm_settingsParser::RuleCppSingleAsteriskClassInternalData;
}

void eversafe_llvm_settingsParser::CppSingleAsteriskClassInternalDataContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->enterCppSingleAsteriskClassInternalData(this);
}

void eversafe_llvm_settingsParser::CppSingleAsteriskClassInternalDataContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->exitCppSingleAsteriskClassInternalData(this);
}


antlrcpp::Any eversafe_llvm_settingsParser::CppSingleAsteriskClassInternalDataContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = (eversafe_llvm_settingsVisitor *)(visitor))
    return parserVisitor->visitCppSingleAsteriskClassInternalData(this);
  else
    return visitor->visitChildren(this);
}

eversafe_llvm_settingsParser::CppSingleAsteriskClassInternalDataContext* eversafe_llvm_settingsParser::cppSingleAsteriskClassInternalData() {
  CppSingleAsteriskClassInternalDataContext *_localctx = _tracker.createInstance<CppSingleAsteriskClassInternalDataContext>(_ctx, getState());
  enterRule(_localctx, 20, eversafe_llvm_settingsParser::RuleCppSingleAsteriskClassInternalData);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(150);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << eversafe_llvm_settingsParser::METHOD)
      | (1ULL << eversafe_llvm_settingsParser::ADD)
      | (1ULL << eversafe_llvm_settingsParser::SET)
      | (1ULL << eversafe_llvm_settingsParser::REMOVE))) != 0)) {
      setState(148);
      _errHandler->sync(this);
      switch (_input->LA(1)) {
        case eversafe_llvm_settingsParser::METHOD: {
          setState(143);
          cppMethodFormat();
          break;
        }

        case eversafe_llvm_settingsParser::ADD:
        case eversafe_llvm_settingsParser::SET:
        case eversafe_llvm_settingsParser::REMOVE: {
          setState(146);
          _errHandler->sync(this);
          switch (_input->LA(1)) {
            case eversafe_llvm_settingsParser::SET: {
              setState(144);
              setApplyFormat();
              break;
            }

            case eversafe_llvm_settingsParser::ADD:
            case eversafe_llvm_settingsParser::REMOVE: {
              setState(145);
              addRemoveApplyForamt();
              break;
            }

          default:
            throw NoViableAltException(this);
          }
          break;
        }

      default:
        throw NoViableAltException(this);
      }
      setState(152);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }

  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CppClassFormatContext ------------------------------------------------------------------

eversafe_llvm_settingsParser::CppClassFormatContext::CppClassFormatContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

eversafe_llvm_settingsParser::SingleCppClassFormatContext* eversafe_llvm_settingsParser::CppClassFormatContext::singleCppClassFormat() {
  return getRuleContext<eversafe_llvm_settingsParser::SingleCppClassFormatContext>(0);
}

eversafe_llvm_settingsParser::DoulbeCppClassFormatContext* eversafe_llvm_settingsParser::CppClassFormatContext::doulbeCppClassFormat() {
  return getRuleContext<eversafe_llvm_settingsParser::DoulbeCppClassFormatContext>(0);
}


size_t eversafe_llvm_settingsParser::CppClassFormatContext::getRuleIndex() const {
  return eversafe_llvm_settingsParser::RuleCppClassFormat;
}

void eversafe_llvm_settingsParser::CppClassFormatContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->enterCppClassFormat(this);
}

void eversafe_llvm_settingsParser::CppClassFormatContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->exitCppClassFormat(this);
}


antlrcpp::Any eversafe_llvm_settingsParser::CppClassFormatContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = (eversafe_llvm_settingsVisitor *)(visitor))
    return parserVisitor->visitCppClassFormat(this);
  else
    return visitor->visitChildren(this);
}

eversafe_llvm_settingsParser::CppClassFormatContext* eversafe_llvm_settingsParser::cppClassFormat() {
  CppClassFormatContext *_localctx = _tracker.createInstance<CppClassFormatContext>(_ctx, getState());
  enterRule(_localctx, 22, eversafe_llvm_settingsParser::RuleCppClassFormat);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(155);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 20, _ctx)) {
    case 1: {
      setState(153);
      singleCppClassFormat();
      break;
    }

    case 2: {
      setState(154);
      doulbeCppClassFormat();
      break;
    }

    }

  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- SingleCppClassFormatContext ------------------------------------------------------------------

eversafe_llvm_settingsParser::SingleCppClassFormatContext::SingleCppClassFormatContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* eversafe_llvm_settingsParser::SingleCppClassFormatContext::NAMESPACE() {
  return getToken(eversafe_llvm_settingsParser::NAMESPACE, 0);
}

eversafe_llvm_settingsParser::SingleAsteriskNameContext* eversafe_llvm_settingsParser::SingleCppClassFormatContext::singleAsteriskName() {
  return getRuleContext<eversafe_llvm_settingsParser::SingleAsteriskNameContext>(0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::SingleCppClassFormatContext::LBRACE() {
  return getToken(eversafe_llvm_settingsParser::LBRACE, 0);
}

eversafe_llvm_settingsParser::CppSingleAsteriskClassInternalDataContext* eversafe_llvm_settingsParser::SingleCppClassFormatContext::cppSingleAsteriskClassInternalData() {
  return getRuleContext<eversafe_llvm_settingsParser::CppSingleAsteriskClassInternalDataContext>(0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::SingleCppClassFormatContext::RBRACE() {
  return getToken(eversafe_llvm_settingsParser::RBRACE, 0);
}


size_t eversafe_llvm_settingsParser::SingleCppClassFormatContext::getRuleIndex() const {
  return eversafe_llvm_settingsParser::RuleSingleCppClassFormat;
}

void eversafe_llvm_settingsParser::SingleCppClassFormatContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->enterSingleCppClassFormat(this);
}

void eversafe_llvm_settingsParser::SingleCppClassFormatContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->exitSingleCppClassFormat(this);
}


antlrcpp::Any eversafe_llvm_settingsParser::SingleCppClassFormatContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = (eversafe_llvm_settingsVisitor *)(visitor))
    return parserVisitor->visitSingleCppClassFormat(this);
  else
    return visitor->visitChildren(this);
}

eversafe_llvm_settingsParser::SingleCppClassFormatContext* eversafe_llvm_settingsParser::singleCppClassFormat() {
  SingleCppClassFormatContext *_localctx = _tracker.createInstance<SingleCppClassFormatContext>(_ctx, getState());
  enterRule(_localctx, 24, eversafe_llvm_settingsParser::RuleSingleCppClassFormat);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(157);
    match(eversafe_llvm_settingsParser::NAMESPACE);
    setState(158);
    singleAsteriskName();
    setState(159);
    match(eversafe_llvm_settingsParser::LBRACE);
    setState(160);
    cppSingleAsteriskClassInternalData();
    setState(161);
    match(eversafe_llvm_settingsParser::RBRACE);

  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- DoulbeCppClassFormatContext ------------------------------------------------------------------

eversafe_llvm_settingsParser::DoulbeCppClassFormatContext::DoulbeCppClassFormatContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* eversafe_llvm_settingsParser::DoulbeCppClassFormatContext::NAMESPACE() {
  return getToken(eversafe_llvm_settingsParser::NAMESPACE, 0);
}

eversafe_llvm_settingsParser::DoubleAsteriskNameContext* eversafe_llvm_settingsParser::DoulbeCppClassFormatContext::doubleAsteriskName() {
  return getRuleContext<eversafe_llvm_settingsParser::DoubleAsteriskNameContext>(0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::DoulbeCppClassFormatContext::LBRACE() {
  return getToken(eversafe_llvm_settingsParser::LBRACE, 0);
}

eversafe_llvm_settingsParser::CppClassInternalDataContext* eversafe_llvm_settingsParser::DoulbeCppClassFormatContext::cppClassInternalData() {
  return getRuleContext<eversafe_llvm_settingsParser::CppClassInternalDataContext>(0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::DoulbeCppClassFormatContext::RBRACE() {
  return getToken(eversafe_llvm_settingsParser::RBRACE, 0);
}


size_t eversafe_llvm_settingsParser::DoulbeCppClassFormatContext::getRuleIndex() const {
  return eversafe_llvm_settingsParser::RuleDoulbeCppClassFormat;
}

void eversafe_llvm_settingsParser::DoulbeCppClassFormatContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->enterDoulbeCppClassFormat(this);
}

void eversafe_llvm_settingsParser::DoulbeCppClassFormatContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->exitDoulbeCppClassFormat(this);
}


antlrcpp::Any eversafe_llvm_settingsParser::DoulbeCppClassFormatContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = (eversafe_llvm_settingsVisitor *)(visitor))
    return parserVisitor->visitDoulbeCppClassFormat(this);
  else
    return visitor->visitChildren(this);
}

eversafe_llvm_settingsParser::DoulbeCppClassFormatContext* eversafe_llvm_settingsParser::doulbeCppClassFormat() {
  DoulbeCppClassFormatContext *_localctx = _tracker.createInstance<DoulbeCppClassFormatContext>(_ctx, getState());
  enterRule(_localctx, 26, eversafe_llvm_settingsParser::RuleDoulbeCppClassFormat);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(163);
    match(eversafe_llvm_settingsParser::NAMESPACE);
    setState(164);
    doubleAsteriskName();
    setState(165);
    match(eversafe_llvm_settingsParser::LBRACE);
    setState(166);
    cppClassInternalData();
    setState(167);
    match(eversafe_llvm_settingsParser::RBRACE);

  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CppMethodFormatContext ------------------------------------------------------------------

eversafe_llvm_settingsParser::CppMethodFormatContext::CppMethodFormatContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* eversafe_llvm_settingsParser::CppMethodFormatContext::METHOD() {
  return getToken(eversafe_llvm_settingsParser::METHOD, 0);
}

eversafe_llvm_settingsParser::BlockContext* eversafe_llvm_settingsParser::CppMethodFormatContext::block() {
  return getRuleContext<eversafe_llvm_settingsParser::BlockContext>(0);
}

eversafe_llvm_settingsParser::CppMethodNameContext* eversafe_llvm_settingsParser::CppMethodFormatContext::cppMethodName() {
  return getRuleContext<eversafe_llvm_settingsParser::CppMethodNameContext>(0);
}


size_t eversafe_llvm_settingsParser::CppMethodFormatContext::getRuleIndex() const {
  return eversafe_llvm_settingsParser::RuleCppMethodFormat;
}

void eversafe_llvm_settingsParser::CppMethodFormatContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->enterCppMethodFormat(this);
}

void eversafe_llvm_settingsParser::CppMethodFormatContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->exitCppMethodFormat(this);
}


antlrcpp::Any eversafe_llvm_settingsParser::CppMethodFormatContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = (eversafe_llvm_settingsVisitor *)(visitor))
    return parserVisitor->visitCppMethodFormat(this);
  else
    return visitor->visitChildren(this);
}

eversafe_llvm_settingsParser::CppMethodFormatContext* eversafe_llvm_settingsParser::cppMethodFormat() {
  CppMethodFormatContext *_localctx = _tracker.createInstance<CppMethodFormatContext>(_ctx, getState());
  enterRule(_localctx, 28, eversafe_llvm_settingsParser::RuleCppMethodFormat);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(169);
    match(eversafe_llvm_settingsParser::METHOD);

    setState(170);
    cppMethodName();
    setState(171);
    block();

  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CppNameContext ------------------------------------------------------------------

eversafe_llvm_settingsParser::CppNameContext::CppNameContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

eversafe_llvm_settingsParser::SingleAsteriskNameContext* eversafe_llvm_settingsParser::CppNameContext::singleAsteriskName() {
  return getRuleContext<eversafe_llvm_settingsParser::SingleAsteriskNameContext>(0);
}

eversafe_llvm_settingsParser::DoubleAsteriskNameContext* eversafe_llvm_settingsParser::CppNameContext::doubleAsteriskName() {
  return getRuleContext<eversafe_llvm_settingsParser::DoubleAsteriskNameContext>(0);
}


size_t eversafe_llvm_settingsParser::CppNameContext::getRuleIndex() const {
  return eversafe_llvm_settingsParser::RuleCppName;
}

void eversafe_llvm_settingsParser::CppNameContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->enterCppName(this);
}

void eversafe_llvm_settingsParser::CppNameContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->exitCppName(this);
}


antlrcpp::Any eversafe_llvm_settingsParser::CppNameContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = (eversafe_llvm_settingsVisitor *)(visitor))
    return parserVisitor->visitCppName(this);
  else
    return visitor->visitChildren(this);
}

eversafe_llvm_settingsParser::CppNameContext* eversafe_llvm_settingsParser::cppName() {
  CppNameContext *_localctx = _tracker.createInstance<CppNameContext>(_ctx, getState());
  enterRule(_localctx, 30, eversafe_llvm_settingsParser::RuleCppName);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(175);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 21, _ctx)) {
    case 1: {
      setState(173);
      singleAsteriskName();
      break;
    }

    case 2: {
      setState(174);
      doubleAsteriskName();
      break;
    }

    }

  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- SingleAsteriskNameContext ------------------------------------------------------------------

eversafe_llvm_settingsParser::SingleAsteriskNameContext::SingleAsteriskNameContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<tree::TerminalNode *> eversafe_llvm_settingsParser::SingleAsteriskNameContext::NAME() {
  return getTokens(eversafe_llvm_settingsParser::NAME);
}

tree::TerminalNode* eversafe_llvm_settingsParser::SingleAsteriskNameContext::NAME(size_t i) {
  return getToken(eversafe_llvm_settingsParser::NAME, i);
}

std::vector<eversafe_llvm_settingsParser::DuplicateKeywordContext *> eversafe_llvm_settingsParser::SingleAsteriskNameContext::duplicateKeyword() {
  return getRuleContexts<eversafe_llvm_settingsParser::DuplicateKeywordContext>();
}

eversafe_llvm_settingsParser::DuplicateKeywordContext* eversafe_llvm_settingsParser::SingleAsteriskNameContext::duplicateKeyword(size_t i) {
  return getRuleContext<eversafe_llvm_settingsParser::DuplicateKeywordContext>(i);
}

std::vector<tree::TerminalNode *> eversafe_llvm_settingsParser::SingleAsteriskNameContext::DCOLON() {
  return getTokens(eversafe_llvm_settingsParser::DCOLON);
}

tree::TerminalNode* eversafe_llvm_settingsParser::SingleAsteriskNameContext::DCOLON(size_t i) {
  return getToken(eversafe_llvm_settingsParser::DCOLON, i);
}

tree::TerminalNode* eversafe_llvm_settingsParser::SingleAsteriskNameContext::WILDCARD() {
  return getToken(eversafe_llvm_settingsParser::WILDCARD, 0);
}


size_t eversafe_llvm_settingsParser::SingleAsteriskNameContext::getRuleIndex() const {
  return eversafe_llvm_settingsParser::RuleSingleAsteriskName;
}

void eversafe_llvm_settingsParser::SingleAsteriskNameContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->enterSingleAsteriskName(this);
}

void eversafe_llvm_settingsParser::SingleAsteriskNameContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->exitSingleAsteriskName(this);
}


antlrcpp::Any eversafe_llvm_settingsParser::SingleAsteriskNameContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = (eversafe_llvm_settingsVisitor *)(visitor))
    return parserVisitor->visitSingleAsteriskName(this);
  else
    return visitor->visitChildren(this);
}

eversafe_llvm_settingsParser::SingleAsteriskNameContext* eversafe_llvm_settingsParser::singleAsteriskName() {
  SingleAsteriskNameContext *_localctx = _tracker.createInstance<SingleAsteriskNameContext>(_ctx, getState());
  enterRule(_localctx, 32, eversafe_llvm_settingsParser::RuleSingleAsteriskName);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    setState(194);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case eversafe_llvm_settingsParser::CPP:
      case eversafe_llvm_settingsParser::OBJC:
      case eversafe_llvm_settingsParser::CC:
      case eversafe_llvm_settingsParser::CLASS:
      case eversafe_llvm_settingsParser::ADD:
      case eversafe_llvm_settingsParser::SET:
      case eversafe_llvm_settingsParser::REMOVE:
      case eversafe_llvm_settingsParser::FUNCTION:
      case eversafe_llvm_settingsParser::DUMMY:
      case eversafe_llvm_settingsParser::ANTIDECOMPILE:
      case eversafe_llvm_settingsParser::ANTICLASSDUMP:
      case eversafe_llvm_settingsParser::ALL:
      case eversafe_llvm_settingsParser::NON:
      case eversafe_llvm_settingsParser::NAME: {
        enterOuterAlt(_localctx, 1);
        setState(179);
        _errHandler->sync(this);
        switch (_input->LA(1)) {
          case eversafe_llvm_settingsParser::NAME: {
            setState(177);
            match(eversafe_llvm_settingsParser::NAME);
            break;
          }

          case eversafe_llvm_settingsParser::CPP:
          case eversafe_llvm_settingsParser::OBJC:
          case eversafe_llvm_settingsParser::CC:
          case eversafe_llvm_settingsParser::CLASS:
          case eversafe_llvm_settingsParser::ADD:
          case eversafe_llvm_settingsParser::SET:
          case eversafe_llvm_settingsParser::REMOVE:
          case eversafe_llvm_settingsParser::FUNCTION:
          case eversafe_llvm_settingsParser::DUMMY:
          case eversafe_llvm_settingsParser::ANTIDECOMPILE:
          case eversafe_llvm_settingsParser::ANTICLASSDUMP:
          case eversafe_llvm_settingsParser::ALL:
          case eversafe_llvm_settingsParser::NON: {
            setState(178);
            duplicateKeyword();
            break;
          }

        default:
          throw NoViableAltException(this);
        }
        setState(188);
        _errHandler->sync(this);
        alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 24, _ctx);
        while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
          if (alt == 1) {
            setState(181);
            match(eversafe_llvm_settingsParser::DCOLON);
            setState(184);
            _errHandler->sync(this);
            switch (_input->LA(1)) {
              case eversafe_llvm_settingsParser::NAME: {
                setState(182);
                match(eversafe_llvm_settingsParser::NAME);
                break;
              }

              case eversafe_llvm_settingsParser::CPP:
              case eversafe_llvm_settingsParser::OBJC:
              case eversafe_llvm_settingsParser::CC:
              case eversafe_llvm_settingsParser::CLASS:
              case eversafe_llvm_settingsParser::ADD:
              case eversafe_llvm_settingsParser::SET:
              case eversafe_llvm_settingsParser::REMOVE:
              case eversafe_llvm_settingsParser::FUNCTION:
              case eversafe_llvm_settingsParser::DUMMY:
              case eversafe_llvm_settingsParser::ANTIDECOMPILE:
              case eversafe_llvm_settingsParser::ANTICLASSDUMP:
              case eversafe_llvm_settingsParser::ALL:
              case eversafe_llvm_settingsParser::NON: {
                setState(183);
                duplicateKeyword();
                break;
              }

            default:
              throw NoViableAltException(this);
            }
          }
          setState(190);
          _errHandler->sync(this);
          alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 24, _ctx);
        }

        setState(191);
        match(eversafe_llvm_settingsParser::DCOLON);
        setState(192);
        match(eversafe_llvm_settingsParser::WILDCARD);
        break;
      }

      case eversafe_llvm_settingsParser::WILDCARD: {
        enterOuterAlt(_localctx, 2);
        setState(193);
        match(eversafe_llvm_settingsParser::WILDCARD);
        break;
      }

    default:
      throw NoViableAltException(this);
    }

  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- DoubleAsteriskNameContext ------------------------------------------------------------------

eversafe_llvm_settingsParser::DoubleAsteriskNameContext::DoubleAsteriskNameContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<tree::TerminalNode *> eversafe_llvm_settingsParser::DoubleAsteriskNameContext::NAME() {
  return getTokens(eversafe_llvm_settingsParser::NAME);
}

tree::TerminalNode* eversafe_llvm_settingsParser::DoubleAsteriskNameContext::NAME(size_t i) {
  return getToken(eversafe_llvm_settingsParser::NAME, i);
}

std::vector<eversafe_llvm_settingsParser::DuplicateKeywordContext *> eversafe_llvm_settingsParser::DoubleAsteriskNameContext::duplicateKeyword() {
  return getRuleContexts<eversafe_llvm_settingsParser::DuplicateKeywordContext>();
}

eversafe_llvm_settingsParser::DuplicateKeywordContext* eversafe_llvm_settingsParser::DoubleAsteriskNameContext::duplicateKeyword(size_t i) {
  return getRuleContext<eversafe_llvm_settingsParser::DuplicateKeywordContext>(i);
}

std::vector<tree::TerminalNode *> eversafe_llvm_settingsParser::DoubleAsteriskNameContext::DCOLON() {
  return getTokens(eversafe_llvm_settingsParser::DCOLON);
}

tree::TerminalNode* eversafe_llvm_settingsParser::DoubleAsteriskNameContext::DCOLON(size_t i) {
  return getToken(eversafe_llvm_settingsParser::DCOLON, i);
}

tree::TerminalNode* eversafe_llvm_settingsParser::DoubleAsteriskNameContext::DWILDCARD() {
  return getToken(eversafe_llvm_settingsParser::DWILDCARD, 0);
}


size_t eversafe_llvm_settingsParser::DoubleAsteriskNameContext::getRuleIndex() const {
  return eversafe_llvm_settingsParser::RuleDoubleAsteriskName;
}

void eversafe_llvm_settingsParser::DoubleAsteriskNameContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->enterDoubleAsteriskName(this);
}

void eversafe_llvm_settingsParser::DoubleAsteriskNameContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->exitDoubleAsteriskName(this);
}


antlrcpp::Any eversafe_llvm_settingsParser::DoubleAsteriskNameContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = (eversafe_llvm_settingsVisitor *)(visitor))
    return parserVisitor->visitDoubleAsteriskName(this);
  else
    return visitor->visitChildren(this);
}

eversafe_llvm_settingsParser::DoubleAsteriskNameContext* eversafe_llvm_settingsParser::doubleAsteriskName() {
  DoubleAsteriskNameContext *_localctx = _tracker.createInstance<DoubleAsteriskNameContext>(_ctx, getState());
  enterRule(_localctx, 34, eversafe_llvm_settingsParser::RuleDoubleAsteriskName);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(198);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case eversafe_llvm_settingsParser::NAME: {
        setState(196);
        match(eversafe_llvm_settingsParser::NAME);
        break;
      }

      case eversafe_llvm_settingsParser::CPP:
      case eversafe_llvm_settingsParser::OBJC:
      case eversafe_llvm_settingsParser::CC:
      case eversafe_llvm_settingsParser::CLASS:
      case eversafe_llvm_settingsParser::ADD:
      case eversafe_llvm_settingsParser::SET:
      case eversafe_llvm_settingsParser::REMOVE:
      case eversafe_llvm_settingsParser::FUNCTION:
      case eversafe_llvm_settingsParser::DUMMY:
      case eversafe_llvm_settingsParser::ANTIDECOMPILE:
      case eversafe_llvm_settingsParser::ANTICLASSDUMP:
      case eversafe_llvm_settingsParser::ALL:
      case eversafe_llvm_settingsParser::NON: {
        setState(197);
        duplicateKeyword();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
    setState(207);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 28, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(200);
        match(eversafe_llvm_settingsParser::DCOLON);
        setState(203);
        _errHandler->sync(this);
        switch (_input->LA(1)) {
          case eversafe_llvm_settingsParser::NAME: {
            setState(201);
            match(eversafe_llvm_settingsParser::NAME);
            break;
          }

          case eversafe_llvm_settingsParser::CPP:
          case eversafe_llvm_settingsParser::OBJC:
          case eversafe_llvm_settingsParser::CC:
          case eversafe_llvm_settingsParser::CLASS:
          case eversafe_llvm_settingsParser::ADD:
          case eversafe_llvm_settingsParser::SET:
          case eversafe_llvm_settingsParser::REMOVE:
          case eversafe_llvm_settingsParser::FUNCTION:
          case eversafe_llvm_settingsParser::DUMMY:
          case eversafe_llvm_settingsParser::ANTIDECOMPILE:
          case eversafe_llvm_settingsParser::ANTICLASSDUMP:
          case eversafe_llvm_settingsParser::ALL:
          case eversafe_llvm_settingsParser::NON: {
            setState(202);
            duplicateKeyword();
            break;
          }

        default:
          throw NoViableAltException(this);
        }
      }
      setState(209);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 28, _ctx);
    }

    setState(210);
    match(eversafe_llvm_settingsParser::DCOLON);
    setState(211);
    match(eversafe_llvm_settingsParser::DWILDCARD);

  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CppMethodNameContext ------------------------------------------------------------------

eversafe_llvm_settingsParser::CppMethodNameContext::CppMethodNameContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<tree::TerminalNode *> eversafe_llvm_settingsParser::CppMethodNameContext::NAME() {
  return getTokens(eversafe_llvm_settingsParser::NAME);
}

tree::TerminalNode* eversafe_llvm_settingsParser::CppMethodNameContext::NAME(size_t i) {
  return getToken(eversafe_llvm_settingsParser::NAME, i);
}

std::vector<eversafe_llvm_settingsParser::DuplicateKeywordContext *> eversafe_llvm_settingsParser::CppMethodNameContext::duplicateKeyword() {
  return getRuleContexts<eversafe_llvm_settingsParser::DuplicateKeywordContext>();
}

eversafe_llvm_settingsParser::DuplicateKeywordContext* eversafe_llvm_settingsParser::CppMethodNameContext::duplicateKeyword(size_t i) {
  return getRuleContext<eversafe_llvm_settingsParser::DuplicateKeywordContext>(i);
}

std::vector<tree::TerminalNode *> eversafe_llvm_settingsParser::CppMethodNameContext::DCOLON() {
  return getTokens(eversafe_llvm_settingsParser::DCOLON);
}

tree::TerminalNode* eversafe_llvm_settingsParser::CppMethodNameContext::DCOLON(size_t i) {
  return getToken(eversafe_llvm_settingsParser::DCOLON, i);
}


size_t eversafe_llvm_settingsParser::CppMethodNameContext::getRuleIndex() const {
  return eversafe_llvm_settingsParser::RuleCppMethodName;
}

void eversafe_llvm_settingsParser::CppMethodNameContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->enterCppMethodName(this);
}

void eversafe_llvm_settingsParser::CppMethodNameContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->exitCppMethodName(this);
}


antlrcpp::Any eversafe_llvm_settingsParser::CppMethodNameContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = (eversafe_llvm_settingsVisitor *)(visitor))
    return parserVisitor->visitCppMethodName(this);
  else
    return visitor->visitChildren(this);
}

eversafe_llvm_settingsParser::CppMethodNameContext* eversafe_llvm_settingsParser::cppMethodName() {
  CppMethodNameContext *_localctx = _tracker.createInstance<CppMethodNameContext>(_ctx, getState());
  enterRule(_localctx, 36, eversafe_llvm_settingsParser::RuleCppMethodName);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(215);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case eversafe_llvm_settingsParser::NAME: {
        setState(213);
        match(eversafe_llvm_settingsParser::NAME);
        break;
      }

      case eversafe_llvm_settingsParser::CPP:
      case eversafe_llvm_settingsParser::OBJC:
      case eversafe_llvm_settingsParser::CC:
      case eversafe_llvm_settingsParser::CLASS:
      case eversafe_llvm_settingsParser::ADD:
      case eversafe_llvm_settingsParser::SET:
      case eversafe_llvm_settingsParser::REMOVE:
      case eversafe_llvm_settingsParser::FUNCTION:
      case eversafe_llvm_settingsParser::DUMMY:
      case eversafe_llvm_settingsParser::ANTIDECOMPILE:
      case eversafe_llvm_settingsParser::ANTICLASSDUMP:
      case eversafe_llvm_settingsParser::ALL:
      case eversafe_llvm_settingsParser::NON: {
        setState(214);
        duplicateKeyword();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
    setState(224);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == eversafe_llvm_settingsParser::DCOLON) {
      setState(217);
      match(eversafe_llvm_settingsParser::DCOLON);
      setState(220);
      _errHandler->sync(this);
      switch (_input->LA(1)) {
        case eversafe_llvm_settingsParser::NAME: {
          setState(218);
          match(eversafe_llvm_settingsParser::NAME);
          break;
        }

        case eversafe_llvm_settingsParser::CPP:
        case eversafe_llvm_settingsParser::OBJC:
        case eversafe_llvm_settingsParser::CC:
        case eversafe_llvm_settingsParser::CLASS:
        case eversafe_llvm_settingsParser::ADD:
        case eversafe_llvm_settingsParser::SET:
        case eversafe_llvm_settingsParser::REMOVE:
        case eversafe_llvm_settingsParser::FUNCTION:
        case eversafe_llvm_settingsParser::DUMMY:
        case eversafe_llvm_settingsParser::ANTIDECOMPILE:
        case eversafe_llvm_settingsParser::ANTICLASSDUMP:
        case eversafe_llvm_settingsParser::ALL:
        case eversafe_llvm_settingsParser::NON: {
          setState(219);
          duplicateKeyword();
          break;
        }

      default:
        throw NoViableAltException(this);
      }
      setState(226);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }

  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CContext ------------------------------------------------------------------

eversafe_llvm_settingsParser::CContext::CContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* eversafe_llvm_settingsParser::CContext::CC() {
  return getToken(eversafe_llvm_settingsParser::CC, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::CContext::LBRACE() {
  return getToken(eversafe_llvm_settingsParser::LBRACE, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::CContext::RBRACE() {
  return getToken(eversafe_llvm_settingsParser::RBRACE, 0);
}

eversafe_llvm_settingsParser::SetApplyFormatContext* eversafe_llvm_settingsParser::CContext::setApplyFormat() {
  return getRuleContext<eversafe_llvm_settingsParser::SetApplyFormatContext>(0);
}

eversafe_llvm_settingsParser::CInternalDataContext* eversafe_llvm_settingsParser::CContext::cInternalData() {
  return getRuleContext<eversafe_llvm_settingsParser::CInternalDataContext>(0);
}


size_t eversafe_llvm_settingsParser::CContext::getRuleIndex() const {
  return eversafe_llvm_settingsParser::RuleC;
}

void eversafe_llvm_settingsParser::CContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->enterC(this);
}

void eversafe_llvm_settingsParser::CContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->exitC(this);
}


antlrcpp::Any eversafe_llvm_settingsParser::CContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = (eversafe_llvm_settingsVisitor *)(visitor))
    return parserVisitor->visitC(this);
  else
    return visitor->visitChildren(this);
}

eversafe_llvm_settingsParser::CContext* eversafe_llvm_settingsParser::c() {
  CContext *_localctx = _tracker.createInstance<CContext>(_ctx, getState());
  enterRule(_localctx, 38, eversafe_llvm_settingsParser::RuleC);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(227);
    match(eversafe_llvm_settingsParser::CC);
    setState(228);
    match(eversafe_llvm_settingsParser::LBRACE);
    setState(230);
    _errHandler->sync(this);

    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 32, _ctx)) {
    case 1: {
      setState(229);
      setApplyFormat();
      break;
    }

    }
    setState(233);
    _errHandler->sync(this);

    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 33, _ctx)) {
    case 1: {
      setState(232);
      cInternalData();
      break;
    }

    }
    setState(235);
    match(eversafe_llvm_settingsParser::RBRACE);

  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CInternalDataContext ------------------------------------------------------------------

eversafe_llvm_settingsParser::CInternalDataContext::CInternalDataContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<eversafe_llvm_settingsParser::FunctionFormatContext *> eversafe_llvm_settingsParser::CInternalDataContext::functionFormat() {
  return getRuleContexts<eversafe_llvm_settingsParser::FunctionFormatContext>();
}

eversafe_llvm_settingsParser::FunctionFormatContext* eversafe_llvm_settingsParser::CInternalDataContext::functionFormat(size_t i) {
  return getRuleContext<eversafe_llvm_settingsParser::FunctionFormatContext>(i);
}

std::vector<eversafe_llvm_settingsParser::SetApplyFormatContext *> eversafe_llvm_settingsParser::CInternalDataContext::setApplyFormat() {
  return getRuleContexts<eversafe_llvm_settingsParser::SetApplyFormatContext>();
}

eversafe_llvm_settingsParser::SetApplyFormatContext* eversafe_llvm_settingsParser::CInternalDataContext::setApplyFormat(size_t i) {
  return getRuleContext<eversafe_llvm_settingsParser::SetApplyFormatContext>(i);
}

std::vector<eversafe_llvm_settingsParser::AddRemoveApplyForamtContext *> eversafe_llvm_settingsParser::CInternalDataContext::addRemoveApplyForamt() {
  return getRuleContexts<eversafe_llvm_settingsParser::AddRemoveApplyForamtContext>();
}

eversafe_llvm_settingsParser::AddRemoveApplyForamtContext* eversafe_llvm_settingsParser::CInternalDataContext::addRemoveApplyForamt(size_t i) {
  return getRuleContext<eversafe_llvm_settingsParser::AddRemoveApplyForamtContext>(i);
}


size_t eversafe_llvm_settingsParser::CInternalDataContext::getRuleIndex() const {
  return eversafe_llvm_settingsParser::RuleCInternalData;
}

void eversafe_llvm_settingsParser::CInternalDataContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->enterCInternalData(this);
}

void eversafe_llvm_settingsParser::CInternalDataContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->exitCInternalData(this);
}


antlrcpp::Any eversafe_llvm_settingsParser::CInternalDataContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = (eversafe_llvm_settingsVisitor *)(visitor))
    return parserVisitor->visitCInternalData(this);
  else
    return visitor->visitChildren(this);
}

eversafe_llvm_settingsParser::CInternalDataContext* eversafe_llvm_settingsParser::cInternalData() {
  CInternalDataContext *_localctx = _tracker.createInstance<CInternalDataContext>(_ctx, getState());
  enterRule(_localctx, 40, eversafe_llvm_settingsParser::RuleCInternalData);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(244);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 36, _ctx);
    while (alt != 1 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1 + 1) {
        setState(242);
        _errHandler->sync(this);
        switch (_input->LA(1)) {
          case eversafe_llvm_settingsParser::FUNCTION: {
            setState(237);
            functionFormat();
            break;
          }

          case eversafe_llvm_settingsParser::ADD:
          case eversafe_llvm_settingsParser::SET:
          case eversafe_llvm_settingsParser::REMOVE: {
            setState(240);
            _errHandler->sync(this);
            switch (_input->LA(1)) {
              case eversafe_llvm_settingsParser::SET: {
                setState(238);
                setApplyFormat();
                break;
              }

              case eversafe_llvm_settingsParser::ADD:
              case eversafe_llvm_settingsParser::REMOVE: {
                setState(239);
                addRemoveApplyForamt();
                break;
              }

            default:
              throw NoViableAltException(this);
            }
            break;
          }

        default:
          throw NoViableAltException(this);
        }
      }
      setState(246);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 36, _ctx);
    }

  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FunctionFormatContext ------------------------------------------------------------------

eversafe_llvm_settingsParser::FunctionFormatContext::FunctionFormatContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* eversafe_llvm_settingsParser::FunctionFormatContext::FUNCTION() {
  return getToken(eversafe_llvm_settingsParser::FUNCTION, 0);
}

eversafe_llvm_settingsParser::BlockContext* eversafe_llvm_settingsParser::FunctionFormatContext::block() {
  return getRuleContext<eversafe_llvm_settingsParser::BlockContext>(0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::FunctionFormatContext::NAME() {
  return getToken(eversafe_llvm_settingsParser::NAME, 0);
}

eversafe_llvm_settingsParser::DuplicateKeywordContext* eversafe_llvm_settingsParser::FunctionFormatContext::duplicateKeyword() {
  return getRuleContext<eversafe_llvm_settingsParser::DuplicateKeywordContext>(0);
}


size_t eversafe_llvm_settingsParser::FunctionFormatContext::getRuleIndex() const {
  return eversafe_llvm_settingsParser::RuleFunctionFormat;
}

void eversafe_llvm_settingsParser::FunctionFormatContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->enterFunctionFormat(this);
}

void eversafe_llvm_settingsParser::FunctionFormatContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->exitFunctionFormat(this);
}


antlrcpp::Any eversafe_llvm_settingsParser::FunctionFormatContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = (eversafe_llvm_settingsVisitor *)(visitor))
    return parserVisitor->visitFunctionFormat(this);
  else
    return visitor->visitChildren(this);
}

eversafe_llvm_settingsParser::FunctionFormatContext* eversafe_llvm_settingsParser::functionFormat() {
  FunctionFormatContext *_localctx = _tracker.createInstance<FunctionFormatContext>(_ctx, getState());
  enterRule(_localctx, 42, eversafe_llvm_settingsParser::RuleFunctionFormat);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(247);
    match(eversafe_llvm_settingsParser::FUNCTION);
    setState(250);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case eversafe_llvm_settingsParser::NAME: {
        setState(248);
        match(eversafe_llvm_settingsParser::NAME);
        break;
      }

      case eversafe_llvm_settingsParser::CPP:
      case eversafe_llvm_settingsParser::OBJC:
      case eversafe_llvm_settingsParser::CC:
      case eversafe_llvm_settingsParser::CLASS:
      case eversafe_llvm_settingsParser::ADD:
      case eversafe_llvm_settingsParser::SET:
      case eversafe_llvm_settingsParser::REMOVE:
      case eversafe_llvm_settingsParser::FUNCTION:
      case eversafe_llvm_settingsParser::DUMMY:
      case eversafe_llvm_settingsParser::ANTIDECOMPILE:
      case eversafe_llvm_settingsParser::ANTICLASSDUMP:
      case eversafe_llvm_settingsParser::ALL:
      case eversafe_llvm_settingsParser::NON: {
        setState(249);
        duplicateKeyword();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
    setState(252);
    block();

  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- BlockContext ------------------------------------------------------------------

eversafe_llvm_settingsParser::BlockContext::BlockContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* eversafe_llvm_settingsParser::BlockContext::LBRACE() {
  return getToken(eversafe_llvm_settingsParser::LBRACE, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::BlockContext::RBRACE() {
  return getToken(eversafe_llvm_settingsParser::RBRACE, 0);
}

std::vector<eversafe_llvm_settingsParser::SetApplyFormatContext *> eversafe_llvm_settingsParser::BlockContext::setApplyFormat() {
  return getRuleContexts<eversafe_llvm_settingsParser::SetApplyFormatContext>();
}

eversafe_llvm_settingsParser::SetApplyFormatContext* eversafe_llvm_settingsParser::BlockContext::setApplyFormat(size_t i) {
  return getRuleContext<eversafe_llvm_settingsParser::SetApplyFormatContext>(i);
}

std::vector<eversafe_llvm_settingsParser::AddRemoveApplyForamtContext *> eversafe_llvm_settingsParser::BlockContext::addRemoveApplyForamt() {
  return getRuleContexts<eversafe_llvm_settingsParser::AddRemoveApplyForamtContext>();
}

eversafe_llvm_settingsParser::AddRemoveApplyForamtContext* eversafe_llvm_settingsParser::BlockContext::addRemoveApplyForamt(size_t i) {
  return getRuleContext<eversafe_llvm_settingsParser::AddRemoveApplyForamtContext>(i);
}


size_t eversafe_llvm_settingsParser::BlockContext::getRuleIndex() const {
  return eversafe_llvm_settingsParser::RuleBlock;
}

void eversafe_llvm_settingsParser::BlockContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->enterBlock(this);
}

void eversafe_llvm_settingsParser::BlockContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->exitBlock(this);
}


antlrcpp::Any eversafe_llvm_settingsParser::BlockContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = (eversafe_llvm_settingsVisitor *)(visitor))
    return parserVisitor->visitBlock(this);
  else
    return visitor->visitChildren(this);
}

eversafe_llvm_settingsParser::BlockContext* eversafe_llvm_settingsParser::block() {
  BlockContext *_localctx = _tracker.createInstance<BlockContext>(_ctx, getState());
  enterRule(_localctx, 44, eversafe_llvm_settingsParser::RuleBlock);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(254);
    match(eversafe_llvm_settingsParser::LBRACE);
    setState(259);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 39, _ctx);
    while (alt != 1 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1 + 1) {
        setState(257);
        _errHandler->sync(this);
        switch (_input->LA(1)) {
          case eversafe_llvm_settingsParser::SET: {
            setState(255);
            setApplyFormat();
            break;
          }

          case eversafe_llvm_settingsParser::ADD:
          case eversafe_llvm_settingsParser::REMOVE: {
            setState(256);
            addRemoveApplyForamt();
            break;
          }

        default:
          throw NoViableAltException(this);
        }
      }
      setState(261);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 39, _ctx);
    }
    setState(262);
    match(eversafe_llvm_settingsParser::RBRACE);

  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- SetApplyFormatContext ------------------------------------------------------------------

eversafe_llvm_settingsParser::SetApplyFormatContext::SetApplyFormatContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

eversafe_llvm_settingsParser::SetObfusKeysContext* eversafe_llvm_settingsParser::SetApplyFormatContext::setObfusKeys() {
  return getRuleContext<eversafe_llvm_settingsParser::SetObfusKeysContext>(0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::SetApplyFormatContext::SEMICOLON() {
  return getToken(eversafe_llvm_settingsParser::SEMICOLON, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::SetApplyFormatContext::SET() {
  return getToken(eversafe_llvm_settingsParser::SET, 0);
}


size_t eversafe_llvm_settingsParser::SetApplyFormatContext::getRuleIndex() const {
  return eversafe_llvm_settingsParser::RuleSetApplyFormat;
}

void eversafe_llvm_settingsParser::SetApplyFormatContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->enterSetApplyFormat(this);
}

void eversafe_llvm_settingsParser::SetApplyFormatContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->exitSetApplyFormat(this);
}


antlrcpp::Any eversafe_llvm_settingsParser::SetApplyFormatContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = (eversafe_llvm_settingsVisitor *)(visitor))
    return parserVisitor->visitSetApplyFormat(this);
  else
    return visitor->visitChildren(this);
}

eversafe_llvm_settingsParser::SetApplyFormatContext* eversafe_llvm_settingsParser::setApplyFormat() {
  SetApplyFormatContext *_localctx = _tracker.createInstance<SetApplyFormatContext>(_ctx, getState());
  enterRule(_localctx, 46, eversafe_llvm_settingsParser::RuleSetApplyFormat);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(264);
    match(eversafe_llvm_settingsParser::SET);
    setState(265);
    setObfusKeys();
    setState(266);
    match(eversafe_llvm_settingsParser::SEMICOLON);

  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- SetObfusKeysContext ------------------------------------------------------------------

eversafe_llvm_settingsParser::SetObfusKeysContext::SetObfusKeysContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

eversafe_llvm_settingsParser::AllNoneObfusKeyContext* eversafe_llvm_settingsParser::SetObfusKeysContext::allNoneObfusKey() {
  return getRuleContext<eversafe_llvm_settingsParser::AllNoneObfusKeyContext>(0);
}

std::vector<eversafe_llvm_settingsParser::AddRemoveObfusKeyContext *> eversafe_llvm_settingsParser::SetObfusKeysContext::addRemoveObfusKey() {
  return getRuleContexts<eversafe_llvm_settingsParser::AddRemoveObfusKeyContext>();
}

eversafe_llvm_settingsParser::AddRemoveObfusKeyContext* eversafe_llvm_settingsParser::SetObfusKeysContext::addRemoveObfusKey(size_t i) {
  return getRuleContext<eversafe_llvm_settingsParser::AddRemoveObfusKeyContext>(i);
}

std::vector<tree::TerminalNode *> eversafe_llvm_settingsParser::SetObfusKeysContext::COMMA() {
  return getTokens(eversafe_llvm_settingsParser::COMMA);
}

tree::TerminalNode* eversafe_llvm_settingsParser::SetObfusKeysContext::COMMA(size_t i) {
  return getToken(eversafe_llvm_settingsParser::COMMA, i);
}


size_t eversafe_llvm_settingsParser::SetObfusKeysContext::getRuleIndex() const {
  return eversafe_llvm_settingsParser::RuleSetObfusKeys;
}

void eversafe_llvm_settingsParser::SetObfusKeysContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->enterSetObfusKeys(this);
}

void eversafe_llvm_settingsParser::SetObfusKeysContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->exitSetObfusKeys(this);
}


antlrcpp::Any eversafe_llvm_settingsParser::SetObfusKeysContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = (eversafe_llvm_settingsVisitor *)(visitor))
    return parserVisitor->visitSetObfusKeys(this);
  else
    return visitor->visitChildren(this);
}

eversafe_llvm_settingsParser::SetObfusKeysContext* eversafe_llvm_settingsParser::setObfusKeys() {
  SetObfusKeysContext *_localctx = _tracker.createInstance<SetObfusKeysContext>(_ctx, getState());
  enterRule(_localctx, 48, eversafe_llvm_settingsParser::RuleSetObfusKeys);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(277);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case eversafe_llvm_settingsParser::ALL:
      case eversafe_llvm_settingsParser::NON: {
        setState(268);
        allNoneObfusKey();
        break;
      }

      case eversafe_llvm_settingsParser::BOGUS:
      case eversafe_llvm_settingsParser::FLATTEN:
      case eversafe_llvm_settingsParser::WRAPPER:
      case eversafe_llvm_settingsParser::BRANCH:
      case eversafe_llvm_settingsParser::SPILT:
      case eversafe_llvm_settingsParser::STRING:
      case eversafe_llvm_settingsParser::DUMMY:
      case eversafe_llvm_settingsParser::ANTIDECOMPILE:
      case eversafe_llvm_settingsParser::SUBSTITUDE:
      case eversafe_llvm_settingsParser::ANTICLASSDUMP: {
        setState(269);
        addRemoveObfusKey();
        setState(274);
        _errHandler->sync(this);
        _la = _input->LA(1);
        while (_la == eversafe_llvm_settingsParser::COMMA) {
          setState(270);
          match(eversafe_llvm_settingsParser::COMMA);
          setState(271);
          addRemoveObfusKey();
          setState(276);
          _errHandler->sync(this);
          _la = _input->LA(1);
        }
        break;
      }

    default:
      throw NoViableAltException(this);
    }

  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- AddRemoveApplyForamtContext ------------------------------------------------------------------

eversafe_llvm_settingsParser::AddRemoveApplyForamtContext::AddRemoveApplyForamtContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

eversafe_llvm_settingsParser::AddRemoveObfusKeysContext* eversafe_llvm_settingsParser::AddRemoveApplyForamtContext::addRemoveObfusKeys() {
  return getRuleContext<eversafe_llvm_settingsParser::AddRemoveObfusKeysContext>(0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::AddRemoveApplyForamtContext::SEMICOLON() {
  return getToken(eversafe_llvm_settingsParser::SEMICOLON, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::AddRemoveApplyForamtContext::REMOVE() {
  return getToken(eversafe_llvm_settingsParser::REMOVE, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::AddRemoveApplyForamtContext::ADD() {
  return getToken(eversafe_llvm_settingsParser::ADD, 0);
}


size_t eversafe_llvm_settingsParser::AddRemoveApplyForamtContext::getRuleIndex() const {
  return eversafe_llvm_settingsParser::RuleAddRemoveApplyForamt;
}

void eversafe_llvm_settingsParser::AddRemoveApplyForamtContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->enterAddRemoveApplyForamt(this);
}

void eversafe_llvm_settingsParser::AddRemoveApplyForamtContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->exitAddRemoveApplyForamt(this);
}


antlrcpp::Any eversafe_llvm_settingsParser::AddRemoveApplyForamtContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = (eversafe_llvm_settingsVisitor *)(visitor))
    return parserVisitor->visitAddRemoveApplyForamt(this);
  else
    return visitor->visitChildren(this);
}

eversafe_llvm_settingsParser::AddRemoveApplyForamtContext* eversafe_llvm_settingsParser::addRemoveApplyForamt() {
  AddRemoveApplyForamtContext *_localctx = _tracker.createInstance<AddRemoveApplyForamtContext>(_ctx, getState());
  enterRule(_localctx, 50, eversafe_llvm_settingsParser::RuleAddRemoveApplyForamt);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(279);
    _la = _input->LA(1);
    if (!(_la == eversafe_llvm_settingsParser::ADD

    || _la == eversafe_llvm_settingsParser::REMOVE)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
    setState(280);
    addRemoveObfusKeys();
    setState(281);
    match(eversafe_llvm_settingsParser::SEMICOLON);

  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- AddRemoveObfusKeysContext ------------------------------------------------------------------

eversafe_llvm_settingsParser::AddRemoveObfusKeysContext::AddRemoveObfusKeysContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<eversafe_llvm_settingsParser::AddRemoveObfusKeyContext *> eversafe_llvm_settingsParser::AddRemoveObfusKeysContext::addRemoveObfusKey() {
  return getRuleContexts<eversafe_llvm_settingsParser::AddRemoveObfusKeyContext>();
}

eversafe_llvm_settingsParser::AddRemoveObfusKeyContext* eversafe_llvm_settingsParser::AddRemoveObfusKeysContext::addRemoveObfusKey(size_t i) {
  return getRuleContext<eversafe_llvm_settingsParser::AddRemoveObfusKeyContext>(i);
}

std::vector<tree::TerminalNode *> eversafe_llvm_settingsParser::AddRemoveObfusKeysContext::COMMA() {
  return getTokens(eversafe_llvm_settingsParser::COMMA);
}

tree::TerminalNode* eversafe_llvm_settingsParser::AddRemoveObfusKeysContext::COMMA(size_t i) {
  return getToken(eversafe_llvm_settingsParser::COMMA, i);
}


size_t eversafe_llvm_settingsParser::AddRemoveObfusKeysContext::getRuleIndex() const {
  return eversafe_llvm_settingsParser::RuleAddRemoveObfusKeys;
}

void eversafe_llvm_settingsParser::AddRemoveObfusKeysContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->enterAddRemoveObfusKeys(this);
}

void eversafe_llvm_settingsParser::AddRemoveObfusKeysContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->exitAddRemoveObfusKeys(this);
}


antlrcpp::Any eversafe_llvm_settingsParser::AddRemoveObfusKeysContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = (eversafe_llvm_settingsVisitor *)(visitor))
    return parserVisitor->visitAddRemoveObfusKeys(this);
  else
    return visitor->visitChildren(this);
}

eversafe_llvm_settingsParser::AddRemoveObfusKeysContext* eversafe_llvm_settingsParser::addRemoveObfusKeys() {
  AddRemoveObfusKeysContext *_localctx = _tracker.createInstance<AddRemoveObfusKeysContext>(_ctx, getState());
  enterRule(_localctx, 52, eversafe_llvm_settingsParser::RuleAddRemoveObfusKeys);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(283);
    addRemoveObfusKey();
    setState(288);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == eversafe_llvm_settingsParser::COMMA) {
      setState(284);
      match(eversafe_llvm_settingsParser::COMMA);
      setState(285);
      addRemoveObfusKey();
      setState(290);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }

  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- AddRemoveObfusKeyContext ------------------------------------------------------------------

eversafe_llvm_settingsParser::AddRemoveObfusKeyContext::AddRemoveObfusKeyContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* eversafe_llvm_settingsParser::AddRemoveObfusKeyContext::ANTICLASSDUMP() {
  return getToken(eversafe_llvm_settingsParser::ANTICLASSDUMP, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::AddRemoveObfusKeyContext::ANTIDECOMPILE() {
  return getToken(eversafe_llvm_settingsParser::ANTIDECOMPILE, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::AddRemoveObfusKeyContext::DUMMY() {
  return getToken(eversafe_llvm_settingsParser::DUMMY, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::AddRemoveObfusKeyContext::BOGUS() {
  return getToken(eversafe_llvm_settingsParser::BOGUS, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::AddRemoveObfusKeyContext::FLATTEN() {
  return getToken(eversafe_llvm_settingsParser::FLATTEN, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::AddRemoveObfusKeyContext::WRAPPER() {
  return getToken(eversafe_llvm_settingsParser::WRAPPER, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::AddRemoveObfusKeyContext::BRANCH() {
  return getToken(eversafe_llvm_settingsParser::BRANCH, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::AddRemoveObfusKeyContext::SPILT() {
  return getToken(eversafe_llvm_settingsParser::SPILT, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::AddRemoveObfusKeyContext::STRING() {
  return getToken(eversafe_llvm_settingsParser::STRING, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::AddRemoveObfusKeyContext::SUBSTITUDE() {
  return getToken(eversafe_llvm_settingsParser::SUBSTITUDE, 0);
}


size_t eversafe_llvm_settingsParser::AddRemoveObfusKeyContext::getRuleIndex() const {
  return eversafe_llvm_settingsParser::RuleAddRemoveObfusKey;
}

void eversafe_llvm_settingsParser::AddRemoveObfusKeyContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->enterAddRemoveObfusKey(this);
}

void eversafe_llvm_settingsParser::AddRemoveObfusKeyContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->exitAddRemoveObfusKey(this);
}


antlrcpp::Any eversafe_llvm_settingsParser::AddRemoveObfusKeyContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = (eversafe_llvm_settingsVisitor *)(visitor))
    return parserVisitor->visitAddRemoveObfusKey(this);
  else
    return visitor->visitChildren(this);
}

eversafe_llvm_settingsParser::AddRemoveObfusKeyContext* eversafe_llvm_settingsParser::addRemoveObfusKey() {
  AddRemoveObfusKeyContext *_localctx = _tracker.createInstance<AddRemoveObfusKeyContext>(_ctx, getState());
  enterRule(_localctx, 54, eversafe_llvm_settingsParser::RuleAddRemoveObfusKey);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(291);
    _la = _input->LA(1);
    if (!((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << eversafe_llvm_settingsParser::BOGUS)
      | (1ULL << eversafe_llvm_settingsParser::FLATTEN)
      | (1ULL << eversafe_llvm_settingsParser::WRAPPER)
      | (1ULL << eversafe_llvm_settingsParser::BRANCH)
      | (1ULL << eversafe_llvm_settingsParser::SPILT)
      | (1ULL << eversafe_llvm_settingsParser::STRING)
      | (1ULL << eversafe_llvm_settingsParser::DUMMY)
      | (1ULL << eversafe_llvm_settingsParser::ANTIDECOMPILE)
      | (1ULL << eversafe_llvm_settingsParser::SUBSTITUDE)
      | (1ULL << eversafe_llvm_settingsParser::ANTICLASSDUMP))) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }

  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- AllNoneObfusKeyContext ------------------------------------------------------------------

eversafe_llvm_settingsParser::AllNoneObfusKeyContext::AllNoneObfusKeyContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* eversafe_llvm_settingsParser::AllNoneObfusKeyContext::ALL() {
  return getToken(eversafe_llvm_settingsParser::ALL, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::AllNoneObfusKeyContext::NON() {
  return getToken(eversafe_llvm_settingsParser::NON, 0);
}


size_t eversafe_llvm_settingsParser::AllNoneObfusKeyContext::getRuleIndex() const {
  return eversafe_llvm_settingsParser::RuleAllNoneObfusKey;
}

void eversafe_llvm_settingsParser::AllNoneObfusKeyContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->enterAllNoneObfusKey(this);
}

void eversafe_llvm_settingsParser::AllNoneObfusKeyContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->exitAllNoneObfusKey(this);
}


antlrcpp::Any eversafe_llvm_settingsParser::AllNoneObfusKeyContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = (eversafe_llvm_settingsVisitor *)(visitor))
    return parserVisitor->visitAllNoneObfusKey(this);
  else
    return visitor->visitChildren(this);
}

eversafe_llvm_settingsParser::AllNoneObfusKeyContext* eversafe_llvm_settingsParser::allNoneObfusKey() {
  AllNoneObfusKeyContext *_localctx = _tracker.createInstance<AllNoneObfusKeyContext>(_ctx, getState());
  enterRule(_localctx, 56, eversafe_llvm_settingsParser::RuleAllNoneObfusKey);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(293);
    _la = _input->LA(1);
    if (!(_la == eversafe_llvm_settingsParser::ALL

    || _la == eversafe_llvm_settingsParser::NON)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }

  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- SeparContext ------------------------------------------------------------------

eversafe_llvm_settingsParser::SeparContext::SeparContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* eversafe_llvm_settingsParser::SeparContext::DCOLON() {
  return getToken(eversafe_llvm_settingsParser::DCOLON, 0);
}


size_t eversafe_llvm_settingsParser::SeparContext::getRuleIndex() const {
  return eversafe_llvm_settingsParser::RuleSepar;
}

void eversafe_llvm_settingsParser::SeparContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->enterSepar(this);
}

void eversafe_llvm_settingsParser::SeparContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->exitSepar(this);
}


antlrcpp::Any eversafe_llvm_settingsParser::SeparContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = (eversafe_llvm_settingsVisitor *)(visitor))
    return parserVisitor->visitSepar(this);
  else
    return visitor->visitChildren(this);
}

eversafe_llvm_settingsParser::SeparContext* eversafe_llvm_settingsParser::separ() {
  SeparContext *_localctx = _tracker.createInstance<SeparContext>(_ctx, getState());
  enterRule(_localctx, 58, eversafe_llvm_settingsParser::RuleSepar);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(295);
    match(eversafe_llvm_settingsParser::DCOLON);

  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- DuplicateKeywordContext ------------------------------------------------------------------

eversafe_llvm_settingsParser::DuplicateKeywordContext::DuplicateKeywordContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* eversafe_llvm_settingsParser::DuplicateKeywordContext::NON() {
  return getToken(eversafe_llvm_settingsParser::NON, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::DuplicateKeywordContext::ALL() {
  return getToken(eversafe_llvm_settingsParser::ALL, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::DuplicateKeywordContext::ANTICLASSDUMP() {
  return getToken(eversafe_llvm_settingsParser::ANTICLASSDUMP, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::DuplicateKeywordContext::ANTIDECOMPILE() {
  return getToken(eversafe_llvm_settingsParser::ANTIDECOMPILE, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::DuplicateKeywordContext::DUMMY() {
  return getToken(eversafe_llvm_settingsParser::DUMMY, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::DuplicateKeywordContext::FUNCTION() {
  return getToken(eversafe_llvm_settingsParser::FUNCTION, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::DuplicateKeywordContext::CPP() {
  return getToken(eversafe_llvm_settingsParser::CPP, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::DuplicateKeywordContext::OBJC() {
  return getToken(eversafe_llvm_settingsParser::OBJC, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::DuplicateKeywordContext::CC() {
  return getToken(eversafe_llvm_settingsParser::CC, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::DuplicateKeywordContext::CLASS() {
  return getToken(eversafe_llvm_settingsParser::CLASS, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::DuplicateKeywordContext::ADD() {
  return getToken(eversafe_llvm_settingsParser::ADD, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::DuplicateKeywordContext::SET() {
  return getToken(eversafe_llvm_settingsParser::SET, 0);
}

tree::TerminalNode* eversafe_llvm_settingsParser::DuplicateKeywordContext::REMOVE() {
  return getToken(eversafe_llvm_settingsParser::REMOVE, 0);
}


size_t eversafe_llvm_settingsParser::DuplicateKeywordContext::getRuleIndex() const {
  return eversafe_llvm_settingsParser::RuleDuplicateKeyword;
}

void eversafe_llvm_settingsParser::DuplicateKeywordContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->enterDuplicateKeyword(this);
}

void eversafe_llvm_settingsParser::DuplicateKeywordContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = (eversafe_llvm_settingsListener *)(listener);
  if (parserListener != nullptr)
    parserListener->exitDuplicateKeyword(this);
}


antlrcpp::Any eversafe_llvm_settingsParser::DuplicateKeywordContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = (eversafe_llvm_settingsVisitor *)(visitor))
    return parserVisitor->visitDuplicateKeyword(this);
  else
    return visitor->visitChildren(this);
}

eversafe_llvm_settingsParser::DuplicateKeywordContext* eversafe_llvm_settingsParser::duplicateKeyword() {
  DuplicateKeywordContext *_localctx = _tracker.createInstance<DuplicateKeywordContext>(_ctx, getState());
  enterRule(_localctx, 60, eversafe_llvm_settingsParser::RuleDuplicateKeyword);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(297);
    _la = _input->LA(1);
    if (!((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << eversafe_llvm_settingsParser::CPP)
      | (1ULL << eversafe_llvm_settingsParser::OBJC)
      | (1ULL << eversafe_llvm_settingsParser::CC)
      | (1ULL << eversafe_llvm_settingsParser::CLASS)
      | (1ULL << eversafe_llvm_settingsParser::ADD)
      | (1ULL << eversafe_llvm_settingsParser::SET)
      | (1ULL << eversafe_llvm_settingsParser::REMOVE)
      | (1ULL << eversafe_llvm_settingsParser::FUNCTION)
      | (1ULL << eversafe_llvm_settingsParser::DUMMY)
      | (1ULL << eversafe_llvm_settingsParser::ANTIDECOMPILE)
      | (1ULL << eversafe_llvm_settingsParser::ANTICLASSDUMP)
      | (1ULL << eversafe_llvm_settingsParser::ALL)
      | (1ULL << eversafe_llvm_settingsParser::NON))) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }

  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

// Static vars and initialization.
std::vector<dfa::DFA> eversafe_llvm_settingsParser::_decisionToDFA;
atn::PredictionContextCache eversafe_llvm_settingsParser::_sharedContextCache;

// We own the ATN which in turn owns the ATN states.
atn::ATN eversafe_llvm_settingsParser::_atn;
std::vector<uint16_t> eversafe_llvm_settingsParser::_serializedATN;

std::vector<std::string> eversafe_llvm_settingsParser::_ruleNames = {
  "settings", "objc", "objcInternalData", "objcClassFormat", "objcClassIntetnalData",
  "objcMethodFormat", "objcGlobalMethodFormat", "cpp", "cppInternalData",
  "cppClassInternalData", "cppSingleAsteriskClassInternalData", "cppClassFormat",
  "singleCppClassFormat", "doulbeCppClassFormat", "cppMethodFormat", "cppName",
  "singleAsteriskName", "doubleAsteriskName", "cppMethodName", "c", "cInternalData",
  "functionFormat", "block", "setApplyFormat", "setObfusKeys", "addRemoveApplyForamt",
  "addRemoveObfusKeys", "addRemoveObfusKey", "allNoneObfusKey", "separ",
  "duplicateKeyword"
};

std::vector<std::string> eversafe_llvm_settingsParser::_literalNames = {
  "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
  "", "", "", "", "", "'**'", "'*'", "'['", "']'", "'{'", "'}'", "'('",
  "')'", "','", "'.'", "'::'", "'$'", "';'"
};

std::vector<std::string> eversafe_llvm_settingsParser::_symbolicNames = {
  "", "CPP", "OBJC", "CC", "CLASS", "NAMESPACE", "METHOD", "ADD", "SET",
  "REMOVE", "FUNCTION", "BOGUS", "FLATTEN", "WRAPPER", "BRANCH", "SPILT",
  "STRING", "DUMMY", "ANTIDECOMPILE", "SUBSTITUDE", "ANTICLASSDUMP", "ALL",
  "NON", "DWILDCARD", "WILDCARD", "LBRACK", "RBRACK", "LBRACE", "RBRACE",
  "LPAREN", "RPAREN", "COMMA", "DOT", "DCOLON", "INNER", "SEMICOLON", "NAME",
  "CppGlobalMethodName", "ObjcMethodName", "ObjcGlobalMethodName", "CppMethodName",
  "LineComment", "WS", "ANY"
};

dfa::Vocabulary eversafe_llvm_settingsParser::_vocabulary(_literalNames, _symbolicNames);

std::vector<std::string> eversafe_llvm_settingsParser::_tokenNames;

eversafe_llvm_settingsParser::Initializer::Initializer() {
	for (size_t i = 0; i < _symbolicNames.size(); ++i) {
		std::string name = _vocabulary.getLiteralName(i);
		if (name.empty()) {
			name = _vocabulary.getSymbolicName(i);
		}

		if (name.empty()) {
			_tokenNames.push_back("<INVALID>");
		} else {
      _tokenNames.push_back(name);
    }
	}

  _serializedATN = {
    0x3, 0x608b, 0xa72a, 0x8133, 0xb9ed, 0x417c, 0x3be7, 0x7786, 0x5964,
    0x3, 0x2d, 0x12e, 0x4, 0x2, 0x9, 0x2, 0x4, 0x3, 0x9, 0x3, 0x4, 0x4,
    0x9, 0x4, 0x4, 0x5, 0x9, 0x5, 0x4, 0x6, 0x9, 0x6, 0x4, 0x7, 0x9, 0x7,
    0x4, 0x8, 0x9, 0x8, 0x4, 0x9, 0x9, 0x9, 0x4, 0xa, 0x9, 0xa, 0x4, 0xb,
    0x9, 0xb, 0x4, 0xc, 0x9, 0xc, 0x4, 0xd, 0x9, 0xd, 0x4, 0xe, 0x9, 0xe,
    0x4, 0xf, 0x9, 0xf, 0x4, 0x10, 0x9, 0x10, 0x4, 0x11, 0x9, 0x11, 0x4,
    0x12, 0x9, 0x12, 0x4, 0x13, 0x9, 0x13, 0x4, 0x14, 0x9, 0x14, 0x4, 0x15,
    0x9, 0x15, 0x4, 0x16, 0x9, 0x16, 0x4, 0x17, 0x9, 0x17, 0x4, 0x18, 0x9,
    0x18, 0x4, 0x19, 0x9, 0x19, 0x4, 0x1a, 0x9, 0x1a, 0x4, 0x1b, 0x9, 0x1b,
    0x4, 0x1c, 0x9, 0x1c, 0x4, 0x1d, 0x9, 0x1d, 0x4, 0x1e, 0x9, 0x1e, 0x4,
    0x1f, 0x9, 0x1f, 0x4, 0x20, 0x9, 0x20, 0x3, 0x2, 0x5, 0x2, 0x42, 0xa,
    0x2, 0x3, 0x2, 0x5, 0x2, 0x45, 0xa, 0x2, 0x3, 0x2, 0x5, 0x2, 0x48, 0xa,
    0x2, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x5, 0x3, 0x4d, 0xa, 0x3, 0x3, 0x3,
    0x5, 0x3, 0x50, 0xa, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x4, 0x3, 0x4, 0x7,
    0x4, 0x56, 0xa, 0x4, 0xc, 0x4, 0xe, 0x4, 0x59, 0xb, 0x4, 0x3, 0x5, 0x3,
    0x5, 0x3, 0x5, 0x5, 0x5, 0x5e, 0xa, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5,
    0x3, 0x5, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x5, 0x6, 0x67, 0xa, 0x6, 0x7,
    0x6, 0x69, 0xa, 0x6, 0xc, 0x6, 0xe, 0x6, 0x6c, 0xb, 0x6, 0x3, 0x7, 0x3,
    0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 0x3,
    0x9, 0x3, 0x9, 0x3, 0x9, 0x5, 0x9, 0x79, 0xa, 0x9, 0x3, 0x9, 0x7, 0x9,
    0x7c, 0xa, 0x9, 0xc, 0x9, 0xe, 0x9, 0x7f, 0xb, 0x9, 0x3, 0x9, 0x3, 0x9,
    0x3, 0xa, 0x3, 0xa, 0x5, 0xa, 0x85, 0xa, 0xa, 0x3, 0xb, 0x3, 0xb, 0x3,
    0xb, 0x3, 0xb, 0x5, 0xb, 0x8b, 0xa, 0xb, 0x7, 0xb, 0x8d, 0xa, 0xb, 0xc,
    0xb, 0xe, 0xb, 0x90, 0xb, 0xb, 0x3, 0xc, 0x3, 0xc, 0x3, 0xc, 0x5, 0xc,
    0x95, 0xa, 0xc, 0x7, 0xc, 0x97, 0xa, 0xc, 0xc, 0xc, 0xe, 0xc, 0x9a,
    0xb, 0xc, 0x3, 0xd, 0x3, 0xd, 0x5, 0xd, 0x9e, 0xa, 0xd, 0x3, 0xe, 0x3,
    0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 0xf, 0x3, 0xf, 0x3,
    0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0x10, 0x3, 0x10, 0x3, 0x10,
    0x3, 0x10, 0x3, 0x11, 0x3, 0x11, 0x5, 0x11, 0xb2, 0xa, 0x11, 0x3, 0x12,
    0x3, 0x12, 0x5, 0x12, 0xb6, 0xa, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12,
    0x5, 0x12, 0xbb, 0xa, 0x12, 0x7, 0x12, 0xbd, 0xa, 0x12, 0xc, 0x12, 0xe,
    0x12, 0xc0, 0xb, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x5, 0x12, 0xc5,
    0xa, 0x12, 0x3, 0x13, 0x3, 0x13, 0x5, 0x13, 0xc9, 0xa, 0x13, 0x3, 0x13,
    0x3, 0x13, 0x3, 0x13, 0x5, 0x13, 0xce, 0xa, 0x13, 0x7, 0x13, 0xd0, 0xa,
    0x13, 0xc, 0x13, 0xe, 0x13, 0xd3, 0xb, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3,
    0x13, 0x3, 0x14, 0x3, 0x14, 0x5, 0x14, 0xda, 0xa, 0x14, 0x3, 0x14, 0x3,
    0x14, 0x3, 0x14, 0x5, 0x14, 0xdf, 0xa, 0x14, 0x7, 0x14, 0xe1, 0xa, 0x14,
    0xc, 0x14, 0xe, 0x14, 0xe4, 0xb, 0x14, 0x3, 0x15, 0x3, 0x15, 0x3, 0x15,
    0x5, 0x15, 0xe9, 0xa, 0x15, 0x3, 0x15, 0x5, 0x15, 0xec, 0xa, 0x15, 0x3,
    0x15, 0x3, 0x15, 0x3, 0x16, 0x3, 0x16, 0x3, 0x16, 0x5, 0x16, 0xf3, 0xa,
    0x16, 0x7, 0x16, 0xf5, 0xa, 0x16, 0xc, 0x16, 0xe, 0x16, 0xf8, 0xb, 0x16,
    0x3, 0x17, 0x3, 0x17, 0x3, 0x17, 0x5, 0x17, 0xfd, 0xa, 0x17, 0x3, 0x17,
    0x3, 0x17, 0x3, 0x18, 0x3, 0x18, 0x3, 0x18, 0x7, 0x18, 0x104, 0xa, 0x18,
    0xc, 0x18, 0xe, 0x18, 0x107, 0xb, 0x18, 0x3, 0x18, 0x3, 0x18, 0x3, 0x19,
    0x3, 0x19, 0x3, 0x19, 0x3, 0x19, 0x3, 0x1a, 0x3, 0x1a, 0x3, 0x1a, 0x3,
    0x1a, 0x7, 0x1a, 0x113, 0xa, 0x1a, 0xc, 0x1a, 0xe, 0x1a, 0x116, 0xb,
    0x1a, 0x5, 0x1a, 0x118, 0xa, 0x1a, 0x3, 0x1b, 0x3, 0x1b, 0x3, 0x1b,
    0x3, 0x1b, 0x3, 0x1c, 0x3, 0x1c, 0x3, 0x1c, 0x7, 0x1c, 0x121, 0xa, 0x1c,
    0xc, 0x1c, 0xe, 0x1c, 0x124, 0xb, 0x1c, 0x3, 0x1d, 0x3, 0x1d, 0x3, 0x1e,
    0x3, 0x1e, 0x3, 0x1f, 0x3, 0x1f, 0x3, 0x20, 0x3, 0x20, 0x3, 0x20, 0x5,
    0x57, 0xf6, 0x105, 0x2, 0x21, 0x2, 0x4, 0x6, 0x8, 0xa, 0xc, 0xe, 0x10,
    0x12, 0x14, 0x16, 0x18, 0x1a, 0x1c, 0x1e, 0x20, 0x22, 0x24, 0x26, 0x28,
    0x2a, 0x2c, 0x2e, 0x30, 0x32, 0x34, 0x36, 0x38, 0x3a, 0x3c, 0x3e, 0x2,
    0x6, 0x4, 0x2, 0x9, 0x9, 0xb, 0xb, 0x3, 0x2, 0xd, 0x16, 0x3, 0x2, 0x17,
    0x18, 0x6, 0x2, 0x3, 0x6, 0x9, 0xc, 0x13, 0x14, 0x16, 0x18, 0x2, 0x13a,
    0x2, 0x41, 0x3, 0x2, 0x2, 0x2, 0x4, 0x49, 0x3, 0x2, 0x2, 0x2, 0x6, 0x57,
    0x3, 0x2, 0x2, 0x2, 0x8, 0x5a, 0x3, 0x2, 0x2, 0x2, 0xa, 0x6a, 0x3, 0x2,
    0x2, 0x2, 0xc, 0x6d, 0x3, 0x2, 0x2, 0x2, 0xe, 0x71, 0x3, 0x2, 0x2, 0x2,
    0x10, 0x75, 0x3, 0x2, 0x2, 0x2, 0x12, 0x84, 0x3, 0x2, 0x2, 0x2, 0x14,
    0x8e, 0x3, 0x2, 0x2, 0x2, 0x16, 0x98, 0x3, 0x2, 0x2, 0x2, 0x18, 0x9d,
    0x3, 0x2, 0x2, 0x2, 0x1a, 0x9f, 0x3, 0x2, 0x2, 0x2, 0x1c, 0xa5, 0x3,
    0x2, 0x2, 0x2, 0x1e, 0xab, 0x3, 0x2, 0x2, 0x2, 0x20, 0xb1, 0x3, 0x2,
    0x2, 0x2, 0x22, 0xc4, 0x3, 0x2, 0x2, 0x2, 0x24, 0xc8, 0x3, 0x2, 0x2,
    0x2, 0x26, 0xd9, 0x3, 0x2, 0x2, 0x2, 0x28, 0xe5, 0x3, 0x2, 0x2, 0x2,
    0x2a, 0xf6, 0x3, 0x2, 0x2, 0x2, 0x2c, 0xf9, 0x3, 0x2, 0x2, 0x2, 0x2e,
    0x100, 0x3, 0x2, 0x2, 0x2, 0x30, 0x10a, 0x3, 0x2, 0x2, 0x2, 0x32, 0x117,
    0x3, 0x2, 0x2, 0x2, 0x34, 0x119, 0x3, 0x2, 0x2, 0x2, 0x36, 0x11d, 0x3,
    0x2, 0x2, 0x2, 0x38, 0x125, 0x3, 0x2, 0x2, 0x2, 0x3a, 0x127, 0x3, 0x2,
    0x2, 0x2, 0x3c, 0x129, 0x3, 0x2, 0x2, 0x2, 0x3e, 0x12b, 0x3, 0x2, 0x2,
    0x2, 0x40, 0x42, 0x5, 0x4, 0x3, 0x2, 0x41, 0x40, 0x3, 0x2, 0x2, 0x2,
    0x41, 0x42, 0x3, 0x2, 0x2, 0x2, 0x42, 0x44, 0x3, 0x2, 0x2, 0x2, 0x43,
    0x45, 0x5, 0x10, 0x9, 0x2, 0x44, 0x43, 0x3, 0x2, 0x2, 0x2, 0x44, 0x45,
    0x3, 0x2, 0x2, 0x2, 0x45, 0x47, 0x3, 0x2, 0x2, 0x2, 0x46, 0x48, 0x5,
    0x28, 0x15, 0x2, 0x47, 0x46, 0x3, 0x2, 0x2, 0x2, 0x47, 0x48, 0x3, 0x2,
    0x2, 0x2, 0x48, 0x3, 0x3, 0x2, 0x2, 0x2, 0x49, 0x4a, 0x7, 0x4, 0x2,
    0x2, 0x4a, 0x4c, 0x7, 0x1d, 0x2, 0x2, 0x4b, 0x4d, 0x5, 0x30, 0x19, 0x2,
    0x4c, 0x4b, 0x3, 0x2, 0x2, 0x2, 0x4c, 0x4d, 0x3, 0x2, 0x2, 0x2, 0x4d,
    0x4f, 0x3, 0x2, 0x2, 0x2, 0x4e, 0x50, 0x5, 0x6, 0x4, 0x2, 0x4f, 0x4e,
    0x3, 0x2, 0x2, 0x2, 0x4f, 0x50, 0x3, 0x2, 0x2, 0x2, 0x50, 0x51, 0x3,
    0x2, 0x2, 0x2, 0x51, 0x52, 0x7, 0x1e, 0x2, 0x2, 0x52, 0x5, 0x3, 0x2,
    0x2, 0x2, 0x53, 0x56, 0x5, 0xe, 0x8, 0x2, 0x54, 0x56, 0x5, 0x8, 0x5,
    0x2, 0x55, 0x53, 0x3, 0x2, 0x2, 0x2, 0x55, 0x54, 0x3, 0x2, 0x2, 0x2,
    0x56, 0x59, 0x3, 0x2, 0x2, 0x2, 0x57, 0x58, 0x3, 0x2, 0x2, 0x2, 0x57,
    0x55, 0x3, 0x2, 0x2, 0x2, 0x58, 0x7, 0x3, 0x2, 0x2, 0x2, 0x59, 0x57,
    0x3, 0x2, 0x2, 0x2, 0x5a, 0x5d, 0x7, 0x6, 0x2, 0x2, 0x5b, 0x5e, 0x7,
    0x26, 0x2, 0x2, 0x5c, 0x5e, 0x5, 0x3e, 0x20, 0x2, 0x5d, 0x5b, 0x3, 0x2,
    0x2, 0x2, 0x5d, 0x5c, 0x3, 0x2, 0x2, 0x2, 0x5e, 0x5f, 0x3, 0x2, 0x2,
    0x2, 0x5f, 0x60, 0x7, 0x1d, 0x2, 0x2, 0x60, 0x61, 0x5, 0xa, 0x6, 0x2,
    0x61, 0x62, 0x7, 0x1e, 0x2, 0x2, 0x62, 0x9, 0x3, 0x2, 0x2, 0x2, 0x63,
    0x69, 0x5, 0xc, 0x7, 0x2, 0x64, 0x67, 0x5, 0x30, 0x19, 0x2, 0x65, 0x67,
    0x5, 0x34, 0x1b, 0x2, 0x66, 0x64, 0x3, 0x2, 0x2, 0x2, 0x66, 0x65, 0x3,
    0x2, 0x2, 0x2, 0x67, 0x69, 0x3, 0x2, 0x2, 0x2, 0x68, 0x63, 0x3, 0x2,
    0x2, 0x2, 0x68, 0x66, 0x3, 0x2, 0x2, 0x2, 0x69, 0x6c, 0x3, 0x2, 0x2,
    0x2, 0x6a, 0x68, 0x3, 0x2, 0x2, 0x2, 0x6a, 0x6b, 0x3, 0x2, 0x2, 0x2,
    0x6b, 0xb, 0x3, 0x2, 0x2, 0x2, 0x6c, 0x6a, 0x3, 0x2, 0x2, 0x2, 0x6d,
    0x6e, 0x7, 0x8, 0x2, 0x2, 0x6e, 0x6f, 0x7, 0x29, 0x2, 0x2, 0x6f, 0x70,
    0x5, 0x2e, 0x18, 0x2, 0x70, 0xd, 0x3, 0x2, 0x2, 0x2, 0x71, 0x72, 0x7,
    0x8, 0x2, 0x2, 0x72, 0x73, 0x7, 0x29, 0x2, 0x2, 0x73, 0x74, 0x5, 0x2e,
    0x18, 0x2, 0x74, 0xf, 0x3, 0x2, 0x2, 0x2, 0x75, 0x76, 0x7, 0x3, 0x2,
    0x2, 0x76, 0x78, 0x7, 0x1d, 0x2, 0x2, 0x77, 0x79, 0x5, 0x30, 0x19, 0x2,
    0x78, 0x77, 0x3, 0x2, 0x2, 0x2, 0x78, 0x79, 0x3, 0x2, 0x2, 0x2, 0x79,
    0x7d, 0x3, 0x2, 0x2, 0x2, 0x7a, 0x7c, 0x5, 0x12, 0xa, 0x2, 0x7b, 0x7a,
    0x3, 0x2, 0x2, 0x2, 0x7c, 0x7f, 0x3, 0x2, 0x2, 0x2, 0x7d, 0x7b, 0x3,
    0x2, 0x2, 0x2, 0x7d, 0x7e, 0x3, 0x2, 0x2, 0x2, 0x7e, 0x80, 0x3, 0x2,
    0x2, 0x2, 0x7f, 0x7d, 0x3, 0x2, 0x2, 0x2, 0x80, 0x81, 0x7, 0x1e, 0x2,
    0x2, 0x81, 0x11, 0x3, 0x2, 0x2, 0x2, 0x82, 0x85, 0x5, 0x1e, 0x10, 0x2,
    0x83, 0x85, 0x5, 0x18, 0xd, 0x2, 0x84, 0x82, 0x3, 0x2, 0x2, 0x2, 0x84,
    0x83, 0x3, 0x2, 0x2, 0x2, 0x85, 0x13, 0x3, 0x2, 0x2, 0x2, 0x86, 0x8d,
    0x5, 0x18, 0xd, 0x2, 0x87, 0x8d, 0x5, 0x1e, 0x10, 0x2, 0x88, 0x8b, 0x5,
    0x30, 0x19, 0x2, 0x89, 0x8b, 0x5, 0x34, 0x1b, 0x2, 0x8a, 0x88, 0x3,
    0x2, 0x2, 0x2, 0x8a, 0x89, 0x3, 0x2, 0x2, 0x2, 0x8b, 0x8d, 0x3, 0x2,
    0x2, 0x2, 0x8c, 0x86, 0x3, 0x2, 0x2, 0x2, 0x8c, 0x87, 0x3, 0x2, 0x2,
    0x2, 0x8c, 0x8a, 0x3, 0x2, 0x2, 0x2, 0x8d, 0x90, 0x3, 0x2, 0x2, 0x2,
    0x8e, 0x8c, 0x3, 0x2, 0x2, 0x2, 0x8e, 0x8f, 0x3, 0x2, 0x2, 0x2, 0x8f,
    0x15, 0x3, 0x2, 0x2, 0x2, 0x90, 0x8e, 0x3, 0x2, 0x2, 0x2, 0x91, 0x97,
    0x5, 0x1e, 0x10, 0x2, 0x92, 0x95, 0x5, 0x30, 0x19, 0x2, 0x93, 0x95,
    0x5, 0x34, 0x1b, 0x2, 0x94, 0x92, 0x3, 0x2, 0x2, 0x2, 0x94, 0x93, 0x3,
    0x2, 0x2, 0x2, 0x95, 0x97, 0x3, 0x2, 0x2, 0x2, 0x96, 0x91, 0x3, 0x2,
    0x2, 0x2, 0x96, 0x94, 0x3, 0x2, 0x2, 0x2, 0x97, 0x9a, 0x3, 0x2, 0x2,
    0x2, 0x98, 0x96, 0x3, 0x2, 0x2, 0x2, 0x98, 0x99, 0x3, 0x2, 0x2, 0x2,
    0x99, 0x17, 0x3, 0x2, 0x2, 0x2, 0x9a, 0x98, 0x3, 0x2, 0x2, 0x2, 0x9b,
    0x9e, 0x5, 0x1a, 0xe, 0x2, 0x9c, 0x9e, 0x5, 0x1c, 0xf, 0x2, 0x9d, 0x9b,
    0x3, 0x2, 0x2, 0x2, 0x9d, 0x9c, 0x3, 0x2, 0x2, 0x2, 0x9e, 0x19, 0x3,
    0x2, 0x2, 0x2, 0x9f, 0xa0, 0x7, 0x7, 0x2, 0x2, 0xa0, 0xa1, 0x5, 0x22,
    0x12, 0x2, 0xa1, 0xa2, 0x7, 0x1d, 0x2, 0x2, 0xa2, 0xa3, 0x5, 0x16, 0xc,
    0x2, 0xa3, 0xa4, 0x7, 0x1e, 0x2, 0x2, 0xa4, 0x1b, 0x3, 0x2, 0x2, 0x2,
    0xa5, 0xa6, 0x7, 0x7, 0x2, 0x2, 0xa6, 0xa7, 0x5, 0x24, 0x13, 0x2, 0xa7,
    0xa8, 0x7, 0x1d, 0x2, 0x2, 0xa8, 0xa9, 0x5, 0x14, 0xb, 0x2, 0xa9, 0xaa,
    0x7, 0x1e, 0x2, 0x2, 0xaa, 0x1d, 0x3, 0x2, 0x2, 0x2, 0xab, 0xac, 0x7,
    0x8, 0x2, 0x2, 0xac, 0xad, 0x5, 0x26, 0x14, 0x2, 0xad, 0xae, 0x5, 0x2e,
    0x18, 0x2, 0xae, 0x1f, 0x3, 0x2, 0x2, 0x2, 0xaf, 0xb2, 0x5, 0x22, 0x12,
    0x2, 0xb0, 0xb2, 0x5, 0x24, 0x13, 0x2, 0xb1, 0xaf, 0x3, 0x2, 0x2, 0x2,
    0xb1, 0xb0, 0x3, 0x2, 0x2, 0x2, 0xb2, 0x21, 0x3, 0x2, 0x2, 0x2, 0xb3,
    0xb6, 0x7, 0x26, 0x2, 0x2, 0xb4, 0xb6, 0x5, 0x3e, 0x20, 0x2, 0xb5, 0xb3,
    0x3, 0x2, 0x2, 0x2, 0xb5, 0xb4, 0x3, 0x2, 0x2, 0x2, 0xb6, 0xbe, 0x3,
    0x2, 0x2, 0x2, 0xb7, 0xba, 0x7, 0x23, 0x2, 0x2, 0xb8, 0xbb, 0x7, 0x26,
    0x2, 0x2, 0xb9, 0xbb, 0x5, 0x3e, 0x20, 0x2, 0xba, 0xb8, 0x3, 0x2, 0x2,
    0x2, 0xba, 0xb9, 0x3, 0x2, 0x2, 0x2, 0xbb, 0xbd, 0x3, 0x2, 0x2, 0x2,
    0xbc, 0xb7, 0x3, 0x2, 0x2, 0x2, 0xbd, 0xc0, 0x3, 0x2, 0x2, 0x2, 0xbe,
    0xbc, 0x3, 0x2, 0x2, 0x2, 0xbe, 0xbf, 0x3, 0x2, 0x2, 0x2, 0xbf, 0xc1,
    0x3, 0x2, 0x2, 0x2, 0xc0, 0xbe, 0x3, 0x2, 0x2, 0x2, 0xc1, 0xc2, 0x7,
    0x23, 0x2, 0x2, 0xc2, 0xc5, 0x7, 0x1a, 0x2, 0x2, 0xc3, 0xc5, 0x7, 0x1a,
    0x2, 0x2, 0xc4, 0xb5, 0x3, 0x2, 0x2, 0x2, 0xc4, 0xc3, 0x3, 0x2, 0x2,
    0x2, 0xc5, 0x23, 0x3, 0x2, 0x2, 0x2, 0xc6, 0xc9, 0x7, 0x26, 0x2, 0x2,
    0xc7, 0xc9, 0x5, 0x3e, 0x20, 0x2, 0xc8, 0xc6, 0x3, 0x2, 0x2, 0x2, 0xc8,
    0xc7, 0x3, 0x2, 0x2, 0x2, 0xc9, 0xd1, 0x3, 0x2, 0x2, 0x2, 0xca, 0xcd,
    0x7, 0x23, 0x2, 0x2, 0xcb, 0xce, 0x7, 0x26, 0x2, 0x2, 0xcc, 0xce, 0x5,
    0x3e, 0x20, 0x2, 0xcd, 0xcb, 0x3, 0x2, 0x2, 0x2, 0xcd, 0xcc, 0x3, 0x2,
    0x2, 0x2, 0xce, 0xd0, 0x3, 0x2, 0x2, 0x2, 0xcf, 0xca, 0x3, 0x2, 0x2,
    0x2, 0xd0, 0xd3, 0x3, 0x2, 0x2, 0x2, 0xd1, 0xcf, 0x3, 0x2, 0x2, 0x2,
    0xd1, 0xd2, 0x3, 0x2, 0x2, 0x2, 0xd2, 0xd4, 0x3, 0x2, 0x2, 0x2, 0xd3,
    0xd1, 0x3, 0x2, 0x2, 0x2, 0xd4, 0xd5, 0x7, 0x23, 0x2, 0x2, 0xd5, 0xd6,
    0x7, 0x19, 0x2, 0x2, 0xd6, 0x25, 0x3, 0x2, 0x2, 0x2, 0xd7, 0xda, 0x7,
    0x26, 0x2, 0x2, 0xd8, 0xda, 0x5, 0x3e, 0x20, 0x2, 0xd9, 0xd7, 0x3, 0x2,
    0x2, 0x2, 0xd9, 0xd8, 0x3, 0x2, 0x2, 0x2, 0xda, 0xe2, 0x3, 0x2, 0x2,
    0x2, 0xdb, 0xde, 0x7, 0x23, 0x2, 0x2, 0xdc, 0xdf, 0x7, 0x26, 0x2, 0x2,
    0xdd, 0xdf, 0x5, 0x3e, 0x20, 0x2, 0xde, 0xdc, 0x3, 0x2, 0x2, 0x2, 0xde,
    0xdd, 0x3, 0x2, 0x2, 0x2, 0xdf, 0xe1, 0x3, 0x2, 0x2, 0x2, 0xe0, 0xdb,
    0x3, 0x2, 0x2, 0x2, 0xe1, 0xe4, 0x3, 0x2, 0x2, 0x2, 0xe2, 0xe0, 0x3,
    0x2, 0x2, 0x2, 0xe2, 0xe3, 0x3, 0x2, 0x2, 0x2, 0xe3, 0x27, 0x3, 0x2,
    0x2, 0x2, 0xe4, 0xe2, 0x3, 0x2, 0x2, 0x2, 0xe5, 0xe6, 0x7, 0x5, 0x2,
    0x2, 0xe6, 0xe8, 0x7, 0x1d, 0x2, 0x2, 0xe7, 0xe9, 0x5, 0x30, 0x19, 0x2,
    0xe8, 0xe7, 0x3, 0x2, 0x2, 0x2, 0xe8, 0xe9, 0x3, 0x2, 0x2, 0x2, 0xe9,
    0xeb, 0x3, 0x2, 0x2, 0x2, 0xea, 0xec, 0x5, 0x2a, 0x16, 0x2, 0xeb, 0xea,
    0x3, 0x2, 0x2, 0x2, 0xeb, 0xec, 0x3, 0x2, 0x2, 0x2, 0xec, 0xed, 0x3,
    0x2, 0x2, 0x2, 0xed, 0xee, 0x7, 0x1e, 0x2, 0x2, 0xee, 0x29, 0x3, 0x2,
    0x2, 0x2, 0xef, 0xf5, 0x5, 0x2c, 0x17, 0x2, 0xf0, 0xf3, 0x5, 0x30, 0x19,
    0x2, 0xf1, 0xf3, 0x5, 0x34, 0x1b, 0x2, 0xf2, 0xf0, 0x3, 0x2, 0x2, 0x2,
    0xf2, 0xf1, 0x3, 0x2, 0x2, 0x2, 0xf3, 0xf5, 0x3, 0x2, 0x2, 0x2, 0xf4,
    0xef, 0x3, 0x2, 0x2, 0x2, 0xf4, 0xf2, 0x3, 0x2, 0x2, 0x2, 0xf5, 0xf8,
    0x3, 0x2, 0x2, 0x2, 0xf6, 0xf7, 0x3, 0x2, 0x2, 0x2, 0xf6, 0xf4, 0x3,
    0x2, 0x2, 0x2, 0xf7, 0x2b, 0x3, 0x2, 0x2, 0x2, 0xf8, 0xf6, 0x3, 0x2,
    0x2, 0x2, 0xf9, 0xfc, 0x7, 0xc, 0x2, 0x2, 0xfa, 0xfd, 0x7, 0x26, 0x2,
    0x2, 0xfb, 0xfd, 0x5, 0x3e, 0x20, 0x2, 0xfc, 0xfa, 0x3, 0x2, 0x2, 0x2,
    0xfc, 0xfb, 0x3, 0x2, 0x2, 0x2, 0xfd, 0xfe, 0x3, 0x2, 0x2, 0x2, 0xfe,
    0xff, 0x5, 0x2e, 0x18, 0x2, 0xff, 0x2d, 0x3, 0x2, 0x2, 0x2, 0x100, 0x105,
    0x7, 0x1d, 0x2, 0x2, 0x101, 0x104, 0x5, 0x30, 0x19, 0x2, 0x102, 0x104,
    0x5, 0x34, 0x1b, 0x2, 0x103, 0x101, 0x3, 0x2, 0x2, 0x2, 0x103, 0x102,
    0x3, 0x2, 0x2, 0x2, 0x104, 0x107, 0x3, 0x2, 0x2, 0x2, 0x105, 0x106,
    0x3, 0x2, 0x2, 0x2, 0x105, 0x103, 0x3, 0x2, 0x2, 0x2, 0x106, 0x108,
    0x3, 0x2, 0x2, 0x2, 0x107, 0x105, 0x3, 0x2, 0x2, 0x2, 0x108, 0x109,
    0x7, 0x1e, 0x2, 0x2, 0x109, 0x2f, 0x3, 0x2, 0x2, 0x2, 0x10a, 0x10b,
    0x7, 0xa, 0x2, 0x2, 0x10b, 0x10c, 0x5, 0x32, 0x1a, 0x2, 0x10c, 0x10d,
    0x7, 0x25, 0x2, 0x2, 0x10d, 0x31, 0x3, 0x2, 0x2, 0x2, 0x10e, 0x118,
    0x5, 0x3a, 0x1e, 0x2, 0x10f, 0x114, 0x5, 0x38, 0x1d, 0x2, 0x110, 0x111,
    0x7, 0x21, 0x2, 0x2, 0x111, 0x113, 0x5, 0x38, 0x1d, 0x2, 0x112, 0x110,
    0x3, 0x2, 0x2, 0x2, 0x113, 0x116, 0x3, 0x2, 0x2, 0x2, 0x114, 0x112,
    0x3, 0x2, 0x2, 0x2, 0x114, 0x115, 0x3, 0x2, 0x2, 0x2, 0x115, 0x118,
    0x3, 0x2, 0x2, 0x2, 0x116, 0x114, 0x3, 0x2, 0x2, 0x2, 0x117, 0x10e,
    0x3, 0x2, 0x2, 0x2, 0x117, 0x10f, 0x3, 0x2, 0x2, 0x2, 0x118, 0x33, 0x3,
    0x2, 0x2, 0x2, 0x119, 0x11a, 0x9, 0x2, 0x2, 0x2, 0x11a, 0x11b, 0x5,
    0x36, 0x1c, 0x2, 0x11b, 0x11c, 0x7, 0x25, 0x2, 0x2, 0x11c, 0x35, 0x3,
    0x2, 0x2, 0x2, 0x11d, 0x122, 0x5, 0x38, 0x1d, 0x2, 0x11e, 0x11f, 0x7,
    0x21, 0x2, 0x2, 0x11f, 0x121, 0x5, 0x38, 0x1d, 0x2, 0x120, 0x11e, 0x3,
    0x2, 0x2, 0x2, 0x121, 0x124, 0x3, 0x2, 0x2, 0x2, 0x122, 0x120, 0x3,
    0x2, 0x2, 0x2, 0x122, 0x123, 0x3, 0x2, 0x2, 0x2, 0x123, 0x37, 0x3, 0x2,
    0x2, 0x2, 0x124, 0x122, 0x3, 0x2, 0x2, 0x2, 0x125, 0x126, 0x9, 0x3,
    0x2, 0x2, 0x126, 0x39, 0x3, 0x2, 0x2, 0x2, 0x127, 0x128, 0x9, 0x4, 0x2,
    0x2, 0x128, 0x3b, 0x3, 0x2, 0x2, 0x2, 0x129, 0x12a, 0x7, 0x23, 0x2,
    0x2, 0x12a, 0x3d, 0x3, 0x2, 0x2, 0x2, 0x12b, 0x12c, 0x9, 0x5, 0x2, 0x2,
    0x12c, 0x3f, 0x3, 0x2, 0x2, 0x2, 0x2d, 0x41, 0x44, 0x47, 0x4c, 0x4f,
    0x55, 0x57, 0x5d, 0x66, 0x68, 0x6a, 0x78, 0x7d, 0x84, 0x8a, 0x8c, 0x8e,
    0x94, 0x96, 0x98, 0x9d, 0xb1, 0xb5, 0xba, 0xbe, 0xc4, 0xc8, 0xcd, 0xd1,
    0xd9, 0xde, 0xe2, 0xe8, 0xeb, 0xf2, 0xf4, 0xf6, 0xfc, 0x103, 0x105,
    0x114, 0x117, 0x122,
  };

  atn::ATNDeserializer deserializer;
  _atn = deserializer.deserialize(_serializedATN);

  size_t count = _atn.getNumberOfDecisions();
  _decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) {
    _decisionToDFA.emplace_back(_atn.getDecisionState(i), i);
  }
}

eversafe_llvm_settingsParser::Initializer eversafe_llvm_settingsParser::_init;
