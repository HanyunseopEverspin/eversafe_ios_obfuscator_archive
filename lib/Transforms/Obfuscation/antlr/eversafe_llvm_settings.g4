grammar eversafe_llvm_settings;

settings : (objc?  cpp?  c?);
objc : OBJC LBRACE (setApplyFormat)? objcInternalData? RBRACE;
objcInternalData : (objcGlobalMethodFormat | objcClassFormat )*? ;
objcClassFormat  : CLASS (NAME|duplicateKeyword) LBRACE objcClassIntetnalData RBRACE;
objcClassIntetnalData : (objcMethodFormat | (setApplyFormat | addRemoveApplyForamt))* ;
objcMethodFormat : METHOD (ObjcGlobalMethodName) block ;
objcGlobalMethodFormat : METHOD ObjcGlobalMethodName block;


//cpp : CPP LBRACE (setApplyFormat)? (cppInternalData)* RBRACE;
//cppInternalData : (cppGlobalMethodFormat  | cppClassFormat) ;
//cppClassInternalData : (cppClassFormat | cppMethodFormat | (setApplyFormat | addRemoveApplyForamt))* ;
//cppMethodFormat : METHOD (cppMethodName) block ;
//cppClassFormat : CLASS cppName LBRACE cppClassInternalData RBRACE ;
//cppGlobalMethodFormat : METHOD (NAME) block;
//cppName : (NAME (DCOLON NAME)*  ((DCOLON DWILDCARD | DCOLON WILDCARD))) | WILDCARD;
//cppMethodName : NAME (DCOLON NAME)* ;





cpp : CPP LBRACE (setApplyFormat)? (cppInternalData)* RBRACE;
cppInternalData : (cppMethodFormat  | cppClassFormat) ;
cppClassInternalData : (cppClassFormat | cppMethodFormat | (setApplyFormat | addRemoveApplyForamt))* ;
cppSingleAsteriskClassInternalData : ( cppMethodFormat | (setApplyFormat | addRemoveApplyForamt))* ;

cppClassFormat : (singleCppClassFormat | doulbeCppClassFormat);
singleCppClassFormat : NAMESPACE singleAsteriskName LBRACE cppSingleAsteriskClassInternalData RBRACE ;
doulbeCppClassFormat : NAMESPACE doubleAsteriskName LBRACE cppClassInternalData RBRACE ;

cppMethodFormat : METHOD (cppMethodName) block ;

cppName : (singleAsteriskName | doubleAsteriskName);
singleAsteriskName : ((NAME|duplicateKeyword) (DCOLON (NAME|duplicateKeyword))*  (( DCOLON WILDCARD))) | WILDCARD;
doubleAsteriskName : ((NAME|duplicateKeyword) (DCOLON (NAME|duplicateKeyword))*  ((DCOLON DWILDCARD)));

cppMethodName : (NAME|duplicateKeyword) (DCOLON (NAME|duplicateKeyword))*  ;


c : CC LBRACE (setApplyFormat)? (cInternalData)? RBRACE ;
cInternalData : (functionFormat | (setApplyFormat | addRemoveApplyForamt))*? ;
functionFormat : FUNCTION (NAME|duplicateKeyword) block ;


block  : LBRACE (setApplyFormat | addRemoveApplyForamt)*? RBRACE ;

// Apply Format
setApplyFormat : ( (SET) setObfusKeys SEMICOLON) ;
setObfusKeys   : (allNoneObfusKey | (addRemoveObfusKey (COMMA addRemoveObfusKey)*)) ;

addRemoveApplyForamt : ( (REMOVE | ADD) addRemoveObfusKeys SEMICOLON) ;
addRemoveObfusKeys : addRemoveObfusKey (COMMA addRemoveObfusKey)* ;

addRemoveObfusKey : (ANTICLASSDUMP | ANTIDECOMPILE | DUMMY | BOGUS | FLATTEN | WRAPPER | BRANCH | SPILT | STRING | SUBSTITUDE) ;
allNoneObfusKey    : ( ALL | NON) ;

// Separator
separ : (DCOLON) ;

duplicateKeyword : (NON|ALL|ANTICLASSDUMP|ANTIDECOMPILE|DUMMY|FUNCTION|CPP|OBJC|CC|CLASS|ADD|SET|REMOVE) ;

// Obfuscation Keyword
CPP : C P P;
OBJC : O B J C ;
CC : C;
CLASS  : C L A S S ;
NAMESPACE  : N A M E S P A C E ;
METHOD : M E T H O D ;
ADD : A D D;
SET : S E T;
REMOVE : R E M O V E;
FUNCTION: F U N C T I O N;
// Obfuscation Option Keyword

BOGUS   : B O G U S ;
FLATTEN : F L A T T E N;
WRAPPER : W R A P P E R;
BRANCH  : B R A N C H;
SPILT   : S P I L T;
STRING  : S T R I N G;
DUMMY   : D U M M Y ;
ANTIDECOMPILE : A N T I D E C O M P I L E;
SUBSTITUDE    : S U B S T I T U D E;
ANTICLASSDUMP : A N T I C L A S S D U M P;
ALL     : A L L ;
NON     : N O N E;

DWILDCARD: '**';
WILDCARD : '*';
LBRACK   : '[';
RBRACK   : ']';
LBRACE   : '{';
RBRACE   : '}';
LPAREN   : '(';
RPAREN   : ')';
COMMA    : ',';
DOT      : '.';
DCOLON   : '::';
INNER    : '$';
SEMICOLON: ';';

NAME : [a-zA-Z][a-zA-Z0-9_()]*;




CppGlobalMethodName : [a-zA-Z][a-zA-Z0-9_]*;
ObjcMethodName : (('-'|'+')*? LBRACK  NAME' ' [a-zA-Z] [a-zA-Z0-9:]* RBRACK) ;
ObjcGlobalMethodName : (('-'|'+')*? LBRACK  [a-zA-Z] [a-zA-Z0-9:]* RBRACK) ;

CppMethodName : [a-zA-Z][a-zA-Z0-9_]* ;
//cFunctionName : [a-zA-Z][a-zA-Z0-9]*;


LineComment
:   '#' ~[\r\n]*
-> skip
;

WS : [ \r\t\n]+ -> skip ;
ANY : . ;



/* case insensitive lexer matching */

fragment A : ('a' | 'A');
fragment B : ('b' | 'B');
fragment C : ('c' | 'C');
fragment D : ('d' | 'D');
fragment E : ('e' | 'E');
fragment F : ('f' | 'F');
fragment G : ('g' | 'G');
fragment H : ('h' | 'H');
fragment I : ('i' | 'I');
fragment J : ('j' | 'J');
fragment L : ('l' | 'L');
fragment M : ('m' | 'M');
fragment N : ('n' | 'N');
fragment O : ('o' | 'O');
fragment P : ('p' | 'P');
fragment R : ('r' | 'R');
fragment S : ('s' | 'S');
fragment T : ('t' | 'T');
fragment U : ('u' | 'U');
fragment W : ('w' | 'W');
fragment Y : ('y' | 'Y');
fragment V : ('v' | 'V');
fragment X : ('x' | 'X');
