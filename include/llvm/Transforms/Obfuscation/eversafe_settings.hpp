//
//  eversafe_settings.hpp
//  antlrcpp-demo
//
//  Created by yunseop han on 07/11/2017.
//  Copyright © 2017 Dan McLaughlin. All rights reserved.
//

#ifndef eversafe_settings_hpp
#define eversafe_settings_hpp

#include <iostream>
#include <vector>
#include <fstream>



typedef enum _ObfuscationFlag {
    ANTIHEXFLAG = 1 << 0,
    DUMMYFLAG = 1 << 1,
    BOGUSFLAG = 1 << 2,
    FLATTENFLAG = 1 << 3,
    WRAPPERFLAG = 1 << 4,
    BRANCHFLAG = 1 << 5,
    SPILTFLAG = 1 << 6,
    STRINGFLAG = 1 << 7,
    SUBSTITUDEFLAG = 1 << 8,
    ANTICLASSFLAG = 1 << 9
}ObfuscationFlag;

class SettingsTrie{
private:
    std::string tokenName;
    SettingsTrie* parentNode;
    int settingsType;
    int tokenType;
    bool isCompare(std::string name);
    SettingsTrie* searchNode(SettingsTrie* rootNode, std::string tokenName);


public:
    std::vector<SettingsTrie*> child;
    std::string originalFullName;
    void setApply(std::vector<std::string> applyFlagVector);
    SettingsTrie* fullNameSearchNode(SettingsTrie* rootNode, std::string fullName);

    int applyFlag;
    SettingsTrie();
    SettingsTrie(int settingsType); //root node
    SettingsTrie(std::string fullName, int tokenType, int settingsType);
    void printNode(SettingsTrie* rootNode);
    void settingInsert(SettingsTrie *rootNode, SettingsTrie* targetNode);

    SettingsTrie* isApply(std::string moduleName, int settingsType);


    void addRemoveApply(size_t addRemoveFlag, std::vector<std::string> applyFlagVector);
};

class SettingsMain{
public:
    std::string filePath;
    std::string DemangleName(const std::string &Name);
    SettingsMain(std::string filePath);
    void makeTrieNode();
    bool isApply(std::string moduleName, ObfuscationFlag oF);
};

#endif /* eversafe_settings_hpp */
