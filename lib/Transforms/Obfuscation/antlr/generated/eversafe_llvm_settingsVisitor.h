
// Generated from eversafe_llvm_settings.g4 by ANTLR 4.7.1

#pragma once


#include "antlr4-runtime.h"
#include "eversafe_llvm_settingsParser.h"


namespace antlrcpptest {

/**
 * This class defines an abstract visitor for a parse tree
 * produced by eversafe_llvm_settingsParser.
 */
class  eversafe_llvm_settingsVisitor : public antlr4::tree::AbstractParseTreeVisitor {
public:

  /**
   * Visit parse trees produced by eversafe_llvm_settingsParser.
   */
    virtual antlrcpp::Any visitSettings(eversafe_llvm_settingsParser::SettingsContext *context) = 0;

    virtual antlrcpp::Any visitObjc(eversafe_llvm_settingsParser::ObjcContext *context) = 0;

    virtual antlrcpp::Any visitObjcInternalData(eversafe_llvm_settingsParser::ObjcInternalDataContext *context) = 0;

    virtual antlrcpp::Any visitObjcClassFormat(eversafe_llvm_settingsParser::ObjcClassFormatContext *context) = 0;

    virtual antlrcpp::Any visitObjcClassIntetnalData(eversafe_llvm_settingsParser::ObjcClassIntetnalDataContext *context) = 0;

    virtual antlrcpp::Any visitObjcMethodFormat(eversafe_llvm_settingsParser::ObjcMethodFormatContext *context) = 0;

    virtual antlrcpp::Any visitObjcGlobalMethodFormat(eversafe_llvm_settingsParser::ObjcGlobalMethodFormatContext *context) = 0;

    virtual antlrcpp::Any visitCpp(eversafe_llvm_settingsParser::CppContext *context) = 0;

    virtual antlrcpp::Any visitCppInternalData(eversafe_llvm_settingsParser::CppInternalDataContext *context) = 0;

    virtual antlrcpp::Any visitCppClassInternalData(eversafe_llvm_settingsParser::CppClassInternalDataContext *context) = 0;

    virtual antlrcpp::Any visitCppSingleAsteriskClassInternalData(eversafe_llvm_settingsParser::CppSingleAsteriskClassInternalDataContext *context) = 0;

    virtual antlrcpp::Any visitCppClassFormat(eversafe_llvm_settingsParser::CppClassFormatContext *context) = 0;

    virtual antlrcpp::Any visitSingleCppClassFormat(eversafe_llvm_settingsParser::SingleCppClassFormatContext *context) = 0;

    virtual antlrcpp::Any visitDoulbeCppClassFormat(eversafe_llvm_settingsParser::DoulbeCppClassFormatContext *context) = 0;

    virtual antlrcpp::Any visitCppMethodFormat(eversafe_llvm_settingsParser::CppMethodFormatContext *context) = 0;

    virtual antlrcpp::Any visitCppName(eversafe_llvm_settingsParser::CppNameContext *context) = 0;

    virtual antlrcpp::Any visitSingleAsteriskName(eversafe_llvm_settingsParser::SingleAsteriskNameContext *context) = 0;

    virtual antlrcpp::Any visitDoubleAsteriskName(eversafe_llvm_settingsParser::DoubleAsteriskNameContext *context) = 0;

    virtual antlrcpp::Any visitCppMethodName(eversafe_llvm_settingsParser::CppMethodNameContext *context) = 0;

    virtual antlrcpp::Any visitC(eversafe_llvm_settingsParser::CContext *context) = 0;

    virtual antlrcpp::Any visitCInternalData(eversafe_llvm_settingsParser::CInternalDataContext *context) = 0;

    virtual antlrcpp::Any visitFunctionFormat(eversafe_llvm_settingsParser::FunctionFormatContext *context) = 0;

    virtual antlrcpp::Any visitBlock(eversafe_llvm_settingsParser::BlockContext *context) = 0;

    virtual antlrcpp::Any visitSetApplyFormat(eversafe_llvm_settingsParser::SetApplyFormatContext *context) = 0;

    virtual antlrcpp::Any visitSetObfusKeys(eversafe_llvm_settingsParser::SetObfusKeysContext *context) = 0;

    virtual antlrcpp::Any visitAddRemoveApplyForamt(eversafe_llvm_settingsParser::AddRemoveApplyForamtContext *context) = 0;

    virtual antlrcpp::Any visitAddRemoveObfusKeys(eversafe_llvm_settingsParser::AddRemoveObfusKeysContext *context) = 0;

    virtual antlrcpp::Any visitAddRemoveObfusKey(eversafe_llvm_settingsParser::AddRemoveObfusKeyContext *context) = 0;

    virtual antlrcpp::Any visitAllNoneObfusKey(eversafe_llvm_settingsParser::AllNoneObfusKeyContext *context) = 0;

    virtual antlrcpp::Any visitSepar(eversafe_llvm_settingsParser::SeparContext *context) = 0;

    virtual antlrcpp::Any visitDuplicateKeyword(eversafe_llvm_settingsParser::DuplicateKeywordContext *context) = 0;


};

}  // namespace antlrcpptest
